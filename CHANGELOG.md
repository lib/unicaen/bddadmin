1.3.0 (26/02/2025)
------------------

- Ajout d'une nouvelle commande bddadmin:update-sequences qui met à jour les séquences en fonction des ID les plus élevés


1.2.0 (20/02/2025)
------------------

- Oracle : Build des indexs à l'update-bdd
- Meilleure gestion des managers compilables
- Refactorisation de l'exécution des requêtes collectées (interne)


1.1.2 (07/02/2025)
------------------

- Renforcement du système de tests automatisés.
- [Fix] Meilleure gestion de la méthode exists, lorsqu'on traite sur le schéma public en Postgresql



1.1.1 (05/02/2025)
------------------

- [Fix] Erreur de nomage des managers d'objets mal compilés



1.1.0 (30/01/2025)
------------------

- Possibilité d'utiliser Connection de Doctrine pour faire des requêtes en modification (pb Oracle gestion des transactions)


1.0.6 (05/12/2024)
------------------

- [Fix] Nouvelle correction regression suite sortie 1.0.4...
- Ajout de la possibilité de na pas faire de transaction dans le Table->merge
- classe Module déplacée dans src : plus propre

1.0.5 (03/12/2024)
------------------

- [Fix] Correction regressions suite sortie 1.0.4...

1.0.4 (03/12/2024)
------------------

- Nouvelle option "callback" de Table->merge pour pouvoir suivre en temps réel l'évolution de l'opération


1.0.3 (29/11/2024)
------------------

- [Fix] Problème de chargement de DDL avec Oracle : pas de schémas
- [Fix] Récupération plus propre & souple du printWorkingDirectory (PWD) en CLI


1.0.2 (21/11/2024)
------------------

- [Optimisation] Table récupère maintenant la DDL depuis le fichier de ddl, et à défaut la recalcule depuis le BDD

1.0.1 (19/11/2024)
------------------

- [Fix] Correction d'un problème sous Postgres lors de suppressions de procédures
- [Fix] Plus de détection à tort de modifs sur les procédures & fonctions en Postgres


1.0 (18/11/2024)
------------------

- BddAdmin est compatible par défaut avec Postgresql
- Module Laminas
- Facade CLI pour les commandes
- DataManager compatible Postgresql
- MigrationManager compatible Postgresql
- plein d'autres trucs


0.9.10 (15/10/2024)
------------------

- [Fix] Correction du problème de détection défectueuse du logging des tables



0.9.9 (12/09/2024)
------------------

- [Fix] Correction des tests unitaires qui doivent passer
- Possibilité de configurer les tests unitaires via un fichier dnas config/autoload de l'appli pour éviter qu'il soit supprimé à chaque MAJ composer
- [Fix] Correction d'un bug lors du changement de nombre de caractères sur des character varying lors d'un update de DDL


0.9.8 (22/08/2024)
------------------

- [Fix] Pour Oracle, la recherche des séquences des tables fonctionne pour les tables & séquences de plus de 30 caractères
- Ajout de la méthode Table->getLastInsertId permettant de retourner la dernière valeur de séquence utilisée pour peupler un ID lors d'un INSERT
- Nouvelle option "return-insert-data" pour Table->merge, permettant de retourner dans "insert-data" les données insérées pour pouvoir récupérer les ID peuplés à partir des séquences
- Modernisation & renforcement du typage de Table

0.9.7 (19/07/2024)
------------------

- Lors d'un update de DDL, enregistrement automatique du fichier des positions s'il est défini en configuration


0.9.6 (04/07/2024)
------------------

- [Fix] Suppression de l'attribut 'ENABLE' dans l'ajout de colonne NOT NULL de Postgresql


0.9.5 (04/07/2024)
------------------

- [Fix] Correction d'un souci de DIFF des séquences avec Postgresql
- [Fix] Correction d'un souci empêchant de supprimer des fonctions avec Postgresql


0.9.4 (26/06/2024)
--------------

- [fix] Meilleur chargement & contrôle du fichier des positionnements de colonnes

0.9.3
--------------
- [FIX] Réparation du système de positionnement de colonnes qui ne fonctionnait plus


0.9.2
--------------

Driver PostreSQL :
- Les mots-clés réservés par PostgreSQL posait soucis (schema, name, etc..), rajout de quote autour.
- Rajout d'une correspondance au type "character" qui n'était pas présent.

0.9.1
--------------

Début de DOC
Simplification des méthodes principales
Driver Postgresql avec gestion des clés