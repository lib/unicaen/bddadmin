# BddAdmin

Outil d'administration de bases de données Postgresql et Oracle

## Fonctionnalités principales :
- Crée une DDL à partir d'une base de données
- Met à jour les objets d'une base de données à partir d'une DDL
- Permet de générer des DIFF en SQL entre deux bases, 2 DDL ou entre une DDL et une base de données
- Permet de constituer un jeu de données pour la mise à jour des données
- Pour les opérations de migrations complexes, un système de scripts qui se lancent sur déclencheur permettant de faire les manipulations nécessaires sur les données
- Système de copie / sauvegarde / restauration de base de données

[Changelog ici](CHANGELOG.md)

## Prérequis
- PHP 8.2
- Drivers Bdd installés, en fonction du SGBD visé (OCI8, PDO, etc)


[Documentation](doc/doc.md)