<?php

namespace Unicaen\BddAdmin;

return [
    'unicaen-bddadmin' => [

        'connection' => [
        ],

        'current_connection' => 'default',

        'ddl' => [
            'dir'                     => 'data/ddl',
            'columns_positions_file'  => 'data/ddl_columns_pos.php',
            'filters'                 => [],
            'update-bdd-filters'      => [],
            'update-ddl-filters'      => [],
            'init-update-bdd-filters' => true,
        ],

        'data' => [
            'actions' => [
                'install' => 'Insertion des données',
                'update'  => 'Contrôle et mise à jour des données',
            ],
            'config'  => [], // configuration par tables
            'sources' => [],
        ],

        'migration' => [
            // array des classes ou services utilisés pour la migration [index => classe ou service, ...]
        ],

        'id_column' => 'id',

        'histo' => [
            'user_id'                      => null,
            'histo_creation_column'        => 'histo_creation',
            'histo_modification_column'    => 'histo_modification',
            'histo_destruction_column'     => 'histo_destruction',
            'histo_createur_id_column'     => 'histo_createur_id',
            'histo_modificateur_id_column' => 'histo_modificateur_id',
            'histo_destructeur_id_column'  => 'histo_destructeur_id',
        ],

        'import' => [
            'source_id'          => null,
            'source_id_column'   => 'source_id',
            'source_code_column' => 'source_code',
        ],
    ],


    'service_manager' => [
        'factories' => [
            Bdd::class                            => BddFactory::class,
            Command\InstallCommand::class         => Command\CommandFactory::class,
            Command\UpdateCommand::class          => Command\CommandFactory::class,
            Command\UpdateDdlCommand::class       => Command\CommandFactory::class,
            Command\UpdateDataCommand::class      => Command\CommandFactory::class,
            Command\UpdateSequencesCommand::class => Command\CommandFactory::class,
            Command\ClearCommand::class           => Command\CommandFactory::class,
            Command\CopyToCommand::class          => Command\CommandFactory::class,
            Command\CopyFromCommand::class        => Command\CommandFactory::class,
            Command\LoadCommand::class            => Command\CommandFactory::class,
            Command\SaveCommand::class            => Command\CommandFactory::class,
        ],
    ],


    'laminas-cli' => [
        'commands' => [
            'bddadmin:install'          => Command\InstallCommand::class,
            'bddadmin:update'           => Command\UpdateCommand::class,
            'bddadmin:clear'            => Command\ClearCommand::class,
            'bddadmin:update-ddl'       => Command\UpdateDdlCommand::class,
            'bddadmin:update-data'      => Command\UpdateDataCommand::class,
            'bddadmin:update-sequences' => Command\UpdateSequencesCommand::class,
            'bddadmin:copy-to'          => Command\CopyToCommand::class,
            'bddadmin:copy-from'        => Command\CopyFromCommand::class,
            'bddadmin:load'             => Command\LoadCommand::class,
            'bddadmin:save'             => Command\SaveCommand::class,
        ],
    ],
];