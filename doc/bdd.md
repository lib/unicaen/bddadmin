# Bdd : opérations principales

Classe `Unicaen\BddAdmin\Bdd`

Bdd est la classe mère de la bibliothèque. Elle comporte les principales méthodes et permet d'accéder
à tous les autres outils de la bibliothèque.

Voici ses fonctionnalités :

- [Gestion des transactions](#transactions)
- [Accès aux outils (Table, managers, DataGen, Migration)](#accès-aux-outils)
- [Exécution de requêtes & SELECTs](requetage.md)
- [Gestion des options de configuration de la bibliothèque](options.md)
- [Récupérations de DDL à partir de la BDD ou de la définition stockée dans le répertoire du code source](ddl.md)
- Opérations de modification de la base
- [Calcul de différentiels](diff.md)
- [Recalcul des valeurs de séquences](#recalcul-des-valeurs-de-séquences)
- [Mise à jour des vues matérialisées](#mise-à-jour-des-vues-matérialisées)
- [Compilation des objets (Oracle uniquement)](#compilation-des-objets)
- [Copies, sauvegardes, restaurations](copies-sauvegardes-restaurations.md)

## Instanciation

### Depuis Laminas

BddAdmin est un module Laminas.
Il faut donc l'utiliser par ce biais.

Comme pour les autres bibliothèques Unicaen,
copier/coller les fichiers config/*.php.dist et les adapter.

Ajouter 'Unicaen\BddAdmin', à la liste de vos mosules dans votre application.

Pour y accéder :
Un BddAwareTrait permet d'injecter ses accesseurs.
Dans la Factory de votre classe, ajouter :
$service->setBdd($container->get(Unicaen\BddAdmin\Bdd::class));




### En accès direct

Ce mode peut servir si on utilise la bibliothèque hors Laminas.
Il peut aussi servir si vous voulez accéder à une autre BDD.

Voici comment instancier un nouvel objet Bdd

```php
use Unicaen\BddAdmin\Bdd;

/* Paramètres d'accès à votre BDD */
$config = [
        'driver'   => 'Postgresql', // Postgresql ou Oracle
        'host'     => '*********',  // IP ou nom DNS
        'port'     => 5432,         // port, à adapter
        'dbname'   => '*********',  // Nom de votre base de données
        'user'     => '*********',  // Utilisateur : "username" ou "user", les 2 clés fonctionnent
        'password' => '*********',  // Mot de passe
];

$bdd = new Bdd($config);
```
Cette portion de code pourra être placée dans une Factory qui ira chercher les paramètres de config dans ceux de votre
application.



## Utilisation des commandes standard

Les commandes ne sont disponibles que si vous utilisez BddAdmin avec son module Laminas.

BddAdmin possède une facade CLI avec des commandes Symphony accessibles.

La liste est accessible via la commande

./vendor/bin/laminas list

Les commandes BddAdmin commencent tout simplement par "bddadmin:".



## Accès aux outils

### DataManager

Il sert à gérer les mises à jour de données

Accès :

```php
/** @var $bdd \Unicaen\BddAdmin\Bdd */

$du = $bdd->data();

```





## Transactions

Trois méthodes permettent de gérer les transactions au niveau de votre SGBD :

- `beginTransaction` démarre une transaction
- `commitTransaction` commite la transaction
- `rollbackTransaction` permet de revenir à l'état antérieur à la transaction

Les transactions sont utiles dans plusieurs cas de figure.
Pour des raisons de performances tout d'abord. Si vous avez beaucoup d'opérations à faire sur des données, alors
démarrer une transaction optimisera vos temps de traitements.
Ensuite, si votre traitement comporte plusieurs opérations et que l'une d'entre elles échoue, vous pourrez revenir en
arrière en faisant un rollback.
Enfin, Si vous utilisez Postgresql, les opérations de modification sur les objets (modifications des colonnes, etc)
peuvent également faire l'objet de transactions.
Cela pourra fiabiliser vos opérations de mise à jour, ce sans coupure de service.

BddAdmin utilise implicitement les transactions pour les opérations passives sur les données (merge de tables).
Pour les autres opérations (modification de DDL, etc.) vous devrez les utiliser explicitement.

Exemple de mise à niveau d'une BDD encapsulée dans une transaction. Il est à noter qu'ici, on commite systématiquement,
y compris en cas de problème.
Cet aspect pourra être amélioré le cas échéant en collectant les erreurs.

```php
// Récupération du schéma de référence, issu du répertoire spécifié via l'option Bdd::OPTION_DDL_DIR
// note : le répertoire peut aussi être passé directement en argument de getRefDdl
$ref = $bdd->getRefDdl();

// Filtre pour l'appliquer les modifications que pour la DDL et ne supprime pas les objets autres
// C'est facultatif, à activer selon contexte
$filters = $ref->makeFilters();

// On met à jour la BDD
$bdd->beginTransaction();
$bdd->alter($ref, $filters);
$bdd->commitTransaction();

```

## Compilation des objets

La compilation des objets n'a de sens que sous Oracle.
Il est possible de compiler certains objets (Packages, vues, triggers, etc).
Cette opération est faite implicitement au moment de la modification de la base de données.

```php

/** @var $bdd \Unicaen\BddAdmin\Bdd */
$bdd->compilerTout();
```

## Mise à jour des vues matérialisées

Les vues matérialisées nécessitent parfois d'être recalculées. Cette opération peut être lancée avec :

```php

/** @var $bdd \Unicaen\BddAdmin\Bdd */
$bdd->refreshMaterializedViews();
```

Si vous avez déjà la DDL, vous pouvez la transmettre à la méthode en argument.

## Recalcul des valeurs de séquences

Les valeurs des séquences sont actualisées à partir des colonnes correspondantes s'il y en a.
Cela évite d'insérer une nouvelle ligne avec un ID doté numéro de séquence déjà présent en base.

```php

/** @var $bdd \Unicaen\BddAdmin\Bdd */
$bdd->majSequences();
```

Cette opération est réalisée automatiquement lorsque vous mettez à jour le jeu de données de votre base à l'aide
du `DataUpdater`.

