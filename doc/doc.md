# Documentation

## Installation

BddAdmin est une bibliothèque Unicaen installable via composer en ajoutant la ligne suivante à votre `composer.json` :

```yaml
"unicaen/bddadmin"                            : "0.9.2",
```

La version sera bien entendu à actualiser.


La documentation complète sera (plus tard) accessible ici :
- [Bdd](bdd.md)
- [Managers](managers.md)
- [Table](table.md)
- [Filtres](filtres.md)
- [Données](donnees.md)
- [Migrations](migrations.md)
- [Ddl](ddl.md)
- [Requêtage](requetage.md)
- [Différentiels](diff.md)
- [Copies, sauvegardes, restaurations](copies-sauvegardes-restaurations.md)
- [Logger](logger.md)
- [Options de configuration](options.md)

## Exemples d'utilisation

### Comment instancier BddAdmin

Voici un script rapide vous permettant d'instancier un nouvel objet Bdd

```php
use Unicaen\BddAdmin\Bdd;

/* Paramètres d'accès à votre BDD */
$config = [
        'driver'   => 'Postgresql', // Postgresql ou Oracle
        'host'     => '*********',  // IP ou nom DNS
        'port'     => 5432,         // port, à adapter
        'dbname'   => '*********',  // Nom de votre base de données
        'username' => '*********',  // Utilisateur
        'password' => '*********',  // Mot de passe
];

$bdd = new Bdd($config);

$bdd->setOptions([
    /* Facultatif, permet de spécifier une fois pour toutes le répertoire où sera renseignée la DDL de votre BDD */
    Bdd::OPTION_DDL_DIR => getcwd() . '/data/ddl',

    /* Facultatif, spécifie le répertoire où seront stockés vos scripts de migration si vous en avez */
    Bdd::OPTION_MIGRATION_DIR => getcwd() . '/admin/migration/',

    /* Facultatif, permet de personnaliser l'ordonnancement des colonnes dans les tables */
    Bdd::OPTION_COLUMNS_POSITIONS_FILE => getcwd() . '/data/ddl_columns_pos.php',
]);

// première requête pour tester, à personnaliser selon votre modèle de données
$data = $bdd->select('select * from annee');

var_dump($data);
```

### Faire un diff depuis la DDL vers la BDD

```php

// Récupération du schéma de référence, issu du répertoire spécifié via l'option Bdd::OPTION_DDL_DIR
// note : le répertoire peut aussi être passé directement en argument de getRefDdl
$ddl = $bdd->getRefDdl();

// On fait un DIFF et on le convertit en SQL
$sql = $bdd->diff($ddl)->toScript();

// Et si on veut faire un diff avec une autre BDD, pour comparer la bdd de prod avec cette de dév:
// /** @var $bddProd \Unicaen\BddAdmin\Bdd */
// /** @var $bddDev \Unicaen\BddAdmin\Bdd */
// $sql = $bddProd->diff($bddDev)->toScript();

echo $sql;
```


### Mise à jour d'une DDL à partir de la base de données

```php
/* Filtres éventuels pour éviter de faire figurer dans la DDL des objets d'utilité locale */
$filters = [];

// On calcule la DDL depuis la BDD
$ddl = $bdd->getDdl($filters);

// Enregistrement du schéma de référence, vers le répertoire spécifié via l'option Bdd::OPTION_DDL_DIR
// note : le répertoire peut aussi être passé directement en argument de saveToDir
$ddl->saveToDir();
```


### Mise à jour de la base de données à partir d'une DDL

```php
// Récupération du schéma de référence, issu du répertoire spécifié via l'option Bdd::OPTION_DDL_DIR
// note : le répertoire peut aussi être passé directement en argument de getRefDdl
$ref = $bdd->getRefDdl();

// Filtre pour l'appliquer les modifications que pour la DDL et ne supprime pas les objets autres
// C'est facultatif, à activer selon contexte
$filters = $ref->makeFilters();

// On met à jour la BDD
$bdd->alter($ref, $filters);
```