# Managers


manager
managerList

    public function schema(): SchemaManagerInterface
    public function index(): IndexManagerInterface
    public function materializedView(): MaterializedViewManagerInteface
    public function procedure(): ProcedureManagerInteface
    public function function (): FunctionManagerInteface
    public function package(): PackageManagerInteface
    public function primaryConstraint(): PrimaryConstraintManagerInterface
    public function refConstraint(): RefConstraintManagerInterface
    public function sequence(): SequenceManagerInterface
    public function table(): TableManagerInterface
    public function trigger(): TriggerManagerInterface
    public function uniqueConstraint(): UniqueConstraintManagerInterface
    public function view(): ViewManagerInterface
