<?php

namespace Unicaen\BddAdmin;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Types;
use Unicaen\BddAdmin\Data\DataManager;
use Unicaen\BddAdmin\Ddl\Ddl;
use Unicaen\BddAdmin\Ddl\DdlDiff;
use Unicaen\BddAdmin\Ddl\DdlFilters;
use Unicaen\BddAdmin\Driver\DriverInterface;
use Unicaen\BddAdmin\Event\EventManagerAwareTrait;
use Unicaen\BddAdmin\Exception\BddCompileException;
use Unicaen\BddAdmin\Exception\BddException;
use Unicaen\BddAdmin\Logger\DefaultLogger;
use Unicaen\BddAdmin\Logger\LoggerAwareTrait;
use Unicaen\BddAdmin\Manager\CompilableInterface;
use Unicaen\BddAdmin\Manager\FunctionManagerInteface;
use Unicaen\BddAdmin\Manager\IndexManagerInterface;
use Unicaen\BddAdmin\Manager\ManagerInterface;
use Unicaen\BddAdmin\Manager\MaterializedViewManagerInteface;
use Unicaen\BddAdmin\Manager\PackageManagerInteface;
use Unicaen\BddAdmin\Manager\PrimaryConstraintManagerInterface;
use Unicaen\BddAdmin\Manager\ProcedureManagerInteface;
use Unicaen\BddAdmin\Manager\RefConstraintManagerInterface;
use Unicaen\BddAdmin\Manager\SchemaManagerInterface;
use Unicaen\BddAdmin\Manager\SequenceManagerInterface;
use Unicaen\BddAdmin\Manager\TableManagerInterface;
use Unicaen\BddAdmin\Manager\TriggerManagerInterface;
use Unicaen\BddAdmin\Manager\UniqueConstraintManagerInterface;
use Unicaen\BddAdmin\Manager\ViewManagerInterface;
use \Exception;
use Unicaen\BddAdmin\Manager\VoidManager;
use Unicaen\BddAdmin\Migration\MigrationManager;
use Unicaen\BddAdmin\Trait\OptionsTrait;

class Bdd
{
    use EventManagerAwareTrait;
    use LoggerAwareTrait;
    use OptionsTrait;

    const OPTION_CONNECTION                   = 'connection';
    const OPTION_CURRENT_CONNECTION           = 'current_connection';
    const OPTION_DDL                          = 'ddl';
    const OPTION_DATA                         = 'data';
    const OPTION_MIGRATION                    = 'migration';
    const OPTION_ID_COLUMN                    = 'id_column';
    const OPTION_HISTO                        = 'histo';
    const OPTION_HISTO_USER_ID                = 'histo/user_id';
    const OPTION_HISTO_CREATION_COLUMN        = 'histo/histo_creation_column';
    const OPTION_HISTO_MODIFICATION_COLUMN    = 'histo/histo_modification_column';
    const OPTION_HISTO_DESTRUCTION_COLUMN     = 'histo/histo_destruction_column';
    const OPTION_HISTO_CREATEUR_ID_COLUMN     = 'histo/histo_createur_id_column';
    const OPTION_HISTO_MODIFICATEUR_ID_COLUMN = 'histo/histo_modificateur_id_column';
    const OPTION_HISTO_DESTRUCTEUR_ID_COLUMN  = 'histo/histo_destructeur_id_column';
    const OPTION_IMPORT                       = 'import';
    const OPTION_IMPORT_SOURCE_ID             = 'import/source_id';
    const OPTION_IMPORT_SOURCE_ID_COLUMN      = 'import/source_id_column';
    const OPTION_IMPORT_SOURCE_CODE_COLUMN    = 'import/source_code_column';

    const FETCH_ALL  = 32;
    const FETCH_EACH = 16;
    const FETCH_ONE  = 8;

    const TYPE_INT    = 'int';
    const TYPE_BOOL   = 'bool';
    const TYPE_FLOAT  = 'float';
    const TYPE_STRING = 'string';
    const TYPE_DATE   = 'date';
    const TYPE_BLOB   = 'blob';
    const TYPE_CLOB   = 'clob';

    private array $changements = [
        Ddl::SCHEMA . '.create'             => 'Création des schémas',
        Ddl::SEQUENCE . '.rename'           => 'Renommage des séquences',
        Ddl::TABLE . '.rename'              => 'Renommage des tables',
        Ddl::VIEW . '.rename'               => 'Renommage des vues',
        Ddl::MATERIALIZED_VIEW . '.rename'  => 'Renommage des vues matérialisées',
        Ddl::PROCEDURE . '.rename'          => 'Renommage des procédures',
        Ddl::FUNCTION . '.rename'           => 'Renommage des fonctions',
        Ddl::PACKAGE . '.rename'            => 'Renommage des packages',
        Ddl::INDEX . '.rename'              => 'Renommage des indexes',
        Ddl::TRIGGER . '.rename'            => 'Renommage des triggers',
        Ddl::PRIMARY_CONSTRAINT . '.rename' => 'Renommage des clés primaires',
        Ddl::REF_CONSTRAINT . '.rename'     => 'Renommage des clés étrangères',
        Ddl::UNIQUE_CONSTRAINT . '.rename'  => 'Renommage des contraintes d\'unicité',
        Ddl::TRIGGER . '.drop'              => 'Suppression des triggers',
//        Ddl::SEQUENCE . '.drop'             => 'Suppression des séquences',
        Ddl::VIEW . '.drop'                 => 'Suppression des vues',
        Ddl::MATERIALIZED_VIEW . '.drop'    => 'Suppression des vues matérialisées',
        Ddl::PROCEDURE . '.drop'            => 'Suppression des procédures',
        Ddl::FUNCTION . '.drop'             => 'Suppression des fonctions',
        Ddl::PACKAGE . '.drop'              => 'Suppression des packages',
        Ddl::REF_CONSTRAINT . '.drop'       => 'Suppression des clés étrangères',
        Ddl::PRIMARY_CONSTRAINT . '.drop'   => 'Suppression des clés primaires',
        Ddl::UNIQUE_CONSTRAINT . '.drop'    => 'Suppression des contraintes d\'unicité',
        Ddl::INDEX . '.drop'                => 'Suppression des indexes',
        Ddl::SEQUENCE . '.create'           => 'Création des séquences',
        Ddl::TABLE . '.create'              => 'Création des tables',
        Ddl::TABLE . '.alter'               => 'Modification des tables',
        Ddl::VIEW . '.create'               => 'Création des vues',
        Ddl::PROCEDURE . '.create'          => 'Création des procédures',
        Ddl::FUNCTION . '.create'           => 'Création des fonctions',
        Ddl::PACKAGE . '.create'            => 'Création des packages',
        Ddl::SEQUENCE . '.alter'            => 'Modification des séquences',
        Ddl::PROCEDURE . '.alter'           => 'Modification des procédures',
        Ddl::FUNCTION . '.alter'            => 'Modification des fonctions',
        Ddl::PACKAGE . '.alter'             => 'Modification des packages',
        Ddl::VIEW . '.alter'                => 'Modification des vues',
        Ddl::MATERIALIZED_VIEW . '.create'  => 'Création des vues matérialisées',
        Ddl::MATERIALIZED_VIEW . '.alter'   => 'Modification des vues matérialisées',
        Ddl::PRIMARY_CONSTRAINT . '.alter'  => 'Modification des clés primaires',
        Ddl::REF_CONSTRAINT . '.alter'      => 'Modification des clés étrangères',
        Ddl::UNIQUE_CONSTRAINT . '.alter'   => 'Modification des contraintes d\'unicité',
        Ddl::TRIGGER . '.alter'             => 'Modification des triggers',
        Ddl::INDEX . '.alter'               => 'Modification des indexes',
        Ddl::INDEX . '.create'              => 'Création des indexes',
        Ddl::PRIMARY_CONSTRAINT . '.create' => 'Création des clés primaires',
        Ddl::REF_CONSTRAINT . '.create'     => 'Création des clés étrangères',
        Ddl::UNIQUE_CONSTRAINT . '.create'  => 'Création des contraintes d\'unicité',
        Ddl::TRIGGER . '.create'            => 'Création des triggers',
        Ddl::TABLE . '.drop'                => 'Suppression des tables',
        Ddl::SEQUENCE . '.drop'             => 'Suppression des séquences',
        Ddl::SCHEMA . '.drop'               => 'Suppression des schémas',
    ];

    private string $connection = 'default';

    /** @var array|Bdd[] */
    private array $bdds = [];

    private ?Connection $doctrineConnection = null;

    private array $config;

    private ?DriverInterface $driver = null;

    private ?MigrationManager $migration = null;

    /**
     * @var ManagerInterface[]
     */
    private array $managers = [];

    protected bool $queryCollect = false;

    protected array $queries = [];

    protected bool $inCopy = false;

    /** @var \Psr\Container\ContainerInterface|null */
    public $__container = null;

    private DataManager $data;



    public function __construct(array $options = [], ?string $connection = null)
    {
        if (!empty($options)) {
            if (empty($connection)) {
                $connection = $options[self::OPTION_CURRENT_CONNECTION] ?? 'default';
            }

            // Compatibilité avec l'ancienne configuration des connexions
            if (!isset($options[self::OPTION_CONNECTION]) && isset($options['host'])) {
                $options = [self::OPTION_CONNECTION => [$connection => $options]];
            }
            $this->setOptions($options);

            $this->connection = $connection;

            $this->connect();
        }

        // On positionne un logger par défaut si on est en mode console, sinon non
        if (PHP_SAPI == 'cli') {
            $this->setLogger(new DefaultLogger());
        }
    }



    public function getDoctrineConnection(): ?Connection
    {
        return $this->doctrineConnection;
    }



    public function setDoctrineConnection(?Connection $doctrineConnection): Bdd
    {
        $this->doctrineConnection = $doctrineConnection;
        return $this;
    }



    /**
     * Hack pour initialiser une classe à partir d'un container respectant Psr\Container\ContainerInterface
     * Si la classe n'est pas connue du container, alors elle est instanciée à l'aide de new
     *
     * @param string $classname
     * @return object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     *
     */
    public function __newClass(string $classname): mixed
    {
        if ($this->__container && method_exists($this->__container, 'has') && method_exists($this->__container, 'get')) {
            if ($this->__container->has($classname)) {
                return $this->__container->get($classname);
            }
        }

        if (!class_exists($classname)) {
            throw new \Exception('La classe ' . $classname . ' n\'existe pas');
        }

        return new $classname;
    }



    public function hasConnection(string $connection): bool
    {
        $connections = $this->options[self::OPTION_CONNECTION];

        return array_key_exists($connection, $connections);
    }



    public function getConnectionName(): string
    {
        return $this->connection;
    }



    public function getBdds(): array
    {
        $bdds = $this->getOption(self::OPTION_CONNECTION);
        unset($bdds[$this->getOption(self::OPTION_CURRENT_CONNECTION)]);

        return array_keys($bdds);
    }



    public function getBdd(string $connection): self
    {
        if (!array_key_exists($connection, $this->bdds)) {
            if ($this->hasConnection($connection)) {
                $this->bdds[$connection] = new self($this->getOptions(), $connection);
            } else {
                throw new \Exception('La connexion "' . $connection . '" n\'est pas présente en config, rubrique unicaen-bddadmin/' . self::OPTION_CONNECTION);
            }
        }

        return $this->bdds[$connection];
    }



    public function getHistoUserId(): ?int
    {
        return $this->getOption(self::OPTION_HISTO_USER_ID);
    }



    public function setHistoUserId(int $histoUserId): self
    {
        $this->setOption(self::OPTION_HISTO_USER_ID, $histoUserId);

        return $this;
    }



    public function getSourceId(): ?int
    {
        return $this->getOption(self::OPTION_IMPORT_SOURCE_ID);
    }



    public function setSourceId(int $sourceOseId): self
    {
        $this->setOption(self::OPTION_IMPORT_SOURCE_ID, $sourceOseId);

        return $this;
    }



    public function beginTransaction(): self
    {
        if ($this->doctrineConnection) {
            $this->doctrineConnection->beginTransaction();
        } else {
            $this->driver->beginTransaction();
        }

        return $this;
    }



    public function commitTransaction(): self
    {
        if ($this->doctrineConnection) {
            $this->doctrineConnection->commit();
        } else {
            $this->driver->commitTransaction();
        }

        return $this;
    }



    public function rollbackTransaction(): self
    {
        if ($this->doctrineConnection) {
            $this->doctrineConnection->rollback();
        } else {
            $this->driver->rollbackTransaction();
        }

        return $this;
    }



    public function exec(string $sql, array $params = [], array $types = []): self
    {
        if ($this->doctrineConnection) {
            foreach ($types as $ti => $tv) {
                switch ($tv) {
                    case self::TYPE_INT:
                        $types[$ti] = Types::INTEGER;
                        break;
                    case self::TYPE_BOOL:
                        $types[$ti] = Types::BOOLEAN;
                        break;
                    case self::TYPE_FLOAT:
                        $types[$ti] = Types::FLOAT;
                        break;
                    case self::TYPE_STRING:
                        $types[$ti] = Types::STRING;
                        break;
                    case self::TYPE_DATE:
                        $types[$ti] = Types::DATETIME_IMMUTABLE;
                        break;
                    case self::TYPE_BLOB:
                        $types[$ti] = Types::BLOB;
                        break;
                    case self::TYPE_CLOB:
                        $types[$ti] = Types::TEXT;
                        break;
                }
            }
            $this->doctrineConnection->executeStatement($sql, $params);
        } else {
            $this->driver->exec($sql, $params, $types);
        }

        return $this;
    }



    public function queryCollect(string $sql, string $description = null): self
    {
        if ($this->queryCollect) {
            $this->queries[$sql] = $description;
        } else {
            $this->exec($sql);
        }

        return $this;
    }



    public function select(string $sql, array $params = [], array $options = []): array|null|SelectParser
    {
        $result = $this->driver->select($sql, $params, $options);
        if (false === $result) {
            return null;
        }
        return $result;
    }



    public function selectOne(string $sql, array $params = [], string $column = null, array $options = []): mixed
    {
        $options['fetch'] = self::FETCH_ONE;
        $res              = $this->select($sql, $params, $options);

        if (empty($res)) return null;

        if (is_array($res) && !empty($column)) {
            if (!isset($res[$column])) {
                $column = strtolower($column);
                $res    = array_change_key_case($res, CASE_LOWER);
            }

            return $res[$column];
        }

        return $res;
    }



    public function selectEach(string $sql, array $params = [], array $options = []): SelectParser
    {
        $options['fetch'] = self::FETCH_EACH;
        return $this->select($sql, $params, $options);
    }



    public function getTable(string $name): Table
    {
        $table = new Table($this, $name);

        return $table;
    }



    public function __destruct()
    {
        $this->driver->disconnect();
    }



    public function connect(): self
    {
        if ($this->driver) {
            $this->driver->disconnect();
        }

        $connectionOptions = $this->getOption(self::OPTION_CONNECTION . '/' . $this->connection, []);

        $driverClass  = isset($connectionOptions['driver']) ? $connectionOptions['driver'] : 'Postgresql';
        $driverClass  = "Unicaen\BddAdmin\Driver\\$driverClass\Driver";
        $this->driver = new $driverClass($this);
        $this->driver->connect($connectionOptions);

        return $this;
    }



    public function manager(string $name): ManagerInterface
    {
        $ddlClass = $this->driver->getDdlClass($name);
        if (!$ddlClass) {
            $ddlClass = VoidManager::class;
        }

        if (!is_subclass_of($ddlClass, ManagerInterface::class)) {
            throw new \Exception($ddlClass . ' n\'est pas un objet DDL valide!!');
        }

        if (!isset($this->managers[$ddlClass])) {
            $this->managers[$ddlClass] = new $ddlClass($this);
        }

        return $this->managers[$ddlClass];
    }



    /**
     * Retourne la liste des managers de la base de données
     */
    public function managerList(): array
    {
        $managers = [];
        foreach (Ddl::types() as $type => $libelle) {
            $manager = $this->manager($type);
            if (!$manager instanceof VoidManager) {
                $managers[$type] = $libelle;
            }
        }

        return $managers;
    }



    public function normalizeObjectName(string|array $object): string
    {
        if (is_string($object)) {
            [$schema, $name] = Util::explodedFullObjectName($object);
        } else {
            if (isset($object['name'])) {
                throw new BddException('Propriété d\'objet "name" non fournie');
            }
            $schema = $object['schema'] ?? null;
            $name   = $object['name'];
        }

        return Util::fullObjectName($schema, $name);
    }



    public function data(): DataManager
    {
        if (!isset($this->data)) {
            $this->data = new DataManager($this);
            $this->data->setOptions($this->getOption(self::OPTION_DATA, []));
        }

        return $this->data;
    }



    public function migration(): MigrationManager
    {
        if (!isset($this->migration)) {
            $this->migration = new MigrationManager();
            $this->migration->setBdd($this);
            $this->migration->setOptions($this->getOption(self::OPTION_MIGRATION, []));
        }

        return $this->migration;
    }



    public function schema(): SchemaManagerInterface
    {
        return $this->manager(Ddl::SCHEMA);
    }



    public function index(): IndexManagerInterface
    {
        return $this->manager(Ddl::INDEX);
    }



    public function materializedView(): MaterializedViewManagerInteface
    {
        return $this->manager(Ddl::MATERIALIZED_VIEW);
    }



    public function procedure(): ProcedureManagerInteface
    {
        return $this->manager(Ddl::PROCEDURE);
    }



    public function function (): FunctionManagerInteface
    {
        return $this->manager(Ddl::FUNCTION);
    }



    public function package(): PackageManagerInteface
    {
        return $this->manager(Ddl::PACKAGE);
    }



    public function primaryConstraint(): PrimaryConstraintManagerInterface
    {
        return $this->manager(Ddl::PRIMARY_CONSTRAINT);
    }



    public function refConstraint(): RefConstraintManagerInterface
    {
        return $this->manager(Ddl::REF_CONSTRAINT);
    }



    public function sequence(): SequenceManagerInterface
    {
        return $this->manager(Ddl::SEQUENCE);
    }



    public function table(): TableManagerInterface
    {
        return $this->manager(Ddl::TABLE);
    }



    public function trigger(): TriggerManagerInterface
    {
        return $this->manager(Ddl::TRIGGER);
    }



    public function uniqueConstraint(): UniqueConstraintManagerInterface
    {
        return $this->manager(Ddl::UNIQUE_CONSTRAINT);
    }



    public function view(): ViewManagerInterface
    {
        return $this->manager(Ddl::VIEW);
    }



    public function getFiltersForUpdateBdd(Ddl $ddl): DdlFilters
    {
        $filters = new DdlFilters();
        $filters->addArray($ddl->getOption(Ddl::OPTION_FILTERS));
        $filters->addArray($ddl->getOption(Ddl::OPTION_UPDATE_BDD_FILTERS));

        if ($ddl->getOption(Ddl::OPTION_INIT_UPDATE_BDD_FILTERS, true)) {
            $filters->setExplicit(true);

            // On ajoute toute la DDL qu'on donne en référence
            $filters->addDdl($ddl);

            // On ajoute toutes les dépendances des tables qu'on a inclu dans la DDL au cas où elles ne seraient pas listées
            $filters->addTablesDeps($this);
        }

        return $filters;
    }



    public function getFiltersForUpdateDdl(): DdlFilters
    {
        $filters = new DdlFilters();
        $filters->addArray($this->getOption(self::OPTION_DDL . '/' . Ddl::OPTION_FILTERS));
        $filters->addArray($this->getOption(self::OPTION_DDL . '/' . Ddl::OPTION_UPDATE_DDL_FILTERS));

        return $filters;
    }



    public function getNewDdl(): Ddl
    {
        $ddl = new Ddl();
        $ddl->setOptions($this->getOption(self::OPTION_DDL, []));

        return $ddl;
    }



    public function getDdl(DdlFilters|array $filters = []): Ddl
    {
        $this->logBegin("Récupération de la DDL");
        $filters  = DdlFilters::normalize($filters);
        $ddl      = $this->getNewDdl();
        $managers = $this->managerList();
        foreach ($managers as $type => $libelle) {
            if (!($filters->isExplicit() && $filters->get($type)->isEmpty())) {
                $this->logMsg('Traitement des ' . $libelle . ' ...', true);
                $manager = $this->manager($type);
                $ddl->set($type, $manager->get($filters[$type]));
            }
        }
        $this->logEnd();

        $ddl->applyColumnPositions();

        return $ddl;
    }



    public function getRefDdl(): Ddl
    {
        $ddl = $this->getNewDdl();
        $ddl->loadFromDir();

        return $ddl;
    }



    protected function alterDdlObject(ManagerInterface $manager, string $action, array $kold, array $knew): array
    {
        $this->queryCollect = true;
        $this->queries      = [];

        $renames = [];
        foreach ($kold as $koldIndex => $koldData) {
            if (isset($koldData['name'])) {
                $koldName = $koldData['name'];
                $koldData = $manager->prepareRenameCompare($koldData);
                unset($koldData['name']);
                foreach ($knew as $knewIndex => $knewData) {
                    if (isset($knewData['name'])) {
                        $knewName = $knewData['name'];
                        $knewData = $manager->prepareRenameCompare($knewData);
                        if ($koldName !== $knewName) {
                            if ($koldData == $knewData) {
                                $renames[$koldIndex] = $knew[$knewIndex];
                                unset($kold[$koldIndex]);
                                unset($knew[$knewName]);
                            }
                        }
                    }
                }
            }
        }

        switch ($action) {
            case 'rename':
                $names = $renames;
                break;
            case 'create':
                $names = array_diff(array_keys($knew), array_keys($kold));
                break;
            case 'alter':
                $names = array_intersect(array_keys($kold), array_keys($knew));
                break;
            case 'drop':
                $names = array_diff(array_keys($kold), array_keys($knew));
                break;
        }

        foreach ($names as $oldName => $name) {
            switch ($action) {
                case 'rename':
                    $manager->rename($oldName, $name);
                    break;
                case 'drop':
                    $manager->drop($kold[$name]);
                    break;
                case 'alter':
                    $manager->alter($kold[$name], $knew[$name]);
                    break;
                case 'create':
                    $manager->create($knew[$name]);
                    break;
            }
        }
        $this->queryCollect = false;

        return $this->queries;
    }



    public function create(Bdd|Ddl|array $ddl, DdlFilters|array $filters = []): void
    {
        $filters = DdlFilters::normalize($filters);
        if ($ddl instanceof self) {
            $ddl = $ddl->getDdl($filters);
        } else {
            $ddl = Ddl::normalize($ddl);
            $ddl->filter($filters);
        }

        foreach ($this->changements as $changement => $label) {
            [$ddlName, $action] = explode('.', $changement);
            if ($action == 'create') {
                $manager = $this->manager($ddlName);
                if ((!$manager instanceof VoidManager) && isset($ddl[$ddlName])) {
                    $queries = $this->alterDdlObject($manager, $action, [], $ddl[$ddlName]);
                    if ($queries) {
                        $this->logBegin($label);
                        $this->execQueries($queries);
                        $this->logEnd();
                    }
                }
            }
        }
    }



    public function execQueries(array $queries): void
    {
        foreach ($queries as $query => $desc) {
            $this->logMsg($desc);
            try {
                $this->exec($query);
            } catch (BddCompileException $e) {
                // ne rien faire => trité après
            } catch (\Throwable $e) {
                $this->logError($e);
            }
        }
    }



    public function alter(Bdd|Ddl|array $ddl, DdlFilters|array $filters = []): void
    {
        $filters = DdlFilters::normalize($filters);

        if ($ddl instanceof self) {
            if (!$ddl->getLogger() && $this->getLogger()) {
                $ddl->setLogger($this->getLogger());
            }
            $ddl = $ddl->getDdl($filters);
        } else {
            $ddl = Ddl::normalize($ddl)->filter($filters);
        }

        foreach ($this->changements as $changement => $label) {
            [$ddlName, $action] = explode('.', $changement);

            $manager = $this->manager($ddlName);
            if (!$manager instanceof VoidManager) {
                $objectFilter = $filters->get($ddlName);

                if (!($filters->isExplicit() && $objectFilter->isEmpty())) {
                    $objectDdl = isset($ddl[$ddlName]) ? $ddl[$ddlName] : [];
                    $this->logMsg("Préparation de l'action \"$label\" ...", true);
                    $queries = $this->alterDdlObject($manager, $action, $manager->get($objectFilter), $objectDdl);
                    if ($queries) {
                        $this->logBegin($label);
                        $this->execQueries($queries);
                        $this->logEnd();
                    }
                }

            }
        }
        $this->logEnd();
    }



    public function drop(DdlFilters|array $filters = []): self
    {
        $filters = DdlFilters::normalize($filters);

        foreach ($this->changements as $changement => $label) {
            [$ddlName, $action] = explode('.', $changement);

            if ($action == 'drop' && !($filters->isExplicit() && $filters->get($ddlName)->isEmpty())) {
                $manager = $this->manager($ddlName);
                if (!$manager instanceof VoidManager) {
                    $ddl = $manager->get($filters->get($ddlName));
                    if (!empty($ddl)) {
                        $queries = $this->alterDdlObject($manager, 'drop', $ddl, []);
                        if ($queries) {
                            $this->logBegin($label);
                            $this->execQueries($queries);
                            $this->logEnd();
                        }
                    }
                }
            }
        }
        return $this;
    }



    public function clear(): self
    {
        $this->logTitle('Vidage complet de la base de données');
        try {
            $this->drop();
        } catch (\Throwable $e) {
            $this->logError($e);
        }
        $this->logSuccess('Base de données vidée complètement');
        return $this;
    }



    public function install(bool $withData = true): self
    {
        $this->logTitle('Installation de la base de données');

        // Mise en place des objets
        try {
            $ddl = $this->getRefDdl();
            $this->create($ddl);
            $this->logSuccess('Objets en place');
        } catch (\Throwable $e) {
            $this->logError($e);
        }

        if ($withData && !empty($this->data()->getSources())) {
            // Installation des données
            $this->logTitle('Insertion du jeu de données données');
            try {
                $this->data()->run(DataManager::ACTION_INSTALL);
                $this->logSuccess('Données écrites');
            } catch (\Throwable $e) {
                $this->logError($e);
            }
        }

        return $this;
    }



    public function update(bool $withData = true, bool $withMigration = true): self
    {
        $this->logTitle('Mise à jour de la base de données');

        $ddl     = $this->getRefDdl();
        $filters = $this->getFiltersForUpdateBdd($ddl);

        if ($withMigration) {
            // Initialisation et lancement de la pré-migration
            $migrationManager = $this->migration();

            $migrationManager->init($ddl, $filters);
            $migrationManager->run(MigrationManager::ACTION_BEFORE);
        }

        try {
            $this->alter($ddl, $filters, true);
            $this->logSuccess('Objets à jour');
        } catch (\Throwable $e) {
            $this->logError($e);
        }

        // On compile tout
        $this->compilerTout();


        // Mise à jour des séquences
        $this->majSequences($ddl);


        // Mise à jour des données
        if ($withData) {
            $this->updateData();
        }


        if ($withMigration) {
            // Post-migration
            $migrationManager->run(MigrationManager::ACTION_AFTER);
        }

        return $this;
    }



    public function updateData(): self
    {
        if (!empty($this->data()->getSources())) {
            $this->logTitle('Contrôle et mise à jour des données');
            try {
                $this->data()->run(DataManager::ACTION_UPDATE);
                $this->logSuccess('Données à jour');
            } catch (\Throwable $e) {
                $this->logError($e);
            }
        }
        return $this;
    }



    public function updateDdl(): self
    {
        $this->logTitle('Génération de la DDL à partir de la base de données');

        try {
            $filters = $this->getFiltersForUpdateDdl();
            $ddl     = $this->getDdl($filters);
            $ddl->saveToDir();
            $this->logSuccess('DDL mise à jour');
        } catch (\Throwable $e) {
            $this->logError($e);
        }

        return $this;
    }



    public function diff(Bdd|Ddl|array $ddl, DdlFilters|array $filters = [], bool $inverse = false): DdlDiff
    {
        $this->logBegin('Génération du différentiel de DDLs');
        if ($ddl instanceof self) {
            $ddl = $ddl->getDdl($filters);
        } else {
            $ddl = Ddl::normalize($ddl)->filter($filters);
        }

        $bdd = $this->getDdl($filters);

        if (!$inverse) {
            $old = $bdd;
            $new = $ddl;
        } else {
            $old = $ddl;
            $new = $bdd;
        }
        $diff = new DdlDiff();
        $cc   = count($this->changements);
        $c    = 0;
        foreach ($this->changements as $changement => $label) {
            [$ddlName, $action] = explode('.', $changement);
            $manager = $this->manager($ddlName);
            if (!$manager instanceof VoidManager) {
                $c++;
                $this->logMsg($label . " (opération $c/$cc) ...", true);
                $queries = $this->alterDdlObject($manager, $action, $old[$ddlName] ?: [], $new[$ddlName] ?: []);
                if (!empty($queries)) {
                    $diff->set($changement, $queries);
                }
            }
        }
        $this->logEnd();

        return $diff;
    }



    public function diffDdl(Bdd|Ddl|array $src, Bdd|Ddl|array $dest, DdlFilters|array $filters = []): DdlDiff
    {
        if ($src instanceof self) {
            $src = $src->getDdl($filters);
        } else {
            $src = Ddl::normalize($src)->filter($filters);
        }

        if ($dest instanceof self) {
            $dest = $dest->getDdl($filters);
        } else {
            $dest = Ddl::normalize($dest)->filter($filters);
        }

        $diff = new DdlDiff();
        $cc   = count($this->changements);
        $c    = 0;
        foreach ($this->changements as $changement => $label) {
            [$ddlName, $action] = explode('.', $changement);
            $manager = $this->manager($ddlName);
            if (!$manager instanceof VoidManager) {
                $c++;
                $queries = $this->alterDdlObject($manager, $action, $src[$ddlName] ?: [], $dest[$ddlName] ?: []);
                if (!empty($queries)) {
                    $diff->set($changement, $queries);
                }
            }
        }

        return $diff;
    }



    public function majSequences(Bdd|Ddl|array $ddl = null): void
    {
        if (!$ddl) {
            $ddl = $this->table()->get();
        } else {
            $ddl = Ddl::normalize($ddl)->get(Ddl::TABLE);
            if (!$ddl) $ddl = [];
        }

        $this->logBegin("Mise à jour de toutes les séquences");
        foreach ($ddl as $tdata) {
            try {
                $this->logMsg("Séquence " . $tdata['sequence'] . " ...", true);
                $this->table()->majSequence($tdata);
            } catch (\Throwable $e) {
                $this->logError($e);
            }
        }
        $this->logEnd();
    }



    public function refreshMaterializedViews($ddl = null): void
    {
        if (!$ddl) {
            $ddl = $this->materializedView()->get();
        } else {
            $ddl = Ddl::normalize($ddl)->get(Ddl::MATERIALIZED_VIEW);
            if (!$ddl) $ddl = [];
        }

        $this->logBegin("Recalcul de toutes les vues matérialisées");
        foreach ($ddl as $mv) {
            try {
                $this->logMsg("Vue matérialisée " . $mv['name'] . " ...", true);
                $this->materializedView()->refresh($mv);
            } catch (\Throwable $e) {
                $this->logError($e);
            }
        }
        $this->logEnd();
    }



    public function compilerTout(): void
    {
        /** @var CompilableInterface[] $compilables */
        $compilables = [];

        // Récupération de la liste des managers compilables
        $managers = $this->managerList();
        foreach ($managers as $name => $null) {
            $manager = $this->manager($name);
            if ($manager instanceof CompilableInterface) {
                $compilables[$name] = $manager;
            }
        }

        // S'il n'y en a pas on ne fait rien
        if (empty($compilables)) {
            return;
        }

        $this->logBegin("Compilation de tous les objets de la BDD");
        $this->queryCollect = true;

        $this->queries = [];
        foreach ($compilables as $managerName => $manager) {
            $manager->compilerTout();
            $this->execQueries($this->queries);
            $this->queries = [];
            $this->logMsg(" ");
        }

        $this->queryCollect = false;
        $this->logEnd("Fin de la compilation");
    }



    public function isInCopy(): bool
    {
        return $this->inCopy;
    }



    public function copy(Bdd|string $source, DdlFilters|array $filters = [], array $fncs = []): self
    {
        if (is_string($source)) {
            $source = $this->getBdd($source);
        }
        if ($this->getLogger() && !$source->getLogger()) {
            $source->setLogger($this->getLogger());
        }

        $this->logBegin("Duplication d'une base de données");

        $excludes = [];
        foreach ($fncs as $table => $fnc) {
            if (false === $fnc) {
                $excludes[] = $table;
            }
        }
        $schDdl = $source->schema()->get();
        $sDdl   = $source->sequence()->get();
        $tDdl   = $source->table()->get(null, $excludes);

        $this->drop();
        $this->create([Ddl::SCHEMA => $schDdl, Ddl::SEQUENCE => $sDdl, Ddl::TABLE => $tDdl]);
        $this->inCopy = true;

        $this->logBegin("Copie des données");
        $this->logMsg('');

        $tables = array_keys($tDdl);
        sort($tables);
        foreach ($tables as $table) {
            $fnc = isset($fncs[$table]) ? $fncs[$table] : null;
            if (false !== $fnc) {
                $this->getTable($table)->copy($source, $fnc);
            }
        }
        $this->logMsg('');
        $this->logEnd("Copie terminée");

        $this->inCopy = false;

        // pas de recopie d'éléments déjà traités avant la copie
        if ($filters instanceof DdlFilters) {
            $filters = $filters->toArray();
        }
        $filters[Ddl::SCHEMA]   = ['excludes' => '%'];
        $filters[Ddl::SEQUENCE] = ['excludes' => '%'];
        $filters[Ddl::TABLE]    = ['excludes' => '%'];

        $this->create($source, $filters);

        $this->majSequences();

        $this->logEnd();

        return $this;
    }



    public function copyTo(string|Bdd $destination, DdlFilters|array $filters = [], array $fncs = []): self
    {
        if (is_string($destination)) {
            $destination = $this->getBdd($destination);
        }
        $destination->copy($this, $filters, $fncs);

        return $this;
    }



    public function save(string $filename, DdlFilters|array $filters = [], array $fncs = []): void
    {
        $this->logBegin("Sauvegarde de la base de données");

        if (file_exists($filename)) {
            throw new Exception('Le fichier existe déjà, merci de le supprimer ou bien de trouver un nouveau nom');
        }

        if (!class_exists('ZipArchive')) {
            throw new Exception('Zip extension not loaded');
        }

        if ($filters instanceof DdlFilters) {
            $filters = $filters->toArray();
        }

        $archive = new \ZipArchive();
        $archive->open($filename, \ZipArchive::CREATE);

        foreach ($fncs as $table => $fnc) {
            if (false === $fnc) {
                if (!isset($filters['table']['excludes'])) {
                    $filters['table']['excludes'] = [];
                }
                $filters['table']['excludes'][] = $table;
            }
        }
        $ddl = $this->getDdl($filters);

        $archive->addFromString('bdd.ddl', $ddl->saveToString());

        $tables = array_keys($ddl->get(Ddl::TABLE));
        sort($tables);
        $tmpNames = [];
        foreach ($tables as $table) {
            $fnc = isset($fncs[$table]) ? $fncs[$table] : null;
            if (false !== $fnc) {
                $tmpname    = tempnam(sys_get_temp_dir(), uniqid());
                $tmpNames[] = $tmpname;
                $this->getTable($table)->save($tmpname, $fnc);
                if (file_exists($tmpname)) {
                    $archive->addFile($tmpname, $table . '.tbl');
                }
            }
        }

        $archive->close();
        foreach ($tmpNames as $tmpName) {
            if (file_exists($tmpName)) {
                unlink($tmpName);
            }
        }
        $this->logEnd();
    }



    public function load(string $filename, DdlFilters|array $filters = [], array $fncs = []): void
    {
        $this->logBegin("Restauration de la base de données");

        if (!file_exists($filename)) {
            throw new \Exception("Le fichier $filename n\'existe pas ou bien il n'a pas été trouvé");
        }

        $tmpPath = tempnam(sys_get_temp_dir(), uniqid());
        unlink($tmpPath);

        $archive = new \ZipArchive();
        if (true !== $archive->open($filename)) {
            throw new Exception('La sauvegarde n\'est pas lisible');
        }
        $archive->extractTo($tmpPath);
        $archive->close();

        $ddl = new Ddl();
        $ddl->loadFromFile($tmpPath . '/bdd.ddl');
        $ddl->filter($filters);

        $schDdl = $ddl->get(Ddl::SCHEMA);
        $sDdl   = $ddl->get(Ddl::SEQUENCE);
        $tDdl   = $ddl->get(Ddl::TABLE);

        $this->drop();
        $this->create([Ddl::SCHEMA => $schDdl, Ddl::SEQUENCE => $sDdl, Ddl::TABLE => $tDdl]);

        $tables = array_keys($tDdl);

        sort($tables);
        $this->inCopy = true;
        foreach ($tables as $table) {
            if (file_exists($tmpPath . '/' . $table . '.tbl')) {
                $fnc = isset($fncs[$table]) ? $fncs[$table] : null;
                if (false !== $fnc) {
                    $this->getTable($table)->load($tmpPath . '/' . $table . '.tbl', $fnc);
                }
            }
        }
        $this->inCopy = false;
        if ($filters instanceof DdlFilters) {
            $filters = $filters->toArray();
        }
        $filters[Ddl::SCHEMA]   = ['excludes' => '%'];
        $filters[Ddl::SEQUENCE] = ['excludes' => '%'];
        $filters[Ddl::TABLE]    = ['excludes' => '%'];

        $this->create($ddl, $filters);

        $this->majSequences($ddl);

        $this->logEnd();

        $dir = opendir($tmpPath);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                unlink($tmpPath . '/' . $file);
            }
        }
        closedir($dir);
        rmdir($tmpPath);
    }
}