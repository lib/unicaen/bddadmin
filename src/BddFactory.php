<?php

namespace Unicaen\BddAdmin;

use Psr\Container\ContainerInterface;
use Unicaen\BddAdmin\Data\DataManager;

/**
 * Description of BddFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class BddFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return Bdd
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): Bdd
    {
        if (Bdd::class == $requestedName){
            $configKey = 'unicaen-bddadmin';
        }else{
            $configKey = $requestedName;
        }
        $config = $container->get('config')['unicaen-bddadmin'];

        $bdd = new Bdd($config);
        $bdd->__container = $container;

        return $bdd;
    }
}