<?php

namespace Unicaen\BddAdmin\Command;

use Psr\Container\ContainerInterface;
use Unicaen\BddAdmin\Bdd;


/**
 * Description of CommandFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class CommandFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return InstallBddCommand
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $command = new $requestedName;
        $command->setBdd($container->get(Bdd::class));

        return $command;
    }
}