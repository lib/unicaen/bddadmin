<?php

namespace Unicaen\BddAdmin\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Data\DataManager;
use Unicaen\BddAdmin\Ddl\Ddl;

/**
 * Description of CopyFromCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class CopyFromCommand extends Command
{
    use BddAwareTrait;

    protected function configure(): void
    {
        $this
            ->setDescription('Copie depuis une autre BDD')
            ->addArgument('source', InputArgument::OPTIONAL, 'Nom de la BDD d\'origine');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);
        $bdd = $this->getBdd()->setLogger($io);

        $bdds = $bdd->getBdds();

        if (empty($bdds)) {
            $io->error('Aucune base de donnée possible n\'est en source : copie impossible');
            $io->info('Veuillez ajouter d\'autres connexions en configuration dans unicaen-bddadmin/connection');
            return Command::INVALID;
        }

        $source = $input->getArgument('source');

        if (!$source) {
            $source = $io->choice(
                'Veuillez choisir une base de données source',
                $bdd->getBdds()
            );
        }

        // Lance la copie
        $bdd->copy($source);

        return Command::SUCCESS;
    }
}