<?php

namespace Unicaen\BddAdmin\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Data\DataManager;
use Unicaen\BddAdmin\Ddl\Ddl;

/**
 * Description of CopyToCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class CopyToCommand extends Command
{
    use BddAwareTrait;

    protected function configure(): void
    {
        $this
            ->setDescription('Copie vers une autre BDD')
            ->addArgument('destination', InputArgument::OPTIONAL, 'Nom de la BDD de destination');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);
        $bdd = $this->getBdd()->setLogger($io);

        $bdds = $bdd->getBdds();

        if (empty($bdds)) {
            $io->error('Aucune base de donnée possible n\'est en destination : copie impossible');
            $io->info('Veuillez ajouter d\'autres connexions en configuration dans unicaen-bddadmin/connection');
            return Command::INVALID;
        }

        $destination = $input->getArgument('destination');

        if (!$destination) {
            // Si l'argument n'est pas fourni, on demande à l'utilisateur de choisir une valeur
            $destination = $io->choice(
                'Veuillez choisir une base de données de destination',
                $bdd->getBdds()
            );
        }

        $bdd->copyTo($destination);

        return Command::SUCCESS;
    }
}