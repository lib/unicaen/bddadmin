<?php

namespace Unicaen\BddAdmin\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Data\DataManager;
use Unicaen\BddAdmin\Ddl\Ddl;
use Unicaen\BddAdmin\Util;

/**
 * Description of LoadCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class LoadCommand extends Command
{
    use BddAwareTrait;

    protected function configure(): void
    {
        $this->setDescription('Chargement de la base de données depuis un fichier')
             ->addArgument('filename', InputArgument::REQUIRED, 'Chemin du fichier de sauvegarde');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);
        $bdd = $this->getBdd()->setLogger($io);

        $filename = $input->getArgument('filename');

        if (!str_starts_with($filename, DIRECTORY_SEPARATOR)){
            $filename = Util::getPrintWorkingDirectory().DIRECTORY_SEPARATOR.$filename;
        }

        if (!file_exists($filename)){
            $io->error('Le fichier '.$filename.' n\'existe pas');
            return Command::FAILURE;
        }

        $bdd->load($filename);

        return Command::SUCCESS;
    }
}