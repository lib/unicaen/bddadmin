<?php

namespace Unicaen\BddAdmin\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unicaen\BddAdmin\BddAwareTrait;

/**
 * Description of UpdateDdlCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class UpdateDdlCommand extends Command
{
    use BddAwareTrait;

    protected function configure(): void
    {
        $this->setDescription('Met à jour la DDL à partir des définitions de la base de données');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);
        $bdd = $this->getBdd()->setLogger($io);

        $bdd->updateDdl();

        return Command::SUCCESS;
    }
}