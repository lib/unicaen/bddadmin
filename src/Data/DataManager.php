<?php

namespace Unicaen\BddAdmin\Data;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Trait\OptionsTrait;

class DataManager
{
    use BddAwareTrait;
    use OptionsTrait;

    const OPTION_SOURCES = 'sources';
    const OPTION_ACTIONS = 'actions';
    const OPTION_CONFIG  = 'config';

    const ACTION_INSTALL = 'install';
    const ACTION_UPDATE  = 'update';

    private array $sourcesData = [];



    public function __construct(Bdd $bdd)
    {
        $this->setBdd($bdd);
    }



    public function run(string $action, ?string $table = null): DataManager
    {
        if (!$this->hasAction($action)) {
            throw new \Exception('Action "' . $action . '" inconnue');
        }

        if ($table && !$this->hasConfig($table)) {
            throw new \Exception('La table "' . $table . '" n\'est pas de directives de configuration : action impossible à réaliser');
        }

        $config = $this->getOption(self::OPTION_CONFIG);
        if ($table) {
            $config = [$table => $config[$table]];
        }

        foreach ($config as $table => $config) {
            $actions = (array)$config['actions'];
            unset($config['actions']);
            if (in_array($action, $actions)) {
                $this->syncTable($table, $action, $config);
            }
        }

        return $this;
    }



    private function syncTable(string $table, string $action, array $config)
    {
        $tableObject = $this->getBdd()->getTable($table);
        $ddl         = $tableObject->getDdl();

        $idCol = $this->getBdd()->getOption(Bdd::OPTION_ID_COLUMN);

        $data    = null;
        $sources = $this->getSources();
        foreach ($sources as $i => $source) {
            if (is_array($source) && isset($source[$table])) {
                $data = $source[$table];
                break;
            }
            $methodName = str_replace('.', '_', $table);
            if (is_string($source) && class_exists($source)) {
                $source = $this->getBdd()->__newClass($source);
            }

            if (is_object($source) && method_exists($source, $methodName)) {
                $data = $source->$methodName($action);
                break;
            }
            if (is_string($source)) {
                if (!isset($this->sourcesData[$i]) && file_exists($source)) {
                    $this->sourcesData[$i] = require $source;
                }
                if (isset($this->sourcesData[$i][$table])) {
                    $data = $this->sourcesData[$i][$table];
                    break;
                }
            }
        }

        if (null === $data) {
            throw new \Exception('Données sources introuvables pour la table "' . $table . '"');
        }
        if (!is_array($data)) {
            throw new \Exception('Les données sources de la table "' . $table . '" doivent être présentées sous forme de tableau de lignes, chaque ligne étant un tableau de colonnes');
        }

        $result = $tableObject->merge(
            $data,
            $config['key'] ?? $idCol,
            $config['options'] ?? []
        );
        if ($result['insert'] + $result['update'] + $result['delete'] + ($result['undelete'] ?? 0) + ($result['soft-delete'] ?? 0) > 0) {
            $msg = str_pad($table, 31, ' ');
            $msg .= 'Insert: ' . ($result['insert'] + ($result['undelete'] ?? 0)) . ', Update: ' . $result['update'] . ', Delete: ' . ($result['delete'] + ($result['soft-delete'] ?? 0));
            $this->getBdd()->logMsg($msg);
        }
    }



    public function getConfig(): array
    {
        return $this->getOption(self::OPTION_CONFIG);
    }



    public function hasConfig(string $tableName): bool
    {
        $config = $this->getConfig();
        return array_key_exists($tableName, $config);
    }



    /**
     * @return string[]|array[]|Object[]
     */
    public function getSources(): array
    {
        return $this->getOption(self::OPTION_SOURCES);
    }



    public function addSource(string|array|object $source): DataManager
    {
        $sources   = $this->getSources();
        $sources[] = $source;
        $this->setOptions(self::OPTION_SOURCES, $sources);

        return $this;
    }



    /**
     * @return array|string[]
     */
    public function getActions(): array
    {
        return $this->getOption(self::OPTION_ACTIONS);
    }



    public function hasAction(string $action): bool
    {
        $actions = $this->getActions();
        return array_key_exists($action, $actions);
    }



    public function addAction(string $name, string $libelle): DataManager
    {
        $actions        = $this->getActions();
        $actions[$name] = $libelle;
        $this->setOptions(self::OPTION_ACTIONS, $actions);

        return $this;
    }

}