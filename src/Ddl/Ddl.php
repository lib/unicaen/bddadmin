<?php

namespace Unicaen\BddAdmin\Ddl;

use Unicaen\BddAdmin\Driver\Postgresql\FunctionManager;
use Unicaen\BddAdmin\Driver\Postgresql\ProcedureManager;
use Unicaen\BddAdmin\Trait\OptionsTrait;
use Iterator;
use ArrayAccess;
use Unicaen\BddAdmin\Util;

class Ddl implements Iterator, ArrayAccess
{
    use OptionsTrait;

    const TABLE              = 'table';
    const VIEW               = 'view';
    const SEQUENCE           = 'sequence';
    const MATERIALIZED_VIEW  = 'materialized-view';
    const PRIMARY_CONSTRAINT = 'primary-constraint';
    const FUNCTION           = 'function';
    const PROCEDURE          = 'procedure';
    const PACKAGE            = 'package';
    const REF_CONSTRAINT     = 'ref-constraint';
    const INDEX              = 'index';
    const UNIQUE_CONSTRAINT  = 'unique-constraint';
    const TRIGGER            = 'trigger';
    const SCHEMA             = 'schema';

    const OPTION_DIR                     = 'dir';
    const OPTION_COLUMNS_POSITIONS_FILE  = 'columns_positions_file';
    const OPTION_FILTERS                 = 'filters';
    const OPTION_UPDATE_BDD_FILTERS      = 'update-bdd-filters';
    const OPTION_UPDATE_DDL_FILTERS      = 'update-ddl-filters';
    const OPTION_INIT_UPDATE_BDD_FILTERS = 'init-update-bdd-filters';

    protected array $data = [];



    public function getDir(): ?string
    {
        return $this->getOption(self::OPTION_DIR);
    }



    public function setDir(?string $dir): Ddl
    {
        return $this->setOption(self::OPTION_DIR, $dir);
    }



    public function getColumnsPositionsFile(): ?string
    {
        return $this->getOption(self::OPTION_COLUMNS_POSITIONS_FILE);
    }



    public function setColumnsPositionsFile(?string $columnsPositionsFile): Ddl
    {
        return $this->setOption(self::OPTION_COLUMNS_POSITIONS_FILE, $columnsPositionsFile);
    }



    public function get(string $name): ?array
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        return null;
    }



    public static function types(): array
    {
        return [
            self::SEQUENCE           => 'séquences',
            self::TABLE              => 'tables',
            self::PRIMARY_CONSTRAINT => 'contraintes de clés primaires',
            self::FUNCTION           => 'fonctions',
            self::PROCEDURE          => 'procédures',
            self::PACKAGE            => 'packages',
            self::VIEW               => 'vues',
            self::MATERIALIZED_VIEW  => 'vues matérialisées',
            self::REF_CONSTRAINT     => 'contraintes de clés étrangères',
            self::UNIQUE_CONSTRAINT  => 'contraintes d\'unicité',
            self::TRIGGER            => 'déclencheurs',
            self::INDEX              => 'indexs',
            self::SCHEMA             => 'schémas',
        ];
    }



    public function set(string $name, ?array $data): self
    {
        $this->data[$name] = $data;

        return $this;
    }



    public function has(string $name): bool
    {
        return array_key_exists($name, $this->data);
    }



    public function toArray(): array
    {
        return $this->data;
    }



    public static function normalize($data): self
    {
        if ($data instanceof self) {
            return $data;
        }

        $ddl = new self;
        if (is_array($data)) {
            $ddl->data = $data;
        }

        return $ddl;
    }



    public function current(): mixed
    {
        return current($this->data);
    }



    public function next(): void
    {
        next($this->data);
    }



    public function key(): string
    {
        return key($this->data);
    }



    public function valid(): bool
    {
        return key($this->data) !== null;
    }



    public function rewind(): void
    {
        reset($this->data);
    }



    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }



    public function offsetGet($offset): mixed
    {
        return $this->get($offset);
    }



    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }



    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }



    /**
     * Applique un filtre à la DDL : en retire tout objet qui ne passe pas le filtre
     */
    public function filter(DdlFilters|array|string|null $filters): self
    {
        $filters = DdlFilters::normalize($filters);

        foreach ($this->data as $ddlType => $ddlConf) {
            if (is_array($ddlConf)) {
                foreach ($ddlConf as $name => $null) {
                    if (!$filters[$ddlType]->match($name)) {
                        unset($this->data[$ddlType][$name]);
                    }
                }
            }
        }

        return $this;
    }



    /**
     *
     * Crée un filtre à partir de la DDL
     * Permet de ne modifier une base de donnée existante que sur le périmètre de la DDL courante sans toucher aux
     * autres objets
     */
    public function makeFilters(): DdlFilters
    {
        $filters = new DdlFilters();
        $filters->setExplicit(true);
        foreach ($this->data as $ddlType => $ddlConf) {
            foreach ($ddlConf as $name => $null) {
                $filters->get($ddlType)->addInclude($name);
            }
        }

        return $filters;
    }



    /**
     * On passe un tableau de positions de colonnes ou bien, on utilise le fichier de config des positions,
     * et cela réorganise l'ordonnancement.
     *
     * @param array|null $positions
     *
     * @return array
     */
    public function applyColumnPositions(array|null $positions = null): array
    {
        if (null === $positions) {
            $colPosFile = $this->getColumnsPositionsFile();
            if ($colPosFile && file_exists($colPosFile)) {
                $positions = require $colPosFile;
                if (!is_array($positions)) {
                    throw new \exception('Le fichier des positions doit retourner un tableau PHP');
                }
            }
        }

        if (null === $positions) {
            $positions = [];
        }

        $tables = $this->get(Ddl::TABLE);
        foreach ($tables as $tableName => $table) {
            $cols = [];
            // on supprime les positions pour des colonnes qui n'existent pas ou plus
            if (isset($positions[$tableName])) {
                foreach ($positions[$tableName] as $idc => $col) {
                    if (!array_key_exists($col, $table['columns'])) {
                        unset($positions[$tableName][$idc]);
                    }
                }
            }

            // On récupère les colonnes qui n'ont pas de position définie explicitement
            foreach ($table['columns'] as $column) {
                $colname = $column['name'];
                if (!isset($positions[$tableName]) || !in_array($colname, $positions[$tableName])) {
                    $cols[$colname] = $column['position'];
                }
            }
            asort($cols);
            $cols = array_keys($cols);

            if (!isset($positions[$tableName])) {
                $positions[$tableName] = [];
            }
            // on a la liste des colonnes ordonnée
            $cols = array_merge($positions[$tableName], $cols);

            // et on l'applique maintenant
            foreach ($cols as $pos => $col) {
                if (isset($this->data[Ddl::TABLE][$tableName]['columns'][$col])) {
                    $this->data[Ddl::TABLE][$tableName]['columns'][$col]['position'] = $pos + 1;
                }
            }

            $positions[$tableName] = $cols;
        }

        return $positions;
    }



    public function orderTabCols(): self
    {

        if (!$this->has(Ddl::TABLE)) return $this;

        foreach ($this->data[Ddl::TABLE] as $table) {
            $columns = $table['columns'];
            uasort($columns, function ($a, $b) {
                return $a['position'] - $b['position'];
            });

            $this->data[Ddl::TABLE][$table['name']]['columns'] = $columns;
        }

        return $this;
    }



    public function writeArray(string $filename, array $data): self
    {
        $ddlString = "<?php\n\n//@" . "formatter:off\n\nreturn " . Util::arrayExport($data) . ";\n\n//@" . "formatter:on\n";

        file_put_contents($filename, $ddlString);

        return $this;
    }



    public function saveToString(): string
    {
        $data = $this->data;
        asort($data);

        $ddlString = "<?php\n\n//@" . "formatter:off\n\nreturn " . Util::arrayExport($data) . ";\n\n//@" . "formatter:on\n";

        return $ddlString;
    }



    public function saveToFile(string $filename): self
    {
        file_put_contents($filename, $this->saveToString());

        return $this;
    }



    protected function rrmdir($src)
    {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    $this->rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }



    public function saveToDir(?string $dirname = null)
    {
        if (!$dirname) {
            $dirname = $this->getDir();
        }
        if (!$dirname) {
            throw new \Exception('Un répertoire doit être spécifié pour mettre à jour la DDL');
        }

        if (file_exists($dirname)) {
            $this->rrmdir($dirname);
        }
        mkdir($dirname);
        foreach ($this->data as $type => $ds) {
            $dir = $dirname . '/' . $type;

            if ($type == self::SEQUENCE) {
                $this->writeArray($dir . '.php', array_keys($ds));
            } else {
                mkdir($dir);
                foreach ($this->data[$type] as $d) {
                    $file = $dir . '/' . $d['name'];
                    switch ($type) {
                        case self::TABLE:
                        case self::PRIMARY_CONSTRAINT:
                        case self::REF_CONSTRAINT:
                        case self::UNIQUE_CONSTRAINT:
                        case self::INDEX:
                            $this->writeArray($file . '.php', $d);
                            break;
                        case self::VIEW:
                        case self::MATERIALIZED_VIEW:
                        case self::PROCEDURE:
                        case self::FUNCTION:
                        case self::TRIGGER:
                            file_put_contents($file . '.sql', $d['definition']);
                            break;
                        case self::PACKAGE:
                            mkdir($file);
                            file_put_contents($file . '/definition.sql', $d['definition']);
                            file_put_contents($file . '/body.sql', $d['body']);
                            break;
                    }
                }
            }
        }

        if ($this->getColumnsPositionsFile()) {
            $positions = $this->applyColumnPositions();
            $this->writeArray($this->getColumnsPositionsFile(), $positions);
        }
    }



    public function loadFromDir(?string $dir = null)
    {
        if (!$dir) {
            $dir = $this->getDir();
        }
        if (!$dir) {
            throw new \Exception('Un répertoire doit être spécifié pour charger la DDL');
        }

        if (!str_ends_with($dir, '/')) {
            $dir .= '/';
        }

        $this->data = [];
        if (file_exists($dir . self::SEQUENCE . '.php')) {
            $sequences                  = require $dir . self::SEQUENCE . '.php';
            $this->data[self::SEQUENCE] = [];
            foreach ($sequences as $sequence) {
                $this->data[self::SEQUENCE][$sequence] = ['name' => $sequence];
            }
        }

        if (file_exists($dir . self::PACKAGE) && is_dir($dir . self::PACKAGE)) {
            $this->data[self::PACKAGE] = [];
            $data                      = scandir($dir . self::PACKAGE);
            foreach ($data as $name) {
                if ($name != '.' && $name != '..') {
                    $this->data[self::PACKAGE][$name] = ['name' => $name];
                    if (file_exists($dir . self::PACKAGE . '/' . $name . '/definition.sql')) {
                        $this->data[self::PACKAGE][$name]['definition'] = file_get_contents($dir . self::PACKAGE . '/' . $name . '/definition.sql');
                    }
                    if (file_exists($dir . self::PACKAGE . '/' . $name . '/body.sql')) {
                        $this->data[self::PACKAGE][$name]['body'] = file_get_contents($dir . self::PACKAGE . '/' . $name . '/body.sql');
                    }
                }
            }
        }

        $arrays = [self::TABLE, self::PRIMARY_CONSTRAINT, self::REF_CONSTRAINT, self::UNIQUE_CONSTRAINT, self::INDEX];
        foreach ($arrays as $type) {
            if (file_exists($dir . $type) && is_dir($dir . $type)) {
                $this->data[$type] = [];
                $data              = scandir($dir . $type);
                foreach ($data as $name) {
                    if ($name != '.' && $name != '..') {
                        $def                             = require $dir . $type . '/' . $name;
                        $this->data[$type][$def['name']] = $def;
                    }
                }
            }
        }

        $sqls = [self::VIEW, self::MATERIALIZED_VIEW, self::TRIGGER, self::PROCEDURE, self::FUNCTION];
        foreach ($sqls as $type) {
            if (file_exists($dir . $type) && is_dir($dir . $type)) {
                $this->data[$type] = [];
                $data              = scandir($dir . $type);
                foreach ($data as $name) {
                    if ($name != '.' && $name != '..') {
                        $def  = file_get_contents($dir . $type . '/' . $name);
                        $name = substr($name, 0, -4);
                        if (str_contains($name, '.')) {
                            [$schema, $vmname] = Util::explodedFullObjectName($name);
                            $this->data[$type][$name] = ['schema' => $schema, 'name' => $vmname, 'definition' => $def];
                        } else {
                            $this->data[$type][$name] = ['name' => $name, 'definition' => $def];
                        }
                        if (false !== strpos($def, 'LANGUAGE plpgsql')) {
                            $this->traitementSpecifiquePostgres($this->data[$type][$name], $type);
                        }
                    }
                }
            }
        }
    }



    private function traitementSpecifiquePostgres(array &$ddl, string $managerName): void
    {
        switch($managerName){
            case self::PROCEDURE:
                ProcedureManager::loadFromDdlFile($ddl);
            case self::FUNCTION:
                FunctionManager::loadFromDdlFile($ddl);
        }
    }



    public function loadFromFile(string $filename): self
    {
        $this->data = require $filename;

        return $this;
    }
}