<?php

namespace Unicaen\BddAdmin\Ddl;

use Unicaen\BddAdmin\Util;

class DdlFilter implements \ArrayAccess
{
    const INCLUDES = 'includes';
    const EXCLUDES = 'excludes';

    protected array $includes = [];

    protected array $excludes = [];



    public function getIncludes(): array
    {
        return $this->includes;
    }



    public function setIncludes(array|string|null $includes): self
    {
        if (is_string($includes)) {
            $this->includes = [$includes];
        } elseif (is_array($includes)) {
            $this->includes = $includes;
        } else {
            $this->includes = [];
        }

        return $this;
    }



    public function addArray(array $filter): self
    {
        if (array_key_exists(self::INCLUDES, $filter)) {
            foreach ($filter[self::INCLUDES] as $include) {
                $this->addInclude($include);
            }
        }

        if (array_key_exists(self::EXCLUDES, $filter)) {
            foreach ($filter[self::EXCLUDES] as $exclude) {
                $this->addExclude($exclude);
            }
        }

        return $this;
    }



    public function addInclude(string $include): self
    {
        $this->includes[] = $include;

        return $this;
    }



    public function hasInclude(string $include): bool
    {
        return in_array($include, $this->includes);
    }



    public function getExcludes(): array
    {
        return $this->excludes;
    }



    public function setExcludes(array|string|null $excludes): DdlFilter
    {
        if (is_string($excludes)) {
            $this->excludes = [$excludes];
        } elseif (is_array($excludes)) {
            $this->excludes = $excludes;
        } else {
            $this->excludes = [];
        }

        return $this;
    }



    public function addExclude(string $exclude): self
    {
        $this->excludes[] = $exclude;

        return $this;
    }



    public function hasExclude(string $exclude): bool
    {
        return in_array($exclude, $this->excludes);
    }



    public function toArray(): array
    {
        return [
            self::INCLUDES => $this->includes,
            self::EXCLUDES => $this->excludes,
        ];
    }



    static public function normalize(DdlFilter|array|string|null $data): DdlFilter
    {
        if ($data instanceof self) {
            return $data;
        }
        $co = new self;
        if (is_array($data)) {
            $vars = [self::INCLUDES, self::EXCLUDES];
            foreach ($vars as $var) {
                if (array_key_exists($var, $data)) {
                    $filter = $data[$var];

                    if (null === $filter) {
                        $filter = [];
                    } elseif (is_string($filter)) {
                        $filter = [$filter];
                    }

                    $co->$var = $filter;
                }
            }
        } else {
            $co->setIncludes($data);
        }

        return $co;
    }



    static public function normalize2(DdlFilter|array|string|null $includes = [], DdlFilter|array|string|null $excludes = []): DdlFilter
    {
        if ($includes instanceof DdlFilter) return $includes;

        return self::normalize(compact(self::INCLUDES, self::EXCLUDES));
    }



    public function toSql(?string $schemaColName, string $colName): array
    {
        $includes = $this->includes;
        $excludes = $this->excludes;

        $f = [];
        $p = [];

        if ($includes) {
            if (is_string($includes)) {
                $includes = [$includes];
            }
            $i = 0;
            if (!empty($includes)) {
                $f[] = 'AND (0=1';
                foreach ($includes as $include) {
                    [$includeSchema, $includeObject] = Util::explodedFullObjectName($include);

                    $i++;
                    if ($schemaColName && $includeSchema) {
                        $f[]               = "OR ($schemaColName LIKE :includesc$i AND $colName LIKE :includeobj$i)";
                        $p["includesc$i"]  = $includeSchema;
                        $p["includeobj$i"] = $includeObject;
                    } else {
                        $f[]            = "OR $colName LIKE :include$i";
                        $p["include$i"] = $includeObject;
                    }
                }
                $f[] = ')';
            }
        }

        if ($excludes) {
            $f[] = 'AND NOT (0=1';
            if (is_string($excludes)) {
                $excludes = [$excludes];
            }
            $i = 0;
            foreach ($excludes as $exclude) {
                [$excludeSchema, $excludeObject] = Util::explodedFullObjectName($exclude);

                $i++;
                if ($schemaColName && $excludeSchema) {
                    $f[]               = "OR ($schemaColName LIKE :excludesc$i AND $colName LIKE :excludeobj$i)";
                    $p["excludesc$i"]  = $excludeSchema;
                    $p["excludeobj$i"] = $excludeObject;
                } else {
                    $f[]            = "OR $colName LIKE :exclude$i";
                    $p["exclude$i"] = $excludeObject;
                }
            }
            $f[] = ')';
        }

        return [implode(' ', $f), $p];
    }



    public function match(string $name): bool
    {
        if ($this->excludes) {
            $excludes = (array)$this->excludes;
            foreach ($excludes as $exclude) {
                if (preg_match('/^' . str_replace('%', '.*', $exclude) . '$/', $name, $out)) {
                    return false;
                }
            }
        }

        if ($this->includes) {
            $includes = (array)$this->includes;
            foreach ($includes as $include) {
                if (preg_match('/^' . str_replace('%', '.*', $include) . '$/', $name, $out)) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }



    public function isEmpty(): bool
    {
        return empty($this->includes) && empty($this->excludes);
    }



    public function offsetExists($offset): bool
    {
        return array_key_exists($offset);
    }



    public function offsetGet($offset): mixed
    {
        return $this->$offset;
    }



    public function offsetSet($offset, $value): void
    {
        $this->$offset = $value;
    }



    public function offsetUnset($offset): void
    {
    }
}