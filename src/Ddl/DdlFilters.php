<?php

namespace Unicaen\BddAdmin\Ddl;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\Util;

class DdlFilters implements \Iterator, \ArrayAccess
{
    const EXPLICIT = 'explicit';

    /**
     * @var DdlFilter[]
     */
    protected array $objects = [];

    protected bool $explicit = false;



    public function get(string $name): DdlFilter
    {
        if (!array_key_exists($name, $this->objects)) {
            $this->objects[$name] = new DdlFilter;
        }

        return $this->objects[$name];
    }



    public function set(string $name, DdlFilter $object): DdlFilters
    {
        $this->objects[$name] = $object;

        return $this;
    }



    public function has(string $name): bool
    {
        return array_key_exists($name, $this->objects);
    }



    public function addArray(array $filters): self
    {
        if (array_key_exists(self::EXPLICIT, $filters) && $filters[self::EXPLICIT]) {
            $this->setExplicit(true);
            unset($filters[self::EXPLICIT]);
        }
        foreach ($filters as $name => $filter) {
            if (array_key_exists($name, $this->objects)) {
                $this->objects[$name]->addArray($filter);
            } else {
                $this->objects[$name] = DdlFilter::normalize($filter);
            }
        }

        return $this;
    }



    public function addDdl(Ddl $ddl): self
    {
        $this->setExplicit(true);

        foreach ($ddl as $ddlClass => $objects) {
            foreach ($objects as $object => $objectDdl) {
                $ddlFilter = $this->get($ddlClass);
                if (!$ddlFilter->hasInclude($object)) {
                    $ddlFilter->addInclude($object);
                }
            }
        }

        return $this;
    }



    public function addTablesDeps(Bdd $bdd): self
    {
        $tablesDep = [
            Ddl::INDEX,
            Ddl::PRIMARY_CONSTRAINT,
            Ddl::REF_CONSTRAINT,
            Ddl::UNIQUE_CONSTRAINT,
        ];

        $ddlTableFilter = $this->get(Ddl::TABLE);

        foreach ($tablesDep as $tableDep) {
            $ddlFilter = $this->get($tableDep);

            $objects = $bdd->manager($tableDep)->get();
            foreach ($objects as $obj) {
                if (is_array($obj)){
                    $tableName = Util::fullObjectName($obj['schema'] ?? null, $obj['table']);
                    if ($ddlTableFilter->hasInclude($tableName)) {
                        $objectName = Util::fullObjectName($obj['schema'] ?? null, $obj['name']);
                        if (!$ddlFilter->hasInclude($objectName)) {
                            $ddlFilter->addInclude($objectName);
                        }
                    }
                }
            }
        }

        return $this;
    }



    public function toArray(): array
    {
        $a = [
            self::EXPLICIT => $this->explicit,
        ];
        foreach ($this->objects as $name => $object) {
            $a[$name] = $object->toArray();
        }

        return $a;
    }



    public static function normalize(DdlFilters|array $data): DdlFilters
    {
        if ($data instanceof self) {
            return $data;
        }

        $config = new self;
        if (is_array($data)) {
            foreach ($data as $name => $objData) {
                switch ($name) {
                    case self::EXPLICIT:
                        $config->setExplicit((bool)$objData);
                        break;
                    default:
                        $config->set($name, DdlFilter::normalize($objData));
                }
            }
        }

        return $config;
    }



    public function isExplicit(): bool
    {
        return $this->explicit;
    }



    public function setExplicit(bool $explicit): DdlFilters
    {
        $this->explicit = $explicit;

        return $this;
    }



    public function current(): mixed
    {
        return current($this->objects);
    }



    public function next(): void
    {
        next($this->objects);
    }



    public function key(): mixed
    {
        return key($this->objects);
    }



    public function valid(): bool
    {
        return key($this->objects) !== null;
    }



    public function rewind(): void
    {
        reset($this->objects);
    }



    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }



    public function offsetGet($offset): mixed
    {
        return $this->get($offset);
    }



    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }



    public function offsetUnset($offset): void
    {
        unset($this->objects[$offset]);
    }

}