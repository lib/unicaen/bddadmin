<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\Manager\AbstractManager;

abstract class AbstractManagerDdlConstraint extends AbstractManager
{
    protected string $description = '';



    protected function indexExists($indexName): bool
    {
        return $this->bdd->index()->exists($indexName);
    }



    protected function getNameAndTable(array|string $name): array
    {
        if (is_array($name)) {
            $tableName = $name['table'];
            $name = $name['name'];
        } else {
            $tableName = null;
        }

        if (!$tableName) {
            $sql = "SELECT TABLE_NAME FROM ALL_CONSTRAINTS WHERE CONSTRAINT_NAME = :name";
            $d = $this->bdd->select($sql, compact('name'), ['fetch' => Bdd::FETCH_ONE]);
            $tableName = $d['TABLE_NAME'];
        }

        return [$tableName, $name];
    }



    public function isDiff(array $d1, array $d2): bool
    {
        unset($d1['index']);
        unset($d2['index']);

        return $d1 != $d2;
    }



    abstract public function makeCreate(array $data): string;



    public function create(array $data): void
    {
        $sql = $this->makeCreate($data);
        $this->addQuery($sql, 'Ajout de la ' . $this->description . ' ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        [$tableName, $name] = $this->getNameAndTable($name);

        $this->addQuery("ALTER TABLE $tableName DROP CONSTRAINT $name", 'Suppression de la ' . $this->description . ' ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        $this->drop($old);
        $this->create($new);
    }



    public function rename(string $oldName, array|string $new): void
    {
        $tableName = $new['table'];
        $newName = $new['name'];

        $sql = "ALTER TABLE \"$tableName\" RENAME CONSTRAINT \"$oldName\" TO \"$newName\"";
        $this->addQuery($sql, 'Renommage de la ' . $this->description . ' ' . $oldName . ' en ' . $newName);
    }



    public function enable(array|string $name): void
    {
        [$tableName, $name] = $this->getNameAndTable($name);

        $this->addQuery("ALTER TABLE $tableName MODIFY CONSTRAINT $name ENABLE", 'Activation de la ' . $this->description . ' ' . $name);
    }



    public function disable(array|string $name): void
    {
        [$tableName, $name] = $this->getNameAndTable($name);

        $this->addQuery("ALTER TABLE $tableName MODIFY CONSTRAINT $name DISABLE", 'Désactivation de la ' . $this->description . ' ' . $name);
    }
}