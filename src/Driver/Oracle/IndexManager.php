<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\CompilableInterface;
use Unicaen\BddAdmin\Manager\IndexManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class IndexManager extends AbstractManager implements IndexManagerInterface, CompilableInterface
{
    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND INDEX_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
        SELECT
          INDEX_NAME NAME
        FROM
          USER_INDEXES
        WHERE
          INDEX_TYPE <> 'LOB'
          AND INDEX_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY
          INDEX_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'index_name');

        $data = [];

        $sql = "SELECT
          index_name \"name\",
          uniqueness \"unique\",
          table_name \"table\"
        FROM
          USER_INDEXES
        WHERE
          INDEX_TYPE <> 'LOB'
          AND INDEX_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY
          INDEX_NAME";

        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            $data[$r['name']] = [
                'name'    => $r['name'],
                'unique'  => ($r['unique'] === 'UNIQUE'),
                'table'   => $r['table'],
                'columns' => [],
            ];
        }

        $sql = "
        SELECT INDEX_NAME \"name\", COLUMN_NAME \"column\" 
        FROM USER_IND_COLUMNS 
        WHERE 1=1 $f 
        ORDER BY COLUMN_POSITION";
        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            if (isset($data[$r['name']])) {
                $data[$r['name']]['columns'][] = $r['column'];
            }
        }

        return $data;
    }



    protected function makeCreate(array $data): string
    {
        $sql = "CREATE ";
        if ($data['unique']) $sql .= "UNIQUE ";
        $sql .= "INDEX " . $data['name'] . " ON " . $data['table'] . ' (' . implode(', ', $data['columns']) . ')';

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $sql = $this->makeCreate($data);
        $this->addQuery($sql, 'Ajout de l\'index ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP INDEX $name", 'Suppression de l\'index ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if ($old != $new) {
            $this->drop($old['name']);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            $newName = $new;
        }

        $sql = "ALTER INDEX \"$oldName\" RENAME TO \"$newName\"";
        $this->addQuery($sql, 'Renommage de l\'index ' . $oldName . ' en ' . $newName);
    }



    public function compiler(string|array $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("ALTER INDEX $name REBUILD", 'Reconstruction de l\'index ' . $name);
    }



    public function compilerTout(): void
    {
        $objects = $this->getList();
        foreach ($objects as $object) {
            $this->compiler($object);
        }
    }
}