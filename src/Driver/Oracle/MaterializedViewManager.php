<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\MaterializedViewManagerInteface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class MaterializedViewManager extends AbstractManager implements MaterializedViewManagerInteface
{
    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND OBJECT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
        SELECT 
          OBJECT_NAME NAME 
        FROM 
          USER_OBJECTS 
        WHERE 
          OBJECT_TYPE = 'MATERIALIZED VIEW' AND GENERATED = 'N'
          $f
        ORDER BY 
          OBJECT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'mview_name');
        $data = [];

        $q = "
          SELECT
            mview_name \"name\",
            query      \"definition\"
          FROM
            USER_MVIEWS
          WHERE
            1=1
            $f
          ORDER BY
            mview_name
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $data[$r['name']] = [
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], true),
            ];
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $sql = 'CREATE MATERIALIZED VIEW ' . $data['name'] . " AS\n";
        $sql .= $data['definition'];
        $this->addQuery($sql, 'Ajout de la vue matérialisée ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP MATERIALIZED VIEW " . $name, 'Suppression de la vue matérialisée ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if ($old != $new) {
            $this->drop($old['name']);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_string($new)) {
            throw new \Exception('Récupération de la définition de OldName à faire');
        }

        $this->drop($oldName);
        $this->create($new);
    }



    /**
     * @param string|array $name
     */
    public function refresh($name): void
    {
        if (is_array($name)) {
            $name = $name['name'];
        }

        $this->addQuery("BEGIN DBMS_SNAPSHOT.REFRESH( '$name','C'); END;", 'Mise à jour de la vue matérialisée ' . $name);
    }
}