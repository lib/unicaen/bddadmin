<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\CompilableInterface;
use Unicaen\BddAdmin\Manager\PackageManagerInteface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class PackageManager extends AbstractManager implements PackageManagerInteface, CompilableInterface
{

    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND OBJECT_NAME = :name";
            $p['name'] = $name;
        }

        $sql  = "
          SELECT 
            OBJECT_NAME NAME
          FROM 
            USER_OBJECTS 
          WHERE 
            OBJECT_TYPE = 'PACKAGE' AND GENERATED = 'N'
            $f
          ORDER BY OBJECT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null,'name');
        $data = [];

        $q = "SELECT 
            name \"name\",
            text \"ddl\",
            CASE WHEN type = 'PACKAGE' THEN 'definition' ELSE 'body' END \"type\"
          FROM
            USER_SOURCE 
          WHERE
            (type = 'PACKAGE' OR type = 'PACKAGE BODY') 
            $f 
          ORDER BY name, line
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            if (!isset($data[$r['name']])) {
                $data[$r['name']] = [
                    'name'       => $r['name'],
                    'definition' => 'CREATE OR REPLACE ',
                    'body'       => 'CREATE OR REPLACE ',
                ];
            }

            $data[$r['name']][$r['type']] .= $r['ddl'];
        }
        foreach ($data as $name => $d) {
            $definition                = $this->purger($d['definition'], false);
            $body                      = $this->purger($d['body'], false);
            $data[$name]['definition'] = $definition;
            $data[$name]['body']       = $body;
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $this->addQuery($data['definition'], 'Ajout/modification de la définition du package ' . $data['name']);
        $this->addQuery($data['body'], 'Ajout/modification du corps du package ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP PACKAGE $name", 'Suppression du package ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_string($new)){
            throw new \Exception('Récupération de la définition de OldName à faire');
        }

        $this->drop($oldName);
        $this->create($new);
    }



    public function compiler(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("ALTER PACKAGE $name COMPILE PACKAGE", 'Compilation de la déclaration du package ' . $name);
        $this->addQuery("ALTER PACKAGE $name COMPILE BODY", 'Compilation du corps de package ' . $name);
    }



    public function compilerTout(): void
    {
        $objects = $this->getList();
        foreach ($objects as $object) {
            $this->compiler($object);
        }
    }
}