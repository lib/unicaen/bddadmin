<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\PrimaryConstraintManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class PrimaryConstraintManager extends AbstractManagerDdlConstraint implements PrimaryConstraintManagerInterface
{
    protected string $description = 'clé primaire';



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND CONSTRAINT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
        SELECT 
          CONSTRAINT_NAME NAME
        FROM 
          USER_CONSTRAINTS 
        WHERE 
          CONSTRAINT_TYPE = 'P'
          AND CONSTRAINT_NAME NOT LIKE 'BIN$%'
          AND CONSTRAINT_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY 
          CONSTRAINT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'c.constraint_name');
        $data = [];

        $sql = "
        SELECT
          c.constraint_name \"name\",
          c.table_name \"table\",
          c.index_name \"index\",
          cc.column_name \"column\"
        FROM
          USER_CONSTRAINTS c
          JOIN USER_CONS_COLUMNS cc ON cc.constraint_name = c.constraint_name
        WHERE
          c.constraint_type = 'P'
          AND c.constraint_name NOT LIKE 'BIN$%'
          AND c.CONSTRAINT_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY
          c.constraint_name,
          cc.position
        ";

        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            if (!isset($data[$r['name']])) {
                $data[$r['name']] = [
                    'name'    => $r['name'],
                    'table'   => $r['table'],
                    'index'   => $r['index'],
                    'columns' => [],
                ];
            }
            $data[$r['name']]['columns'][] = $r['column'];
        }

        return $data;
    }



    public function makeCreate(array $data): string
    {
        $cols = implode(', ', $data['columns']);
        $sql = "ALTER TABLE " . $data['table'] . " ADD CONSTRAINT " . $data['name'] . " PRIMARY KEY ($cols) ";
        if ($data['index']) {
            if ($this->indexExists($data['index'])) {
                $sql .= 'USING INDEX ' . $data['index'] . ' ';
            } else {
                $sql .= "USING INDEX (\n\tCREATE UNIQUE INDEX " . $data['index'] . ' ON ' . $data['table'];
                $sql .= '(' . implode(' ASC, ', $data['columns']) . ' ASC)';
                $sql .= "\n) ";
            }
        }
        $sql .= "ENABLE";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::create($data);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::drop($name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->isDiff($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            parent::alter($old, $new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::rename($oldName, $new);
    }



    /**
     * @param string|array $name
     */
    public function enable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::enable($name);
    }



    /**
     * @param string|array $name
     */
    public function disable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::disable($name);
    }



    /**
     * @return PrimaryConstraintManagerInterface
     */
    public function enableAll(): PrimaryConstraintManagerInterface
    {
        $this->bdd->logBegin("Activation de toutes les clés primaires");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés primaires ont été activées');

        return $this;
    }



    /**
     * @return PrimaryConstraintManagerInterface
     */
    public function disableAll(): PrimaryConstraintManagerInterface
    {
        $this->bdd->logBegin("Désactivation de toutes les clés primaires");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés primaires ont été désactivées');

        return $this;
    }
}