<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\RefConstraintManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class RefConstraintManager extends AbstractManagerDdlConstraint implements RefConstraintManagerInterface
{
    protected string $description = 'clé étrangère';



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND CONSTRAINT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
          SELECT 
            CONSTRAINT_NAME
          FROM 
            USER_CONSTRAINTS 
          WHERE           
            CONSTRAINT_TYPE = 'R'  
            AND CONSTRAINT_NAME NOT LIKE 'BIN$%'
            AND CONSTRAINT_NAME NOT LIKE 'SYS_%'
            $f
          ORDER BY 
            CONSTRAINT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['CONSTRAINT_NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'c.constraint_name');
        $data = [];

        $sql = "
        SELECT
          c.constraint_name \"name\",
          c.table_name      \"table\",
          cc.column_name    \"column\",
          rc.table_name     \"rtable\",
          rcc.column_name   \"rcolumn\",
          c.delete_rule     \"delete_rule\",
          c.index_name      \"index\"
        FROM
          USER_CONSTRAINTS c
          JOIN USER_CONSTRAINTS rc ON rc.constraint_name = c.r_constraint_name AND rc.constraint_type = 'P'
          JOIN USER_CONS_COLUMNS cc ON cc.constraint_name = c.constraint_name
          JOIN USER_CONS_COLUMNS rcc ON rcc.constraint_name = rc.constraint_name AND rcc.position = cc.position
        WHERE
          c.CONSTRAINT_TYPE = 'R' 
          AND c.CONSTRAINT_NAME NOT LIKE 'BIN$%'
          AND c.CONSTRAINT_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY
          c.CONSTRAINT_NAME,
          cc.POSITION
        ";

        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            if (!isset($data[$r['name']])) {
                $data[$r['name']] = [
                    'name'        => $r['name'],
                    'table'       => $r['table'],
                    'rtable'      => $r['rtable'],
                    'delete_rule' => ($r['delete_rule'] != 'NO ACTION') ? $r['delete_rule'] : null,
                    'index'       => $r['index'],
                    'columns'     => [],
                ];
            }
            $data[$r['name']]['columns'][$r['column']] = $r['rcolumn'];
        }

        return $data;
    }



    public function makeCreate(array $data): string
    {
        $cols = implode(', ', array_keys($data['columns']));
        $rCols = implode(', ', array_values($data['columns']));

        $sql = "ALTER TABLE " . $data['table'] . " ADD CONSTRAINT " . $data['name'] . " FOREIGN KEY ($cols) 
        REFERENCES " . $data['rtable'] . " ($rCols) ";
        if ($data['index']) {
            if ($this->indexExists($data['index'])) {
                $sql .= 'USING INDEX ' . $data['index'] . ' ';
            } else {
                $sql .= "USING INDEX (\n\tCREATE UNIQUE INDEX " . $data['index'] . ' ON ' . $data['table'];
                $sql .= '(' . implode(' ASC, ', $data['columns']) . ' ASC)';
                $sql .= "\n) ";
            }
        }
        if ($data['delete_rule']) $sql .= 'ON DELETE ' . $data['delete_rule'] . ' ';
        $sql .= "ENABLE";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::create($data);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::drop($name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->isDiff($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            parent::alter($old, $new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::rename($oldName, $new);
    }



    /***
     * @param string|array $name
     */
    public function enable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::enable($name);
    }



    /***
     * @param string|array $name
     */
    public function disable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::disable($name);
    }



    /**
     * @return RefConstraintManagerInterface
     */
    public function enableAll(): RefConstraintManagerInterface
    {
        $this->bdd->logBegin("Activation de toutes les clés étrangères");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Contrainte " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés étrangères ont été activées');

        return $this;
    }



    /**
     * @return RefConstraintManagerInterface
     */
    public function disableAll(): RefConstraintManagerInterface
    {
        $this->bdd->logBegin("Désactivation de toutes les clés étrangères");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Contrainte " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés étrangères ont été désactivées');

        return $this;
    }
}