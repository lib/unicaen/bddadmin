<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\SequenceManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class SequenceManager extends AbstractManager implements SequenceManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'sequence_name');
        $data = [];

        $sql = "
        SELECT 
          SEQUENCE_NAME \"name\" 
        FROM 
          USER_SEQUENCES 
        WHERE 
          1=1 
          $f
        ORDER BY
          SEQUENCE_NAME
        ";

        $qr = $this->bdd->select($sql, $p);
        foreach ($qr as $r) {
            $data[$r['name']] = $r;
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;


        $sql = "CREATE SEQUENCE " . $data['name'] . " INCREMENT BY 1 MINVALUE 1 NOCACHE";
        $this->addQuery($sql, 'Ajout de la séquence ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $sql = "DROP SEQUENCE $name";
        $this->addQuery($sql, 'Suppression de la séquence ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->drop($old['name']);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_string($new)) {
            $new = ['name' => $new];
        }

        $this->drop($oldName);
        $this->create($new);
    }



    public function nextVal(string|array $sequence): int
    {
        if (is_string($sequence)){
            $sequenceName = $sequence;
        }else{
            $sequenceName = $sequence['name'];
        }

        $r = $this->bdd->selectOne("SELECT $sequenceName.NEXTVAL seqval FROM DUAL");

        return (int)$r['SEQVAL'];
    }



    /**
     * @inheritDoc
     */
    public function prepareRenameCompare(array $data): array
    {
        return $data;
    }

}