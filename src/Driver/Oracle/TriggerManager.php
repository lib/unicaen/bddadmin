<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\CompilableInterface;
use Unicaen\BddAdmin\Manager\TriggerManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class TriggerManager extends AbstractManager implements TriggerManagerInterface, CompilableInterface
{
    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND OBJECT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
          SELECT 
            OBJECT_NAME NAME
          FROM 
            USER_OBJECTS 
          WHERE 
            OBJECT_TYPE = 'TRIGGER' AND GENERATED = 'N'
            $f
          ORDER BY 
            OBJECT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'name');
        $data = [];

        $q = "
          SELECT 
            NAME \"name\",
            TEXT \"ddl\"
          FROM
            USER_SOURCE 
          WHERE
            TYPE = 'TRIGGER'
            AND NAME NOT LIKE 'BIN$%'
            $f
          ORDER BY 
            NAME, 
            LINE
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            if (!isset($data[$r['name']])) {
                $data[$r['name']] = [
                    'name'       => $r['name'],
                    'definition' => 'CREATE OR REPLACE ',
                ];
            }
            $data[$r['name']]['definition'] .= $r['ddl'];
        }
        foreach ($data as $name => $d) {
            $def = $this->purger($d['definition'], false);
            $def = $this->purgeOwner($def, $d['name']);
            $data[$name]['definition'] = $def;
        }

        return $data;
    }



    function purgeOwner(string $ddl, string $name): string
    {
        $np = strpos($ddl, '."'.$name.'"');
        if (false !== $np && 0 === strpos($ddl,'CREATE OR REPLACE TRIGGER "')){
            $owner = substr($ddl, 27, $np-28);
            $ddl = str_replace('"'.$owner.'".', '', $ddl);
        }

        $np = strpos($ddl, '.'.$name);
        if (false !== $np && 0 === strpos($ddl,'CREATE OR REPLACE TRIGGER ')){
            $owner = substr($ddl, 26, $np-26);
            $ddl = str_replace($owner.'.', '', $ddl);
        }

        return $ddl;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $this->addQuery($data['definition'], 'Ajout/modification du trigger ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP TRIGGER $name", 'Suppression du trigger ' . $name);
    }



    public function enable(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("alter trigger $name enable", 'Activation du trigger ' . $name);
    }



    public function disable(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("alter trigger $name disable", 'Désactivation du trigger ' . $name);
    }



    public function enableAll(): TriggerManagerInterface
    {
        $this->bdd->logBegin("Activation de tous les déclencheurs");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Déclencheur " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Tous les déclencheurs ont été activés');

        return $this;
    }



    public function disableAll(): TriggerManagerInterface
    {
        $this->bdd->logBegin("Désactivation de tous les déclencheurs");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Déclencheur " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Tous les déclencheurs ont été désactivés');

        return $this;
    }



    public function alter(array $old, array $new): void
    {
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            $newName = $new;
        }

        $sql = "ALTER TRIGGER \"$oldName\" RENAME TO \"$newName\"";
        $this->addQuery($sql, 'Renommage du trigger ' . $oldName . ' en ' . $newName);
    }



    public function compiler(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("ALTER TRIGGER $name COMPILE", 'Compilation du trigger ' . $name);
    }



    public function compilerTout(): void
    {
        $objects = $this->getList();
        foreach ($objects as $object) {
            $this->compiler($object);
        }
    }
}