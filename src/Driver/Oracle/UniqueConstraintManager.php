<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\UniqueConstraintManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class UniqueConstraintManager extends AbstractManagerDdlConstraint implements UniqueConstraintManagerInterface
{
    protected string $description = 'contrainte d\'unicité';



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND CONSTRAINT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
          SELECT 
            CONSTRAINT_NAME NAME
          FROM 
            USER_CONSTRAINTS 
          WHERE 
            CONSTRAINT_TYPE = 'U'
            AND CONSTRAINT_NAME NOT LIKE 'BIN$%'
            AND CONSTRAINT_NAME NOT LIKE 'SYS_%'
            $f
          ORDER BY 
            CONSTRAINT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'c.constraint_name');
        $data = [];

        $sql = "
        SELECT
          c.CONSTRAINT_NAME \"name\",
          c.TABLE_NAME      \"table\",
          c.INDEX_NAME      \"index\",
          cc.COLUMN_NAME    \"column\"
        FROM
          USER_CONSTRAINTS c
          JOIN USER_CONS_COLUMNS cc ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
        WHERE
          c.CONSTRAINT_TYPE = 'U'
          AND c.CONSTRAINT_NAME NOT LIKE 'BIN$%'
          AND c.CONSTRAINT_NAME NOT LIKE 'SYS_%'
          $f
        ORDER BY
          c.CONSTRAINT_NAME,
          cc.POSITION";

        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            if (!isset($data[$r['name']])) {
                $data[$r['name']] = [
                    'name'    => $r['name'],
                    'table'   => $r['table'],
                    'index'   => $r['index'],
                    'columns' => [],
                ];
            }
            $data[$r['name']]['columns'][] = $r['column'];
        }

        return $data;
    }



    public function makeCreate(array $data): string
    {
        $cols = implode(', ', $data['columns']);
        $sql = "ALTER TABLE " . $data['table'] . " ADD CONSTRAINT " . $data['name'] . " UNIQUE ($cols) ";
        if ($data['index']) {
            if ($this->indexExists($data['index'])) {
                $sql .= 'USING INDEX ' . $data['index'] . ' ';
            } else {
                $sql .= "USING INDEX (\n\tCREATE UNIQUE INDEX " . $data['index'] . ' ON ' . $data['table'];
                $sql .= '(' . implode(' ASC, ', $data['columns']) . ' ASC)';
                $sql .= "\n) ";
            }
        }
        $sql .= "ENABLE";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::create($data);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::drop($name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->isDiff($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            parent::alter($old, $new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::rename($oldName, $new);
    }



    /***
     * @param string|array $name
     */
    public function enable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::enable($name);
    }



    /***
     * @param string|array $name
     */
    public function disable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::disable($name);
    }



    /**
     * @return UniqueConstraintManagerInterface
     */
    public function enableAll(): UniqueConstraintManagerInterface
    {
        $this->bdd->logBegin("Activation de toutes les contraintes d'unicité");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les contraintes d\'unicité ont été activées');

        return $this;
    }



    /**
     * @return UniqueConstraintManagerInterface
     */
    public function disableAll(): UniqueConstraintManagerInterface
    {
        $this->bdd->logBegin("Désactivation de toutes les contraintes d'unicité");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les contraintes d\'unicité ont été désactivées');

        return $this;
    }
}