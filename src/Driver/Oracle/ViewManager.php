<?php

namespace Unicaen\BddAdmin\Driver\Oracle;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\CompilableInterface;
use Unicaen\BddAdmin\Manager\ViewManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;

class ViewManager extends AbstractManager implements ViewManagerInterface, CompilableInterface
{
    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $f = "AND OBJECT_NAME = :name";
            $p['name'] = $name;
        }

        $sql = "
          SELECT 
            OBJECT_NAME NAME
          FROM 
            USER_OBJECTS 
          WHERE 
            OBJECT_TYPE = 'VIEW' AND GENERATED = 'N'
            $f
          ORDER BY 
            OBJECT_NAME
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = $l['NAME'];
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null, 'view_name');
        $data = [];

        $q = "SELECT
            view_name \"name\",
            text \"definition\"
          FROM
            USER_VIEWS
          WHERE
            1=1
            $f
          ORDER BY
            view_name
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $r['definition'] = 'CREATE OR REPLACE FORCE VIEW ' . $r['name'] . " AS\n" . $r['definition'];
            $data[$r['name']] = [
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], true),
            ];
        }

        return $data;
    }



    public function create(array $data, $test = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $this->addQuery($data['definition'], 'Ajout/modification de la vue ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP VIEW " . $name, 'Suppression de la vue ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_string($new)) {
            throw new \Exception('Récupération de la définition de OldName à faire');
        }

        $this->drop($oldName);
        $this->create($new);
    }



    public function compiler(string|array $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("ALTER VIEW $name COMPILE", 'Compilation de la vue ' . $name);
    }



    public function compilerTout(): void
    {
        $objects = $this->getList();
        foreach ($objects as $object) {
            $this->compiler($object);
        }
    }
}