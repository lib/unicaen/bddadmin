<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Util;

abstract class AbstractManagerDdlConstraint extends AbstractManager
{
    protected string $description = '';



    protected function indexExists($indexName): bool
    {
        return $this->bdd->index()->exists($indexName);
    }



    protected function getNameAndTable(array|string $name): array
    {
        if (is_array($name)) {
            $schema = $name['schema'] ?? 'public';
            $table = Util::fullObjectName($name['schema'], $name['table']);
            $name = $name['name'];
        } else {
            $table = null;
            [$schema, $name] = Util::explodedFullObjectName($name);
        }

        if (!$table) {
            $sql = "SELECT table_schema, table_name FROM information_schema.table_constraints WHERE constraint_schema = :schema AND constraint_name = :name";
            $d = $this->bdd->selectOne($sql, compact('schema', 'name'));
            $schema = $d['table_schema'];
            $table = $d['table_name'];

        }

        return [$schema, $table, $name];
    }



    public function isDiff(array $d1, array $d2): bool
    {
        unset($d1['index']);
        unset($d2['index']);

        return $d1 != $d2;
    }



    abstract public function makeCreate(array $data): string;



    public function create(array $data): void
    {
        $sql = $this->makeCreate($data);
        $this->addQuery($sql, 'Ajout de la ' . $this->description . ' ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        [$schema, $tableName, $name] = $this->getNameAndTable($name);

        $this->addQuery("ALTER TABLE $schema.$tableName DROP CONSTRAINT $name", 'Suppression de la ' . $this->description . ' ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        $this->drop($old);
        $this->create($new);
    }



    public function rename(string $oldName, array|string $new): void
    {
        $tableName = $new['table'];
        $newName = $new['name'];

        $sql = "ALTER TABLE $tableName RENAME CONSTRAINT $oldName TO $newName";
        $this->addQuery($sql, 'Renommage de la ' . $this->description . ' ' . $oldName . ' en ' . $newName);
    }



    public function enable(array|string $name): void
    {
        /* Impossible de désactiver & activer une contrainte */
    }



    public function disable(array|string $name): void
    {
        /* Impossible de désactiver & activer une contrainte */
    }
}