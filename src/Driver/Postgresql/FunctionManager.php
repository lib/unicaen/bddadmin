<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\FunctionManagerInteface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class FunctionManager extends AbstractManager implements FunctionManagerInteface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('n.nspname', 'p.proname');
        $data = [];

        $q = "
        SELECT
          n.nspname \"schema\",
          p.proname \"name\",
          pg_catalog.pg_get_functiondef(p.oid)        definition,
          pg_catalog.pg_get_function_result(p.oid)    return,
          pg_catalog.pg_get_function_arguments(p.oid) arguments
        FROM
          pg_catalog.pg_proc p
          LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE
          n.nspname NOT IN ('pg_catalog', 'information_schema')
          AND p.prokind = 'f'
          $f
        ORDER BY
          n.nspname, p.proname
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $name        = Util::fullObjectName($r['schema'], $r['name']);
            $data[$name] = [
                'schema'     => $r['schema'],
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], true),
                'return'     => $r['return'],
                'arguments'  => $r['arguments'],
            ];
        }

        return $data;
    }



    public static function loadFromDdlFile(array &$ddl)
    {
        //$ddl['return'] = null; @todo à gérer...
        $def = $ddl['definition'];

        $namePos   = strpos($def, $ddl['name']);
        $argsStart = $namePos + strlen($ddl['name']);

        if (false !== $namePos && $def[$argsStart] == '(') {
            $arguments        = substr($def, $argsStart + 1, strpos($def, ')', $argsStart + 1) - $argsStart - 1);
            $ddl['arguments'] = $arguments;
        }
    }



    public function exists(string $name): bool
    {
        [$schema, $name] = Util::explodedFullObjectName($name);
        if (!$schema) {
            $schema = 'public';
        }

        $sql    = "
        SELECT 
          count(*) nbr 
        FROM
          pg_catalog.pg_proc p
          LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE
          p.prokind = 'f'
          AND n.nspname = :schema
          AND p.proname = :name
        ";
        $params = ['schema' => $schema, 'name' => $name];

        $nbr = (int)$this->bdd->selectOne($sql, $params, 'nbr');

        return $nbr > 0;
    }



    public function create(array $data, $test = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        $this->addQuery($data['definition'], 'Ajout/modification de la fonction ' . $name);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) {
            $args = isset($name['arguments']) ? '(' . $name['arguments'] . ')' : '';
            $name = Util::fullObjectName($name['schema'] ?? null, $name['name']);
        } else {
            $args = '';
        }

        $this->addQuery("DROP FUNCTION " . $name . $args, 'Suppression de la fonction ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if (!isset($old['schema'])) {
            $old['schema'] = 'public';
        }
        if (!isset($new['schema'])) {
            $new['schema'] = 'public';
        }
        if (array_key_exists('return', $old) && !array_key_exists('return', $new)){
            // Hack pour éviter que des modifs soient détectées à tort
            $new['return'] = $old['return'];
        }
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
            $args    = '(' . $new['arguments'] . ')';
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
            $args = '';
        }

        $this->addQuery("ALTER FUNCTION $oldName$args RENAME TO $newName", 'Renommage de la fonction ' . $oldName . ' en ' . $newName);
    }

}