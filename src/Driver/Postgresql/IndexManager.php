<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\IndexManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class IndexManager extends AbstractManager implements IndexManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('i.schemaname', 'i.indexname');

        $data = [];

        $sql = "
        SELECT
          i.schemaname \"schema\",
          i.indexname  \"name\",
          i.indexdef   definition,
          i.tablename  table_name
        FROM
          pg_catalog.pg_indexes i
          LEFT JOIN information_schema.table_constraints c ON c.constraint_schema = i.schemaname AND c.constraint_name = i.indexname AND c.constraint_type IN ('PRIMARY KEY','UNIQUE')
        where
          i.schemaname not in ('pg_catalog','information_schema')
          AND c.constraint_type IS NULL
          $f
        ORDER BY
          i.schemaname,
          i.indexname
        ";

        $rs = $this->bdd->select($sql, $p);
        foreach ($rs as $r) {
            $name = Util::fullObjectName($r['schema'], $r['name']);
            $def = $r['definition'];
            $data[$name] = $this->extractFromGet($r);
        }

        return $data;
    }



    protected function extractFromGet(array $get): array
    {
        $definition = $get['definition'];

        $res = [
            'name'    => $get['name'],
            'unique'  => false,
            'type'    => NULL,
            'table'   => $get['table_name'],
            'schema'  => $get['schema'],
            'columns' => [],
        ];

        if (!str_starts_with($definition, 'CREATE ')) {
            throw new \Exception('Définition d\'index incorrecte');
        }

        $definition = substr($definition, 7);

        if (str_starts_with($definition, 'UNIQUE ')) {
            $res['unique'] = true;
            $definition = substr($definition, 7);
        }

        if (!str_starts_with($definition, 'INDEX ')) {
            throw new \Exception('Définition d\'index incorrecte');
        }

        $definition = substr($definition, 6);

        $n = $get['name'] . ' ON ' . $get['schema'] . '.' . $get['table_name'];

        if (!str_starts_with($definition, $n)) {
            throw new \Exception('Définition d\'index incorrecte : problème de nommage de table, schéma ou index');
        }

        [$using, $cols] = explode('(', trim(substr($definition, strlen($n))));

        $using = trim($using);

        if (str_starts_with($using, 'USING ')) {
            $res['type'] = substr($using, 6);
        }

        $cols = explode(',', trim(substr($cols, 0, -1)));
        foreach ($cols as $col) {
            $res['columns'][] = trim($col);
        }

        return $res;
    }



    protected function makeCreate(array $data)
    {
        $name = $data['name'];
        $schema = $data['schema'] ?? 'public';
        $table = $data['table'];
        $unique = $data['unique'] ?? false;
        $type = $data['type'] ?? 'btree';
        $cols = implode(', ', $data['columns']);

        $sql = "CREATE ";
        if ($unique) $sql .= "UNIQUE ";
        $sql .= "INDEX $name ON $schema.$table USING $type ($cols)";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $sql = $this->makeCreate($data);
        $this->addQuery($sql, 'Ajout de l\'index ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = $name['name'];

        $this->addQuery("DROP INDEX $name", 'Suppression de l\'index ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if ($old != $new) {
            $this->drop($old);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        $this->addQuery("ALTER INDEX $oldName RENAME TO $newName", 'Renommage de l\'index ' . $oldName . ' en ' . $newName);
    }

}