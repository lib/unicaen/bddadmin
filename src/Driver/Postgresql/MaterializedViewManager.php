<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\MaterializedViewManagerInteface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class MaterializedViewManager extends AbstractManager implements MaterializedViewManagerInteface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('schemaname', 'matviewname');
        $data = [];

        $q = "
          SELECT 
            schemaname \"schema\",
            matviewname \"name\",
            pg_get_viewdef(schemaname || '.' || matviewname, true) definition
          FROM 
            pg_catalog.pg_matviews
          WHERE 
            schemaname NOT IN ('pg_catalog','information_schema')
            $f
          ORDER BY
            schemaname,
            matviewname
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $name = Util::fullObjectName($r['schema'], $r['name']);
            $r['definition'] = $r['definition'];
            $data[$name] = [
                'schema'     => $r['schema'],
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], true),
            ];
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        if (!str_starts_with(strtoupper($data['definition']), 'CREATE ')){
            $data['definition'] = 'CREATE MATERIALIZED VIEW '.$name.' AS'."\n".$data['definition'];
        }
        $data['definition'] .= "\nWITH DATA";

        $this->addQuery($data['definition'], 'Ajout de la vue matérialisée ' . $name);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) {
            $name = Util::fullObjectName($name['schema'] ?? null, $name['name']);
        }

        $this->addQuery("DROP MATERIALIZED VIEW " . $name, 'Suppression de la vue matérialisée ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;
        if (!isset($old['schema'])){
            $old['schema'] = 'public';
        }
        if (!isset($new['schema'])){
            $new['schema'] = 'public';
        }
        if ($old != $new) {
            $this->drop($old);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        $this->addQuery("ALTER MATERIALIZED VIEW $oldName RENAME TO $newName", 'Renommage de la vue matérialisée ' . $oldName . ' en ' . $newName);
    }



    /**
     * @param string|array $name
     */
    public function refresh(string|array $name): void
    {
        if (is_array($name)) {
            $name = Util::fullObjectName($name['schema'] ?? '', $name['name']);
        }

        $this->addQuery("REFRESH MATERIALIZED VIEW $name WITH DATA", 'Mise à jour de la vue matérialisée ' . $name);
    }
}