<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\PrimaryConstraintManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class PrimaryConstraintManager extends AbstractManagerDdlConstraint implements PrimaryConstraintManagerInterface
{
    protected string $description = 'clé primaire';



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $filter = DdlFilter::normalize2([$name]);
            [$f, $p] = $filter->toSql('c.constraint_schema', 'c.constraint_name');
        }

        $sql = "
            SELECT
              c.constraint_schema,
              c.constraint_name
            FROM
              information_schema.table_constraints c
            WHERE
              c.constraint_type = 'PRIMARY KEY'
              AND c.constraint_schema NOT IN ('pg_catalog','information_schema')
              $f
            ORDER BY
              c.constraint_schema, c.constraint_name 
            ";

        $list = [];
        $parser = $r = $this->bdd->selectEach($sql, $p);
        while ($r = $parser->next()) {
            $name = Util::fullObjectName($r['constraint_schema'], $r['constraint_name']);
            $list[] = $name;
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('ns.nspname', 'c.conname');

        $sql = "
            SELECT
              ns.nspname constraint_schema,
              c.conname constraint_name,
              nst.nspname table_schema,
              t.relname table_name,
              c.conindid::regclass index_name,
              cu.column_name
            FROM
              pg_catalog.pg_constraint c
              JOIN pg_catalog.pg_namespace ns ON ns.oid = c.connamespace
              JOIN pg_class t ON t.oid = c.conrelid
              JOIN pg_catalog.pg_namespace nst ON nst.oid = t.relnamespace
              JOIN information_schema.constraint_column_usage cu ON cu.constraint_schema = ns.nspname AND cu.constraint_name = c.conname
            WHERE
              c.contype='p' -- primary key
              AND ns.nspname NOT IN ('pg_catalog','information_schema')
              $f
            ORDER BY
              ns.nspname, c.conname, cu.column_name
            ";

        $data = [];
        $parser = $r = $this->bdd->selectEach($sql, $p);
        while ($r = $parser->next()) {
            $name = Util::fullObjectName($r['constraint_schema'], $r['constraint_name']);
            if (!array_key_exists($name, $data)) {
                $data[$name] = [
                    'schema'  => $r['constraint_schema'],
                    'name'    => $r['constraint_name'],
                    'table'   => Util::fullObjectName($r['table_schema'], $r['table_name']),
                    'index'   => $r['index_name'],
                    'columns' => [],
                ];
            }
            $data[$name]['columns'][] = $r['column_name'];
        }

        return $data;
    }



    public function makeCreate(array $data): string
    {
        $table = Util::fullObjectName($data['schema'] ?? null, $data['table']);
        $name = $data['name'];
        $cols = implode(', ', $data['columns']);

        $sql = "ALTER TABLE $table ADD CONSTRAINT $name PRIMARY KEY ($cols)";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::create($data);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::drop($name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->isDiff($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            parent::alter($old, $new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        parent::rename($oldName, $new);
    }



    /**
     * @param string|array $name
     */
    public function enable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::enable($name);
    }



    /**
     * @param string|array $name
     */
    public function disable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::disable($name);
    }



    /**
     * @return PrimaryConstraintManagerInterface
     */
    public function enableAll(): PrimaryConstraintManagerInterface
    {
        $this->bdd->logBegin("Activation de toutes les clés primaires");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés primaires ont été activées');

        return $this;
    }



    /**
     * @return PrimaryConstraintManagerInterface
     */
    public function disableAll(): PrimaryConstraintManagerInterface
    {
        $this->bdd->logBegin("Désactivation de toutes les clés primaires");
        $l = $this->getList();
        foreach ($l as $d) {
            $this->bdd->logMsg("Clé primaire " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés primaires ont été désactivées');

        return $this;
    }
}