<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\RefConstraintManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class RefConstraintManager extends AbstractManagerDdlConstraint implements RefConstraintManagerInterface
{
    protected string $description = 'clé étrangère';



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $filter = DdlFilter::normalize2([$name]);
            [$f, $p] = $filter->toSql('c.constraint_schema', 'c.constraint_name');
        }

        $sql = "
            SELECT
              c.constraint_schema,
              c.constraint_name
            FROM
              information_schema.table_constraints c
            WHERE
              c.constraint_type = 'FOREIGN KEY'
              AND c.constraint_schema not in ('pg_catalog','information_schema')
              $f
            ";

        $list = [];
        $parser = $r = $this->bdd->selectEach($sql, $p);
        while ($r = $parser->next()) {
            $name = Util::fullObjectName($r['constraint_schema'], $r['constraint_name']);
            $list[] = $name;
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('nams.nspname', 'cons.conname');

        $sql = "
            SELECT
              nams.nspname     constraint_schema,
              cons.conname     constraint_name,
              cnams.nspname    table_schema,
              clas.relname     table_name,
              inams.nspname    index_schema,
              iclas.relname    index_name,
              cons.confupdtype update_rule,
              cons.confdeltype delete_rule,
              rnams.nspname    rtable_schema,
              rclas.relname    rtable_name,
              attr.attname     col,
              cattr.attname    rcol
            FROM
              pg_catalog.pg_constraint cons
              JOIN pg_catalog.pg_namespace nams ON nams.oid = cons.connamespace
              JOIN pg_class clas ON clas.oid = cons.conrelid
              JOIN pg_catalog.pg_namespace cnams ON cnams.oid = clas.relnamespace
              JOIN pg_class rclas ON rclas.oid = cons.confrelid
              JOIN pg_catalog.pg_namespace rnams ON rnams.oid = rclas.relnamespace
              JOIN pg_attribute attr ON attr.attrelid = clas.oid
              JOIN pg_attribute cattr ON cattr.attrelid = rclas.oid
              JOIN pg_class iclas ON iclas.oid = cons.conindid
              JOIN pg_catalog.pg_namespace inams ON inams.oid = iclas.relnamespace
            WHERE
              cons.contype = 'f'
              AND nams.nspname not in ('pg_catalog','information_schema')
              $f
              AND (
              (     attr.attnum = cons.conkey[1] AND cattr.attnum = cons.confkey[1])
                OR (attr.attnum = cons.conkey[2] AND cattr.attnum = cons.confkey[2])
                OR (attr.attnum = cons.conkey[3] AND cattr.attnum = cons.confkey[3])
                OR (attr.attnum = cons.conkey[4] AND cattr.attnum = cons.confkey[4])
                OR (attr.attnum = cons.conkey[5] AND cattr.attnum = cons.confkey[5])
                OR (attr.attnum = cons.conkey[6] AND cattr.attnum = cons.confkey[6])
                OR (attr.attnum = cons.conkey[7] AND cattr.attnum = cons.confkey[7])
                OR (attr.attnum = cons.conkey[8] AND cattr.attnum = cons.confkey[8])
                OR (attr.attnum = cons.conkey[9] AND cattr.attnum = cons.confkey[9])
                OR (attr.attnum = cons.conkey[10] AND cattr.attnum = cons.confkey[10])
                OR (attr.attnum = cons.conkey[11] AND cattr.attnum = cons.confkey[11])
                OR (attr.attnum = cons.conkey[12] AND cattr.attnum = cons.confkey[12])
                OR (attr.attnum = cons.conkey[13] AND cattr.attnum = cons.confkey[13])
                OR (attr.attnum = cons.conkey[14] AND cattr.attnum = cons.confkey[14])
                OR (attr.attnum = cons.conkey[15] AND cattr.attnum = cons.confkey[15])
                OR (attr.attnum = cons.conkey[16] AND cattr.attnum = cons.confkey[16])
                OR (attr.attnum = cons.conkey[17] AND cattr.attnum = cons.confkey[17])
                OR (attr.attnum = cons.conkey[18] AND cattr.attnum = cons.confkey[18])
                OR (attr.attnum = cons.conkey[19] AND cattr.attnum = cons.confkey[19])
                OR (attr.attnum = cons.conkey[20] AND cattr.attnum = cons.confkey[20])
              )
            ";

        $rules = [
            'a' => 'NO ACTION',
            'r' => 'RESTRICT',
            'c' => 'CASCADE',
            'n' => 'SET NULL',
            'd' => 'SET DEFAULT'
        ];

        $data = [];
        $parser = $r = $this->bdd->selectEach($sql, $p);
        while ($r = $parser->next()) {
            $name = Util::fullObjectName($r['constraint_schema'], $r['constraint_name']);
            if (!array_key_exists($name, $data)) {
                $data[$name] = [
                    'schema'      => $r['constraint_schema'],
                    'name'        => $r['constraint_name'],
                    'table'       => Util::fullObjectName($r['table_schema'], $r['table_name']),
                    'rtable'      => Util::fullObjectName($r['rtable_schema'], $r['rtable_name']),
                    'update_rule' => $rules[$r['update_rule'] ?? 'a'],
                    'delete_rule' => $rules[$r['delete_rule'] ?? 'a'],
                    'index'       => Util::fullObjectName($r['index_schema'], $r['index_name']),
                    'columns'     => [],
                ];
            }
            $data[$name]['columns'][$r['col']] = $r['rcol'];
        }

        return $data;
    }



    public function makeCreate(array $data): string
    {
        $table = $data['table'];
        $name = Util::fullObjectName($data['schema'],$data['name']);
        $rtable = $data['rtable'];
        $updRule = $data['update_rule'] ?? 'NO ACTION';
        $delRule = $data['delete_rule'] ?? 'NO ACTION';

        $cols = implode(', ', array_keys($data['columns']));
        $rcols = implode(', ', array_values($data['columns']));

        $sql = "ALTER TABLE $table ADD CONSTRAINT $name FOREIGN KEY ($cols) REFERENCES $rtable ($rcols) MATCH SIMPLE ON UPDATE $updRule ON DELETE $delRule";

        return $sql;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::create($data);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::drop($name);
    }



    public function alter(array $old, array $new): void
    {
        if ($this->isDiff($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            parent::alter($old, $new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        parent::rename($oldName, $new);
    }



    /***
     * @param string|array $name
     */
    public function enable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::enable($name);
    }



    /***
     * @param string|array $name
     */
    public function disable($name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        parent::disable($name);
    }



    /**
     * @return RefConstraintManagerInterface
     */
    public function enableAll(): RefConstraintManagerInterface
    {
        $this->bdd->logBegin("Activation de toutes les clés étrangères");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Contrainte " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés étrangères ont été activées');

        return $this;
    }



    /**
     * @return RefConstraintManagerInterface
     */
    public function disableAll(): RefConstraintManagerInterface
    {
        $this->bdd->logBegin("Désactivation de toutes les clés étrangères");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Contrainte " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Toutes les clés étrangères ont été désactivées');

        return $this;
    }
}