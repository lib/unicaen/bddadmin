<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\SchemaManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class SchemaManager extends AbstractManager implements SchemaManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql(null,'schema_name');
        $data = [];

        $qr = $this->bdd->select("
          SELECT 
            schema_name \"name\" 
          FROM 
            information_schema.schemata 
          WHERE 
            schema_owner = 'pg_database_owner'
            AND schema_name <> 'public'
            $f
          ORDER BY 
            schema_name
        ", $p);
        foreach ($qr as $r) {
            $data[$r['name']] = $r;
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $sql = "CREATE SCHEMA " . $data['name'] . " AUTHORIZATION pg_database_owner";
        $this->addQuery($sql, 'Ajout du schéma ' . $data['name']);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)){
            $name = $name['name'];
            $cascade = $name['cascade'] ?? false;
        }else{
            $cascade = false;
        }

        $sql = "DROP SCHEMA $name";
        if ($cascade){
            $sql .= ' CASCADE';
        }
        $this->addQuery($sql, 'Suppression du schéma ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        // on ne peut pas modifier un schéma
        // @todo gérer les owners de schémas
    }



    public function rename(string $oldName, array|string $new): void
    {
        if (is_array($new)){
            $newName = $new['name'];
        }else{
            $newName = $new;
        }

        if ($newName !== $oldName) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $sql = "ALTER SCHEMA $oldName RENAME TO $newName";
            $this->addQuery($sql, "Renommage du schéma $oldName en $newName");
        }
    }



    /**
     * @inheritDoc
     */
    public function prepareRenameCompare(array $data): array
    {
        return $data;
    }

}