<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\SequenceManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class SequenceManager extends AbstractManager implements SequenceManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('schemaname', 'sequencename');
        $data = [];

        $qr = $this->bdd->select("
        SELECT 
          schemaname \"schema\",
          sequencename \"name\"
        FROM 
          pg_catalog.pg_sequences
        WHERE
          schemaname NOT IN ('pg_catalog','information_schema')
          $f
        ORDER BY
          schemaname, sequencename
        ", $p);
        foreach ($qr as $r) {
            $name = Util::fullObjectName($r['schema'], $r['name']);
            $data[$name] = $r;
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        $minValue = isset($data['minvalue']) ? (int)$data['minvalue'] : 1;

        $sql = "CREATE SEQUENCE $name INCREMENT BY 1 MINVALUE $minValue";
        $this->addQuery($sql, 'Ajout de la séquence ' . $name);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) {
            $name = Util::fullObjectName($name['schema'] ?? null, $name['name']);
        }

        $sql = "DROP SEQUENCE $name";
        $this->addQuery($sql, 'Suppression de la séquence ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        $oldNormalized = Util::fullObjectName($old['schema'] ?? 'public', $old['name']);
        $newNormalized = Util::fullObjectName($new['schema'] ?? 'public', $new['name']);

        if ($oldNormalized != $newNormalized) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->drop($old);
            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_string($new)) {
            [$newSchema, $newName] = Util::explodedFullObjectName($new);
            $new = ['name' => $newName];
            if ($newSchema) {
                $new['schema'] = $newSchema;
            }
        }

        $this->drop($oldName);
        $this->create($new);
    }



    public function nextVal(string|array $sequence): int
    {
        if (is_string($sequence)) {
            $sequenceName = $sequence;
        } else {
            $sequenceName = Util::fullObjectName($sequence['schema'], $sequence['name']);
        }

        $r = $this->bdd->selectOne("SELECT nextval('$sequenceName') seqval");

        return (int)$r['seqval'];
    }



    /**
     * @inheritDoc
     */
    public function prepareRenameCompare(array $data): array
    {
        return $data;
    }

}