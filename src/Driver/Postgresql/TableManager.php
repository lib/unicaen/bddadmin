<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\TableManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Exception\BddException;
use Unicaen\BddAdmin\Util;

class TableManager extends AbstractManager implements TableManagerInterface
{
    private function interpreterCommentaire(?string $commentaire): array
    {
        $data = [];
        if ($commentaire) {
            $keys = [
                'sequence',
                'columns-order',
            ];

            $commentaire = strtoupper($commentaire);
            $commentaire = str_replace(' ', '', $commentaire);
            $commentaire = str_replace("\t", '', $commentaire);
            $commentaire = str_replace("\n", '', $commentaire);

            foreach ($keys as $key) {
                $kpos = strpos($commentaire, strtoupper($key) . '=');
                if (false !== $kpos) {
                    $vend = strpos($commentaire, ';', $kpos + 1);
                    $data[$key] = substr($commentaire, $kpos + strlen($key) + 1, $vend - ($kpos + strlen($key) + 1));
                }
            }
        }

        return $data;
    }



    public function getList(?string $name = null): array
    {
        $p = [];
        $f = "";
        if ($name) {
            $filter = DdlFilter::normalize2([$name]);
            [$f, $p] = $filter->toSql('table_schema', 'table_name');
        }

        $sql = "
        SELECT 
          table_schema \"schema\",
          table_name   \"name\"
        FROM
          information_schema.tables 
        WHERE
          table_type = 'BASE TABLE'
          AND table_schema NOT IN ('pg_catalog','information_schema')
          $f
        ";

        $list = [];
        $r = $this->bdd->select($sql, $p);
        foreach ($r as $l) {
            $list[] = Util::fullObjectName($l['schema'], $l['name']);
        }

        return $list;
    }



    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('ns.nspname', 't.relname');
        $data = [];

        $q = "SELECT
            ns.nspname                 \"schema\",
            t.relname                  \"name\",
            t.relpersistence           relpersistence,
            c.column_name              cname,
            c.data_type                \"type\",
            c.character_maximum_length length,
            c.numeric_scale            scale,
            COALESCE(c.datetime_precision,c.numeric_precision) \"precision\",
            c.is_nullable              nullable,
            c.column_default           \"default\",
            c.ordinal_position         \"position\",
            pg_catalog.col_description(pgcc.oid, c.ordinal_position::int) col_commentaire,
            obj_description(t.oid)     commentaire,
            s.sequencename             \"sequence\"
          FROM
                 pg_catalog.pg_class        t
            JOIN pg_catalog.pg_namespace   ns ON ns.oid = t.relnamespace
            JOIN information_schema.columns c ON c.table_schema = ns.nspname and c.table_name = t.relname 
            JOIN pg_catalog.pg_class     pgcc ON pgcc.oid = (SELECT ('\"' || c.table_schema || '\".\"' || c.table_name || '\"')::regclass::oid)
            LEFT JOIN pg_catalog.pg_sequences s ON s.schemaname = ns.nspname AND s.sequencename = t.relname || '_id_seq'
          WHERE
            t.relkind = 'r'
            AND ns.nspname not in ('pg_catalog','information_schema') 
            $f
          ORDER BY
            ns.nspname, t.relname, c.column_name
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $paq) {
            $name = Util::fullObjectName($paq['schema'], $paq['name']);
            if (!isset($data[$name])) {
                $data[$name] = [
                    'schema'      => $paq['schema'],
                    'name'        => $paq['name'],
                    'temporary'   => $paq['relpersistence'] == 't',
                    'logging'     => $paq['relpersistence'] == 'p',
                    'commentaire' => $paq['commentaire'],
                    'sequence'    => $paq['sequence'],
                    'columns'     => [],
                ];
                if ($commData = $this->interpreterCommentaire($paq['commentaire'])) {
                    foreach ($commData as $k => $v) {
                        $data[$paq['name']][$k] = $v;
                    }
                }
            }

            $column = [
                'name'        => $paq['cname'],
                'type'        => null,
                'bdd-type'    => $paq['type'],
                'length'      => (int)$paq['length'],
                'scale'       => $paq['scale'] ? (int)$paq['scale'] : null,
                'precision'   => $paq['precision'] ? (int)$paq['precision'] : null,
                'nullable'    => $paq['nullable'] == 'YES',
                'default'     => $this->purgeDefaultVal($paq['default']),
                'position'    => $paq['position'],
                'commentaire' => $paq['col_commentaire'],
            ];

            $this->typeFromBdd($column);

            if (empty($column['type'])) {
                throw new BddException('Le type de colonne "' . $column['bdd-type'] . '" n\'est pas reconnu pour ' . $name . '.' . $column['cname']);
            }

            $data[$name]['columns'][$paq['cname']] = $column;
        }

        foreach ($data as $table => $tdata) {
            $colPos = isset($tdata['columns-order']) ? explode(',', $tdata['columns-order']) : [];
            $oriColPos = [];
            foreach ($tdata['columns'] as $cname => $column) {
                $oriColPos[$column['position']] = $cname;
            }
            ksort($oriColPos);
            foreach ($oriColPos as $cname) {
                if (!in_array($cname, $colPos)) {
                    $colPos[] = $cname;
                }
            }
            $position = 1;
            foreach ($colPos as $cname) {
                if (isset($data[$table]['columns'][$cname])) {
                    $data[$table]['columns'][$cname]['position'] = $position;
                    $position++;
                }
            }
        }

        return $data;
    }



    private function purgeDefaultVal(?string $default): ?string
    {
        if (null === $default || '' === $default) {
            return $default;
        }

        $default = $this->purger($default);
        if ('NULL' === $default) $default = null;

        if (str_ends_with($default, '::character varying')) {
            return substr($default, 0, strlen($default) - 19);
        }

        return $default;
    }



    protected function makeCreate(array $data)
    {
        $sql = "CREATE ";
        if ($data['temporary']) {
            $sql .= "TEMP ";
        } elseif (isset($data['logging']) && $data['logging'] == false) {
            $sql .= 'UNLOGGED ';
        }

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        $sql .= "TABLE $name\n   (\t";

        $cols = [];
        $columns = $data['columns'];
        uasort($columns, function ($a, $b) {
            $apos = isset($a['position']) ? $a['position'] : 1;
            $bpos = isset($b['position']) ? $b['position'] : 1;

            return $apos - $bpos;
        });
        foreach ($columns as $column) {
            $cp = ['"' . $column['name'] . '"', $this->makeColumnType($column)];
            if ($column['default'] !== null) {
                $cp[] = 'DEFAULT ' . $column['default'];
            }
            if (!$column['nullable']) {
                $cp[] = "NOT NULL";
            }

            $cols[] = implode(" ", $cp);
        }
        $sql .= implode(",\n\t", $cols);
        $sql .= "\n   )";

        return $sql;
    }



    protected function makeCreateComm(array $data, bool $forceUpdateNull = false): ?string
    {
        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        if ($data['commentaire']) {
            $comm = "'" . str_replace("'", "''", $data['commentaire']) . "'";

            return 'COMMENT ON TABLE ' . $name . ' IS ' . $comm;
        }

        if ($forceUpdateNull) {
            return 'COMMENT ON TABLE ' . $name . ' IS \'\'';
        } else {
            return null;
        }
    }



    private function makeColumnType(array $column): string
    {
        if (isset($column['bdd-type'])) {
            $resType = $column['bdd-type'];
        } else {
            $resType = null;
        }

        if (!$resType) {
            switch ($column['type']) {
                // booléens
                case Bdd::TYPE_BOOL:
                    $resType = 'boolean';
                    break;

                // entiers
                case Bdd::TYPE_INT:
                    switch ($column['precision']) {
                        case 2:
                            $resType = 'smallint';
                            break;
                        case 4:
                            $resType = 'integer';
                            break;
                        case 8:
                            $resType = 'bugint';
                            break;
                        default:
                            $resType = 'integer';
                    }
                    break;

                // nombres réels
                case Bdd::TYPE_FLOAT:
                    if (empty($column['scale'])) {
                        switch ($column['precision']) {
                            case 4:
                                $resType = 'real';
                                break;
                            case 8:
                                $resType = 'double precision';
                                break;
                            default:
                                $resType = 'double precision';
                        }
                    } else {
                        $resType = 'numeric';
                    }
                    break;

                // Chaînes de caractères
                case Bdd::TYPE_STRING:
                    $resType = 'character varying';
                    break;
                case Bdd::TYPE_CLOB:
                    $resType = 'text';
                    break;

                // Dates
                case Bdd::TYPE_DATE:
                    $resType = 'timestamp with time zone';
                    break;

                // données binaires (BYTE)
                case Bdd::TYPE_BLOB:
                    $resType = 'bytea';
                    break;
            }
        }

        if ($resType == 'character varying' && $column['length']) {
            $resType .= '(' . $column['length'] . ')';
        }

        if ($resType == 'numeric' && $column['precision'] && $column['scale']) {
            $resType .= '(' . $column['precision'] . ',' . $column['scale'] . ')';
        }

        return $resType;
    }



    protected function typeFromBdd(array &$column): void
    {
        switch ($column['bdd-type']) {
            // booléens
            case 'boolean':
                $column['type'] = Bdd::TYPE_BOOL;
                break;

            // entiers
            case 'smallint':
                $column['type'] = Bdd::TYPE_INT;
                $column['precision'] = 2;
                break;

            case 'integer':
                $column['type'] = Bdd::TYPE_INT;
                $column['precision'] = 4;
                break;

            case 'bigint':
                $column['type'] = Bdd::TYPE_INT;
                $column['precision'] = 8;
                break;

            case 'oid':
                $column['type'] = Bdd::TYPE_INT;
                break;

            // nombres réels
            case 'real':
                $column['type'] = Bdd::TYPE_FLOAT;
                $column['precision'] = 4;
                $column['scale'] = null;
                break;

            case 'double precision':
                $column['type'] = Bdd::TYPE_FLOAT;
                $column['precision'] = 8;
                $column['scale'] = null;
                break;

            case 'numeric':
                $column['type'] = Bdd::TYPE_FLOAT;
                break;

            // Chaînes de caractères
            case 'character varying':
                $column['type'] = Bdd::TYPE_STRING;
                break;

            case 'character':
                $column['type'] = Bdd::TYPE_STRING;
                break;

            case 'uuid':
                $column['type'] = Bdd::TYPE_STRING;
                break;

            case 'text':
                $column['type'] = Bdd::TYPE_CLOB;
                break;

            case 'jsonb':
                $column['type'] = Bdd::TYPE_CLOB;
                break;

            case 'json':
                $column['type'] = Bdd::TYPE_CLOB;
                break;

            // Dates
            case 'date':
                $column['type'] = Bdd::TYPE_DATE;
                break;

            case 'timestamp without time zone':
                $column['type'] = Bdd::TYPE_DATE;
                break;

            case 'timestamp with time zone':
                $column['type'] = Bdd::TYPE_DATE;
                break;

            // données binaires (BYTE)
            case 'bytea':
                $column['type'] = Bdd::TYPE_BLOB;
                break;
        }
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        /* Création de la table */
        $this->addQuery($this->makeCreate($data), 'Ajout de la table ' . $name);

        /* Création du commentaire éventuel de la table */
        if ($comm = $this->makeCreateComm($data)) {
            $this->addQuery($comm, 'Ajout de commentaire sur la table ' . $name);
        }

        /* Création des commentaires éventuels de colonnes */
        foreach ($data['columns'] as $column) {
            $this->alterColumnComment($name, ['commentaire' => null], $column);
        }
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) $name = Util::fullObjectName($name['schema'] ?? null, $name['name']);

        $this->addQuery("DROP TABLE $name", 'Suppression de la table ' . $name);
    }



    public function majSequence(array $data): void
    {
        if (!isset($data['sequence'])) return;

        $idCol = $this->bdd->getOption(Bdd::OPTION_ID_COLUMN);

        if (!isset($data['columns'][$idCol])) return;

        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        $seqVal = (int)$this->bdd->selectOne("SELECT COALESCE(MAX(id+1),1) seq_id FROM $name", [], 'seq_id');

        $sequence = Util::fullObjectName($data['schema'] ?? null, $data['sequence']);

        $sql = "ALTER SEQUENCE $sequence RESTART WITH $seqVal";

        $this->addQuery($sql, 'Mise à jour de la séquence ' . $data['sequence']);
    }



    public function isDiff(array $d1, array $d2): bool
    {
        foreach ($d1 as $key => $val) {
            if ($key != 'columns') {
                if ($d1[$key] !== $d2[$key]) {
                    return true;
                }
            }
        }

        if (array_keys($d1['columns']) != array_keys($d2['columns'])) {
            return true; // différences de colonnes
        }

        foreach ($d1['columns'] as $column => $colParams) {
            if ($this->isColDiff($d1['columns'][$column], $d2['columns'][$column])) {
                return true;
            }
        }

        return false;
    }



    private function isColDiff(array $col1, array $col2): bool
    {
        return $this->isColDiffType($col1, $col2)
            || $this->isColDiffPrecision($col1, $col2)
            || $this->isColDiffDefault($col1, $col2)
            || $this->isColDiffNullable($col1, $col2)
            || $this->isColDiffComment($col1, $col2);
    }



    private function isColDiffType(array $col1, array $col2): bool
    {
        return $col1['type'] !== $col2['type']
            || $col1['bdd-type'] !== $col2['bdd-type']
            || $col1['scale'] !== $col2['scale'];
    }



    private function isColDiffPrecision(array $col1, array $col2): bool
    {
        return $col1['length'] !== $col2['length']
            || $col1['precision'] !== $col2['precision'];
    }



    private function isColDiffNullable(array $col1, array $col2): bool
    {
        return $col1['nullable'] !== $col2['nullable'];
    }



    private function isColDiffDefault(array $col1, array $col2): bool
    {
        return $col1['default'] !== $col2['default'];
    }



    private function isColDiffComment(array $col1, array $col2): bool
    {
        return $col1['commentaire'] !== $col2['commentaire'];
    }



    protected function normalized(array $ddl)
    {
        if (!isset($ddl['logging'])) {
            $ddl['logging'] = true;
        }

        return $ddl;
    }



    public function alter(array $old, array $new): void
    {
        $old = $this->normalized($old);
        $new = $this->normalized($new);

        if ($this->isDiff($old, $new)) {
            $name = Util::fullObjectName($new['schema'] ?? null, $new['name']);

            if ($this->sendEvent()->getReturn('no-exec')) return;

            if ($old['logging'] !== $new['logging']) {
                $sql = "ALTER TABLE $name SET ".($new['logging'] ? 'LOGGED' : 'UNLOGGED');
                $this->addQuery($sql, 'Modification du logging de la table ' . $name);
            }

            $newCols = array_diff(array_keys($new['columns']), array_keys($old['columns']));
            $updCols = array_intersect(array_keys($old['columns']), array_keys($new['columns']));
            $delCols = array_diff(array_keys($old['columns']), array_keys($new['columns']));

            foreach ($newCols as $newCol) {
                $this->addColumn($name, $new['columns'][$newCol]);
            }

            foreach ($updCols as $updCol) {
                $cOld = $old['columns'][$updCol];
                $cNew = $new['columns'][$updCol];
                $this->alterColumnType($name, $cOld, $cNew);
                $this->alterColumnNullable($name, $cOld, $cNew);
                $this->alterColumnDefault($name, $cOld, $cNew);
                $this->alterColumnComment($name, $cOld, $cNew);
            }

            foreach ($delCols as $delCol) {
                $this->dropColumn($name, $old['columns'][$delCol]);
            }

            if ($old['commentaire'] !== $new['commentaire']) {
                $this->addQuery($this->makeCreateComm($new, true), 'Modification du commentaire de la table ' . $name);
            }
        }
    }



    private function isEmpty(string $table): bool
    {
        $r = $this->bdd->selectOne('SELECT * FROM ' . $table);

        return null === $r;
    }



    private function hasEmptyValue(string $table, string $column): bool
    {
        $r = $this->bdd->selectOne('SELECT ' . $column . ' FROM ' . $table . ' WHERE ' . $column . ' IS NULL');

        return null !== $r;
    }



    private function addColumn(string $table, array $column, $noNotNull = false): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $cp = ['"' . $column['name'] . '"', $this->makeColumnType($column)];
        if ($column['default'] !== null) {
            $cp[] = 'DEFAULT ' . $column['default'];
        }

        if (!$column['nullable'] && !$noNotNull) {
            if ($column['default'] === null && !$this->isEmpty($table)) {
                $this->bdd->logMsg("La colonne $table." . $column['name'] . " ne sera pas déclarée NOT NULL, car des données sont déjà présentes dans la table et aucune valeur par défaut n'a été configurée");
            } else {
                $cp[] = "NOT NULL";
            }
        }

        $sql = "ALTER TABLE $table ADD COLUMN" . implode(" ", $cp);
        $this->addQuery($sql, 'Ajout de la colonne ' . $column['name'] . ' sur la table ' . $table);

        /* Ajout du commentaire éventuel de la colonne */
        $this->alterColumnComment($table, ['commentaire' => null], $column);
    }



    private function dropColumn(string $table, array $column): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $column = $column['name'];
        $this->addQuery(
            "ALTER TABLE $table DROP COLUMN $column",
            'Suppression de la colonne ' . $column . ' sur la table ' . $table
        );
    }



    private function alterColumnType(string $table, array $old, array $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $column = $new['name'];
        if ($this->isColDiffType($old, $new) || $this->isColDiffPrecision($old, $new)) {
            $sql = "ALTER TABLE $table ALTER COLUMN $column TYPE " . $this->makeColumnType($new);
            $this->addQuery($sql, 'Changement du type de la colonne ' . $column . ' de la table ' . $table);
        }
    }



    private function alterColumnNullable(string $table, array $old, array $new): void
    {
        $column = $new['name'];
        if ($this->isColDiffNullable($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            if ($this->hasEmptyValue($table, $column)) {
                $this->bdd->logMsg('La colonne ' . $table . '.' . $column . ' n\'a pas pu être déclarée NOT NULL');
            } else {
                $sql = "ALTER TABLE $table ALTER COLUMN $column " . ($new['nullable'] ? 'DROP' : 'SET') . " NOT NULL";
                $this->addQuery($sql, 'Changement d\'état de la colonne ' . $column . ' de la table ' . $table);
            }
        }
    }



    private function alterColumnDefault(string $table, array $old, array $new): void
    {
        $column = $new['name'];
        if ($this->isColDiffDefault($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $default = $new['default'];
            if (null === $default) $default = 'NULL';
            $sql = "ALTER TABLE $table ALTER COLUMN $column SET DEFAULT $default";
            $this->addQuery($sql, 'Changement de valeur par défaut de la colonne ' . $column . ' de la table ' . $table);
        }
    }



    private function alterColumnComment(string $table, array $old, array $new): void
    {
        $column = $new['name'];
        if ($this->isColDiffComment($old, $new)) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $commentaire = $new['commentaire'];
            if ($commentaire) {
                $commentaire = "'" . str_replace("'", "''", $commentaire) . "'";
                $sql = "COMMENT ON COLUMN $table.\"$column\" IS $commentaire";
                $this->addQuery($sql, 'Modification du commentaire de la colonne ' . $column . ' de la table ' . $table);
            } else {
                $sql = "COMMENT ON COLUMN $table.\"$column\" IS ''";
                $this->addQuery($sql, 'Suppression du commentaire de la colonne ' . $column . ' de la table ' . $table);
            }
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        $sql = "ALTER TABLE $oldName RENAME TO $newName";
        $this->addQuery($sql, 'Renommage de la table ' . $oldName . ' en ' . $newName);
    }
}