<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\TriggerManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class TriggerManager extends AbstractManager implements TriggerManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('n.nspname', 't.tgname');
        $data = [];

        $q = "
        SELECT
          n.nspname \"schema\",
          t.tgname  \"name\",
          c.relname table_name,
          pg_catalog.pg_get_triggerdef(t.oid, true) definition,
          CASE t.tgenabled
            WHEN 'O' THEN 'enabled'
            WHEN 'D' THEN 'disabled'
            WHEN 'R' THEN 'replica'
            WHEN 'A' THEN 'always'
          END       status
        FROM
          pg_catalog.pg_trigger t
          JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
          JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE
          NOT t.tgisinternal
          $f
        ORDER BY
          schema, name
        ";

        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $name = Util::fullObjectName($r['schema'], $r['name']);
            $data[$name] = [
                'schema'     => $r['schema'],
                'table'      => $r['table_name'],
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], false),
                'status'     => $r['status']
            ];
        }

        return $data;
    }



    public function create(array $data): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        $this->addQuery($data['definition'], 'Ajout/modification du trigger ' . $name);
    }



    public function drop(array|string $name, ?string $table = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) {
            if (!$table) {
                $table = Util::fullObjectName($name['schema'] ?? '', $name['table']);
            }
            $name = $name['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($name);
        }

        $this->addQuery("DROP TRIGGER $name ON $table", 'Suppression du trigger ' . $name);
    }



    public function enable(array|string $name, ?string $table = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)){
            $table = Util::fullObjectName($name['schema'] ?? '', $name['table']);
            $name = $name['name'];
        }

        $this->addQuery("ALTER TABLE $table ENABLE TRIGGER $name", 'Activation du trigger ' . $name);
    }



    public function disable(array|string $name, ?string $table = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)){
            $table = Util::fullObjectName($name['schema'] ?? '', $name['table']);
            $name = $name['name'];
        }

        $this->addQuery("ALTER TABLE $table DISABLE TRIGGER $name", 'Désactivation du trigger ' . $name);
    }



    public function enableAll(): TriggerManagerInterface
    {
        $this->bdd->logBegin("Activation de tous les déclencheurs");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Déclencheur " . $d['name'], true);
            try {
                $this->enable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Tous les déclencheurs ont été activés');

        return $this;
    }



    public function disableAll(): TriggerManagerInterface
    {
        $this->bdd->logBegin("Désactivation de tous les déclencheurs");
        $l = $this->get();
        foreach ($l as $d) {
            $this->bdd->logMsg("Déclencheur " . $d['name'], true);
            try {
                $this->disable($d);
            } catch (\Throwable $e) {
                $this->bdd->logError($e);
            }
        }
        $this->bdd->logEnd('Tous les déclencheurs ont été désactivés');

        return $this;
    }



    public function alter(array $old, array $new): void
    {
        if (!isset($old['schema'])){
            $old['schema'] = 'public';
        }
        if (!isset($new['schema'])){
            $new['schema'] = 'public';
        }
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new, ?string $table = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
            if (!$table) {
                $table = Util::fullObjectName($new['schema'] ?? '', $new['table']);
            }
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        $sql = "ALTER TRIGGER $oldName ON $table RENAME TO $newName";
        $this->addQuery($sql, 'Renommage du trigger ' . $oldName . ' en ' . $newName);
    }

}