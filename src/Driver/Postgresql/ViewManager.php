<?php

namespace Unicaen\BddAdmin\Driver\Postgresql;

use Unicaen\BddAdmin\Manager\AbstractManager;
use Unicaen\BddAdmin\Manager\ViewManagerInterface;
use Unicaen\BddAdmin\Ddl\DdlFilter;
use Unicaen\BddAdmin\Util;

class ViewManager extends AbstractManager implements ViewManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        $filter = DdlFilter::normalize2($includes, $excludes);
        [$f, $p] = $filter->toSql('v.table_schema', 'v.table_name');
        $data = [];

        $q = "
          SELECT 
            v.table_schema    \"schema\",
            v.table_name      \"name\",
            pg_get_viewdef(v.table_schema || '.' || v.table_name, true) definition
          FROM 
            information_schema.views v
          WHERE 
            v.table_schema NOT IN ('pg_catalog','information_schema')
            $f
          ORDER BY
            v.table_schema,
            v.table_name
        ";
        $p = $this->bdd->select($q, $p);
        foreach ($p as $r) {
            $name = Util::fullObjectName($r['schema'], $r['name']);
            $data[$name] = [
                'schema'     => $r['schema'],
                'name'       => $r['name'],
                'definition' => $this->purger($r['definition'], true),
            ];
        }

        return $data;
    }



    public function create(array $data, $test = null): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        $name = Util::fullObjectName($data['schema'] ?? null, $data['name']);

        if (!str_starts_with(strtoupper($data['definition']), 'CREATE ')){
            $data['definition'] = 'CREATE OR REPLACE VIEW '.$name.' AS'."\n".$data['definition'];
        }

        $this->addQuery($data['definition'], 'Ajout/modification de la vue ' . $name);
    }



    public function drop(array|string $name): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($name)) {
            $name = Util::fullObjectName($name['schema'] ?? null, $name['name']);
        }

        $this->addQuery("DROP VIEW " . $name, 'Suppression de la vue ' . $name);
    }



    public function alter(array $old, array $new): void
    {
        if (!isset($old['schema'])){
            $old['schema'] = 'public';
        }
        if (!isset($new['schema'])){
            $new['schema'] = 'public';
        }
        if ($old != $new) {
            if ($this->sendEvent()->getReturn('no-exec')) return;

            $this->create($new);
        }
    }



    public function rename(string $oldName, array|string $new): void
    {
        if ($this->sendEvent()->getReturn('no-exec')) return;

        if (is_array($new)) {
            $newName = $new['name'];
        } else {
            [$null, $newName] = Util::explodedFullObjectName($new);
        }

        $this->addQuery("ALTER VIEW $oldName RENAME TO $newName", 'Renommage de la vue ' . $oldName . ' en ' . $newName);
    }

}