<?php

namespace Unicaen\BddAdmin\Logger;


class DefaultLogger implements LoggerInterface
{
    const COLOR_LIGHT_RED = '1;31';
    const COLOR_LIGHT_PURPLE = '1;35';
    const COLOR_LIGHT_GREEN  = '1;32';


    protected ?string $lastMessage = null;

    protected bool $lastRewrite = false;



    public function print($text, $color = null, $bgColor = null): void
    {
        if ($bgColor) $bgColor = ';' . $bgColor;

        if (!$color && !$bgColor) {
            echo $text;
        } else {
            echo "\e[$color$bgColor" . "m$text\e[0m";
        }
    }



    public function println($text, $color = null, $bgColor = null): void
    {
        $this->print($text, $color, $bgColor);
        echo "\n";
    }



    public function msg($message, bool $rewrite = false): void
    {
        if ($rewrite) {
            if ($this->lastMessage) {
                $m = $message . str_pad('', strlen($this->lastMessage) - strlen($message) + 2) . "\r";
            } else {
                $m = $message . "\r";
            }
            $this->print($m);
        } else {
            $this->println($message);
        }
        $this->lastMessage = $message;
        $this->lastRewrite = $rewrite;
    }



    public function title(string $title): void
    {
        $spaces = 1;
        $pstr = str_repeat(' ', $spaces);
        $t    = $pstr . $title . $pstr;

        $len = mb_strlen($t);

        echo '╔' . str_repeat('═', $len) . "╗\n";
        echo '║' . str_repeat(' ', $len) . "║\n";
        echo "║" . $t . "║\n";
        echo '║' . str_repeat(' ', $len) . "║\n";
        echo '╚' . str_repeat('═', $len) . "╝\n\n";
    }



    public function success(string $message): void
    {
        if ($this->lastRewrite) $this->println('');
        $this->println($message, self::COLOR_LIGHT_GREEN);
    }
    
    

    public function error(\Throwable|string $e): void
    {
        if ($e instanceof \Throwable) {
            $e = $e->getMessage();
        }
        if ($this->lastRewrite) $this->println('');
        $this->println($e, self::COLOR_LIGHT_RED);
    }



    public function begin(string $title): void
    {
        if ($this->lastMessage) {
            $title .= str_pad('', strlen($this->lastMessage) - strlen($title) + 2);
        }
        $this->lastRewrite = false;
        $this->lastMessage = null;

        $this->println($title, self::COLOR_LIGHT_PURPLE);
    }



    public function end(?string $msg = null): void
    {
        if ($this->lastMessage && $this->lastRewrite) {
            $msg .= str_pad('', strlen($this->lastMessage) - strlen($msg ?? '') + 2);
        }
        if ($msg) {
            $this->println($msg);
        } else {
            $this->println('');
        }
    }

}