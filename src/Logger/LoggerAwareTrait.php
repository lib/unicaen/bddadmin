<?php

namespace Unicaen\BddAdmin\Logger;

use Symfony\Component\Console\Style\SymfonyStyle;

trait LoggerAwareTrait
{

    protected ?LoggerInterface $logger = null;



    /**
     * @return LoggerInterface|null
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }



    /**
     * @param LoggerInterface|null $logger
     *
     * @return self
     */
    public function setLogger(null|LoggerInterface|SymfonyStyle $logger): self
    {
        if ($logger instanceof SymfonyStyle){
            $logger = new SymfonyStyleLogger($logger);
        }
        $this->logger = $logger;

        return $this;
    }



    public function logTitle(string $title): void
    {
        if ($this->logger) $this->logger->title($title);
    }


    public function logSuccess(string $message): void
    {
        if ($this->logger) $this->logger->success($message);
    }


    public function logError(string|\Throwable $e): void
    {
        if ($this->logger) $this->logger->error($e);
    }



    public function logBegin(string $title): void
    {
        if ($this->logger) $this->logger->begin($title);
    }



    public function logEnd(?string $msg = null): void
    {
        if ($this->logger) $this->logger->end($msg);
    }



    public function logMsg(string $message, bool $rewrite = false): void
    {
        if ($this->logger) $this->logger->msg($message, $rewrite);
    }
}