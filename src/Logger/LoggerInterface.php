<?php

namespace Unicaen\BddAdmin\Logger;

interface LoggerInterface
{
    public function title(string $title): void;



    public function success(string $message): void;



    public function error(\Throwable|string $e): void;



    public function begin(string $title): void;



    public function end(?string $msg = null): void;



    public function msg($message, bool $rewrite = false): void;
}