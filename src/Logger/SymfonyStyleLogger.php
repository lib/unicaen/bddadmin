<?php

namespace Unicaen\BddAdmin\Logger;


use Symfony\Component\Console\Style\SymfonyStyle;

class SymfonyStyleLogger implements LoggerInterface
{
    protected ?string $lastMessage = null;

    protected bool $lastRewrite = false;

    protected SymfonyStyle $symfonyStyle;



    public function __construct(SymfonyStyle $symfonyStyle)
    {
        $this->symfonyStyle = $symfonyStyle;
    }



    public function print($text, $color = null, $bgColor = null): void
    {
        $this->symfonyStyle->write($text);
    }



    public function println($text, $color = null, $bgColor = null): void
    {
        $this->symfonyStyle->writeln($text);
    }



    public function msg($message, bool $rewrite = false): void
    {
        if ($rewrite) {
            if ($this->lastMessage) {
                $m = $message . str_pad('', strlen($this->lastMessage) - strlen($message) + 2) . "\r";
            } else {
                $m = $message . "\r";
            }
            $this->print($m);
        } else {
            $this->println($message);
        }
        $this->lastMessage = $message;
        $this->lastRewrite = $rewrite;
    }



    public function title(string $title): void
    {
        $this->symfonyStyle->title($title);
    }



    public function success(string $message): void
    {
        if ($this->lastRewrite) $this->println('');
        $this->symfonyStyle->success($message);
    }



    public function error(\Throwable|string $e): void
    {
        if ($e instanceof \Throwable) {
            $e = $e->getMessage();
        }
        if ($this->lastRewrite) $this->println('');
        $this->symfonyStyle->error($e);
    }



    public function begin(string $title): void
    {
        if ($this->lastMessage) {
            $title .= str_pad('', strlen($this->lastMessage) - strlen($title) + 2);
        }
        $this->lastRewrite = false;
        $this->lastMessage = null;
        $this->symfonyStyle->comment($title);
    }



    public function end(?string $msg = null): void
    {
        if ($this->lastMessage && $this->lastRewrite) {
            $msg .= str_pad('', strlen($this->lastMessage) - strlen($msg ?? '') + 2);
        }
        if ($msg && trim($msg)) {
            $this->symfonyStyle->comment($msg);
        } else {
            $this->println(str_pad(' ', strlen($this->lastMessage ?? '')));
        }
    }

}