<?php

namespace Unicaen\BddAdmin\Manager;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\Event\EventManagerAwareTrait;
use Unicaen\BddAdmin\Exception\BddException;
use Unicaen\BddAdmin\Util;


abstract class AbstractManager implements ManagerInterface
{
    use EventManagerAwareTrait;

    protected Bdd $bdd;



    public function __construct(Bdd $bdd)
    {
        $this->bdd = $bdd;
    }



    protected function purger(string $sql, ?bool $enleverPointVirgule = false): string
    {
        $s = explode("\n", $sql);
        foreach ($s as $i => $l) {
            $s[$i] = rtrim($l);
        }

        $res = trim(implode("\n", $s));
        if ('/' == substr($res, -1) && '*' != substr($res, -2, 1)) {
            $res = trim(substr($res, 0, -1));
        }

        $res = trim($res);

        if ($enleverPointVirgule && substr($res, -1) == ';') {
            $res = trim(substr($res, 0, -1));
        }

        return $res;
    }



    protected function addQuery(string $sql, string $description = null): void
    {
        $this->bdd->queryCollect($sql, $description);
    }



    public function getList(?string $name = null): array
    {
        $includes = [];
        if ($name){
            $includes = [$name];
        }

        return array_keys($this->get($includes));
    }



    /**
     * @param string|string[]|null $includes
     * @param string|string[]|null $excludes
     *
     * @return array
     */
    abstract public function get(string|array|null $includes = null, string|array|null $excludes = null): array;



    public function exists(string $name): bool
    {
        $name = $this->bdd->normalizeObjectName($name);
        $list = $this->getList($name);

        return count($list) == 1 && in_array($name, $list);
    }



    abstract public function create(array $data): void;



    abstract public function drop(array|string $name): void;



    abstract public function alter(array $old, array $new): void;



    abstract public function rename(string $oldName, array|string $new): void;



    public function prepareRenameCompare(array $data): array
    {
        unset($data['name']);

        return $data;
    }
}