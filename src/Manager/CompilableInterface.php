<?php

namespace Unicaen\BddAdmin\Manager;

interface CompilableInterface extends ManagerInterface
{
    public function compiler(string|array $name): void;



    public function compilerTout(): void;
}
