<?php

namespace Unicaen\BddAdmin\Manager;

use Unicaen\BddAdmin\Bdd;


interface ManagerInterface
{
    public function __construct(Bdd $bdd);



    public function getList(): array;



    /**
     * @param string|string[]|null $includes
     * @param string|string[]|null $excludes
     *
     * @return array
     */
    public function get(string|array|null $includes = null, string|array|null $excludes = null): array;



    public function exists(string $name): bool;



    public function create(array $data): void;



    public function drop(array|string $name): void;



    public function alter(array $old, array $new): void;



    public function rename(string $oldName, array|string $new): void;



    public function prepareRenameCompare(array $data): array;
}