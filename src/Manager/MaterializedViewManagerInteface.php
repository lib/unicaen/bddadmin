<?php

namespace Unicaen\BddAdmin\Manager;

interface MaterializedViewManagerInteface extends ManagerInterface
{
    public function refresh(string|array $name): void;
}
