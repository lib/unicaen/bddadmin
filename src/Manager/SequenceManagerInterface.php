<?php

namespace Unicaen\BddAdmin\Manager;

interface SequenceManagerInterface extends ManagerInterface
{
    public function nextVal(string|array $sequence): int;
}