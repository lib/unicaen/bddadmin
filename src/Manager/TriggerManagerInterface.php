<?php

namespace Unicaen\BddAdmin\Manager;

interface TriggerManagerInterface extends ManagerInterface
{
    public function enable(string|array $name): void;



    public function disable(string|array $name): void;



    public function enableAll(): TriggerManagerInterface;



    public function disableAll(): TriggerManagerInterface;

}