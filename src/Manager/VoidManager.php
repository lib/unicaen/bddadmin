<?php

namespace Unicaen\BddAdmin\Manager;

class VoidManager extends AbstractManager implements SchemaManagerInterface
{

    public function get($includes = null, $excludes = null): array
    {
        return [];
    }



    public function create(array $data): void
    {

    }



    public function drop(array|string $name): void
    {

    }



    public function alter(array $old, array $new): void
    {

    }



    public function rename(string $oldName, array|string $new): void
    {

    }



    public function prepareRenameCompare(array $data): array
    {
        return $data;
    }

}