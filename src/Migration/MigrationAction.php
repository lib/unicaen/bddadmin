<?php

namespace Unicaen\BddAdmin\Migration;


use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Logger\LoggerAwareTrait;

abstract class MigrationAction
{
    use BddAwareTrait;
    use LoggerAwareTrait;

    private MigrationManager $manager;



    public final function init(MigrationManager $manager)
    {
        $this->manager = $manager;
        $this->setBdd($this->manager->getBdd());
        $this->setLogger($this->getBdd()->getLogger());
    }



    protected function manager(): MigrationManager
    {
        return $this->manager;
    }



    abstract public function description(): string;



    abstract public function utile(): bool;

    /*

    Ajouter uniquement si nécessaire :
    - une méthode publique before() qui s'exécutera AVANT la mise à jour
    - une méthode publique after() qui s'exécutera APRES la mise à jour

    */
}
