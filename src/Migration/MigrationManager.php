<?php

namespace Unicaen\BddAdmin\Migration;

use Unicaen\BddAdmin\Bdd;
use Unicaen\BddAdmin\BddAwareTrait;
use Unicaen\BddAdmin\Ddl\Ddl;
use Unicaen\BddAdmin\Ddl\DdlFilters;
use Unicaen\BddAdmin\Trait\OptionsTrait;

class MigrationManager
{
    use OptionsTrait;
    use BddAwareTrait;

    const ACTION_BEFORE = 'before';
    const ACTION_AFTER  = 'after';

    protected Ddl $ref;

    protected Ddl $old;

    protected DdlFilters $filters;

    private array $actions = [];



    /**
     * Retourne la nouvelle DDL de la base de données
     */
    public function getRef(): Ddl
    {
        return $this->ref;
    }



    /**
     * Retourne l'ancienne DDL de la base de données
     */
    public function getOld(): Ddl
    {
        return $this->old;
    }



    /**
     * Détermine si un objet existe dans la base de données avant migration
     */
    public function has(string $type, string $name): bool
    {
        return isset($this->old) && isset($this->old->get($type)[$name]);
    }



    public function hasNew(string $type, string $Name): bool
    {
        return !isset($this->old->get($type)[$Name]) && isset($this->ref->get($type)[$Name]);
    }



    public function hasOld(string $type, string $Name): bool
    {
        if (Ddl::TABLE == $type) {
            return $this->tableRealExists($Name) && !isset($this->ref->get(Ddl::TABLE)[$Name]);
        } else {
            return isset($this->old->get($type)[$Name]) && !isset($this->ref->get($type)[$Name]);
        }
    }



    /**
     * Détermine si une table existe dans la base de données avant migration
     */
    public function hasTable(string $tableName): bool
    {
        return isset($this->old->get(Ddl::TABLE)[$tableName]);
    }



    /**
     * Détermine si une colonne existe dans la base de données avant migration
     */
    public function hasColumn(string $tableName, string $columnName): bool
    {
        return isset($this->old->get(Ddl::TABLE)[$tableName]['columns'][$columnName]);
    }



    /**
     * Détermine si une colonne doit être ajoutée
     */
    public function hasNewColumn(string $tableName, string $columnName): bool
    {
        $old = $this->old->get(DDl::TABLE);
        $new = $this->ref->get(Ddl::TABLE);

        return isset($new[$tableName]['columns'][$columnName]) && !isset($old[$tableName]['columns'][$columnName]);
    }



    /**
     * Détermine si une colonne doit être supprimée
     */
    public function hasOldColumn(string $tableName, string $columnName): bool
    {
        $old = $this->old->get(DDl::TABLE);
        $new = $this->ref->get(Ddl::TABLE);

        return !isset($new[$tableName]['columns'][$columnName]) && isset($old[$tableName]['columns'][$columnName]);
    }



    public function tableRealExists($tableName): bool
    {
        return $this->getBdd()->table()->exists($tableName);
    }



    public function sauvegarderTable(string $tableName, string $name): void
    {
        if ($this->tableRealExists($tableName) && !$this->tableRealExists($name)) {
            $this->getBdd()->exec("CREATE TABLE $name AS SELECT * FROM $tableName");
        }
    }



    public function supprimerSauvegarde(string $name): void
    {
        if ($this->tableRealExists($name)) {
            $this->getBdd()->exec("DROP TABLE $name");
        }
    }



    public function init(Ddl $ref, DdlFilters|array $filters = [])
    {
        $this->ref     = $ref;
        $this->filters = DdlFilters::normalize($filters);
        $this->old     = $this->getBdd()->getDdl($this->filters);
    }



    /**
     * @return array|MigrationAction[]
     * @throws \Exception
     */
    public function getScripts(): array
    {
        $scripts = $this->getOptions();

        foreach ($scripts as $index => $script) {
            if (is_string($script) && class_exists($script)) {
                $script          = $this->getBdd()->__newClass($script);
                $scripts[$index] = $script;
            }
            if (!$script instanceof MigrationAction) {
                throw new \Exception('Le script de migration ' . $script::class . ' doit correspondre à une classe héritée de ' . MigrationAction::class);
            }
            $script->init($this);
            if (!method_exists($script, self::ACTION_BEFORE) && !method_exists($script, self::ACTION_AFTER)) {
                throw new \Exception('Le script de migration ' . $script::class . ' doit avoir une ou des méthodes ' . self::ACTION_BEFORE . ' et/ou ' . self::ACTION_AFTER);
            }
        }

        return $scripts;
    }



    public function run(string $context): void
    {
        if ($context != self::ACTION_BEFORE && $context != self::ACTION_AFTER) {
            throw new \Exception('Le contexte de migration ' . $context . ' est non conforme');
        }

        $scripts = $this->getScripts();

        $traducs = [
            self::ACTION_BEFORE => 'AVANT',
            self::ACTION_AFTER  => 'APRES',
        ];

        foreach ($scripts as $object) {
            if (method_exists($object, $context)) {
                if ($object->utile()) {
                    $object->logMsg("[" . $traducs[$context] . " MIGRATION] " . $object->description() . ' ... ');
                    try {
                        $object->$context();
                        $object->logMsg("OK\n");
                    } catch (\Throwable $e) {
                        $object->logError($e);
                    }
                }
            }
        }
    }
}