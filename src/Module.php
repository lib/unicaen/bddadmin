<?php

namespace Unicaen\BddAdmin;

use Laminas\Mvc\MvcEvent;

/**
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Module
{
    public function getConfig()
    {
        return include dirname(__DIR__) . '/config/module.config.php';
    }
}