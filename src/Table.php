<?php

namespace Unicaen\BddAdmin;


use Unicaen\BddAdmin\Ddl\Ddl;

class Table
{

    use BddAwareTrait;

    private string $name;

    private array $ddl = [];

    private array $transformCache = [];

    private ?int $lastInsertId = null;



    /**
     * @inheritDoc
     */
    public function __construct(Bdd $bdd, string $name)
    {
        $this->setBdd($bdd);
        $this->name = $name;
    }



    public function getName(): string
    {
        return $this->name;
    }



    public function getDdl(): array
    {
        if (empty($this->ddl)) {
            $ddlFromFile = $this->getDdlFromFile();
            if (!empty($ddlFromFile)) {
                $this->ddl = $ddlFromFile;
            } else {
                $sTable    = $this->getBdd()->table();
                $this->ddl = $sTable->get($this->name)[$this->name];
            }
        }
        if (!$this->ddl) {
            throw new \Exception('La DDL de la table ' . $this->name . ' n\'a pas pu être calculée');
        }

        return $this->ddl;
    }



    public function getDdlFromFile(): array
    {
        [$schema, $name] = Util::explodedFullObjectName($this->getName());

        $ddlDir   = $this->getBdd()->getOption(Bdd::OPTION_DDL . '/' . Ddl::OPTION_DIR);
        $filename = $ddlDir . '/' . Ddl::TABLE . '/' . Util::fullObjectName($schema, $name) . '.php';
        if (file_exists($filename)) {
            $ddl = require $filename;
            if (is_array($ddl) && $ddl['name'] == $name) {
                return $ddl;
            }
        }
        return [];
    }



    public function setDdl(array $ddl): self
    {
        $this->ddl = $ddl;

        return $this;
    }



    public function hasHistorique(): bool
    {
        $ddl = $this->getDdl();

        $histoCols = [
            Bdd::OPTION_HISTO_CREATION_COLUMN,
            Bdd::OPTION_HISTO_MODIFICATION_COLUMN,
            Bdd::OPTION_HISTO_DESTRUCTION_COLUMN,
            Bdd::OPTION_HISTO_CREATEUR_ID_COLUMN,
            Bdd::OPTION_HISTO_MODIFICATEUR_ID_COLUMN,
            Bdd::OPTION_HISTO_DESTRUCTEUR_ID_COLUMN,
        ];

        foreach ($histoCols as $histoCol) {
            $columnName = $this->getBdd()->getOption($histoCol);
            if (!$columnName) {
                return false;
            }
            if (!isset($ddl['columns'][$columnName])) {
                return false;
            }
        }
        return true;
    }



    public function hasImport(): bool
    {
        $sourceIdCol = $this->getBdd()->getOption(Bdd::OPTION_IMPORT_SOURCE_ID_COLUMN);
        if (!$sourceIdCol) return false;

        $sourceCodeCol = $this->getBdd()->getOption(Bdd::OPTION_IMPORT_SOURCE_CODE_COLUMN);
        if (!$sourceCodeCol) return false;

        $ddl = $this->getDdl();

        return isset($ddl['columns'][$sourceIdCol]) && isset($ddl['columns'][$sourceCodeCol]);
    }



    protected function makeTypesOptions(): array
    {
        $ddl = $this->getDdl();

        $types = [];
        foreach ($ddl['columns'] as $column => $d) {
            $types[$column] = $d['type'] ?? null;
        }

        return $types;
    }



    public function select(array|string|int|null $where = null, array $options = []): array|null|SelectParser
    {
        /* Initialisation des entrées */
        $defaultOptions = [
            'custom-select' => null,
            'fetch'         => Bdd::FETCH_ALL,
            'case'          => -1,
            'types'         => $this->makeTypesOptions(),
            'key'           => null,
            'order-by'      => '',
        ];
        $options        = array_merge($defaultOptions, $options);

        $ddl = $this->getDdl();

        /* Construction et exécution de la requête */
        $cols = '';
        foreach ($ddl['columns'] as $cname => $colDdl) {
            if ($cols != '') $cols .= ', ';
            $cols .= $colDdl['name'] ?? $cname;
        }
        $sql = "SELECT $cols FROM ";
        if ($options['custom-select']) {
            $sql .= "(" . $options['custom-select'] . ") t";
        } else {
            $sql .= $this->name;
        }
        $params = [];
        $sql    .= $this->makeWhere($where, $options, $params);

        if ($options['order-by']) {
            $sql .= ' ORDER BY ' . $options['order-by'];
        }
        $select = $this->getBdd()->select($sql, $params, $options);

        if ($options['fetch'] == Bdd::FETCH_ALL) {
            /* Mise en forme des résultats */
            $data = [];
            foreach ($select as $d) {
                $keyValue        = $this->makeKey($d, $options['key']);
                $data[$keyValue] = $d;
            }

            return $data;
        } else {
            return $select;
        }
    }



    public function copy(Bdd $source, ?callable $fnc = null): self
    {
        $options = ['types' => $this->makeTypesOptions(), 'fetch' => Bdd::FETCH_EACH];

        $count = (int)$source->selectOne('SELECT count(*) c FROM ' . $this->getName(), [], 'c', ['case' => CASE_LOWER]);
        $r     = $source->select('SELECT * FROM ' . $this->getName(), [], $options);

        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logBegin("Copie de la table " . $this->getName());
        }

        $current = 0;
        $this->getBdd()->beginTransaction();
        while ($data = $r->next()) {
            $current++;
            if ($current == $count) {
                $this->getBdd()->logMsg("Copie de la table " . $this->getName() . " Terminée", true);
            } else {
                $val = round($current * 100 / $count, 2);
                $this->getBdd()->logMsg("Copie de la table " . $this->getName() . " en cours (" . $val . "%)", true);
            }
            if ($fnc instanceof \Closure) $data = $fnc($data);
            if (null !== $data) {
                $this->insert($data);
            }
        }
        $this->getBdd()->commitTransaction();

        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logEnd();
        } else {
            $this->getBdd()->logMsg("\n", true);
        }

        return $this;
    }



    public function save(string $filename, ?callable $fnc = null): self
    {
        $options = ['types' => $this->makeTypesOptions(), 'fetch' => Bdd::FETCH_EACH];

        $count = (int)$this->getBdd()->selectOne('SELECT count(*) c FROM ' . $this->getName(), [], 'c', ['case' => CASE_LOWER]);
        $r     = $this->getBdd()->select('SELECT * FROM ' . $this->getName(), [], $options);

        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logBegin("Sauvegarde de la table " . $this->getName());
        }

        if (file_exists($filename)) unlink($filename);
        if ($count > 0) {
            $buff = fopen($filename, 'w');
            fwrite($buff, $count . "\n");
        } else {
            $buff = null;
        }

        $current = 0;
        while ($data = $r->next()) {
            $current++;
            if ($current == $count) {
                $this->getBdd()->logMsg("Sauvegarde de la table " . $this->getName() . " Terminée", true);
            } else {
                $val = round($current * 100 / $count, 2);
                $this->getBdd()->logMsg("Sauvegarde de la table " . $this->getName() . " en cours (" . $val . "%)", true);
            }
            if ($fnc instanceof \Closure) $data = $fnc($data);
            if (null !== $data) {
                fwrite($buff, serialize($data) . "{<{//#end}>}\n");
            }
        }

        if ($buff) fclose($buff);

        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logEnd();
        } else {
            $this->getBdd()->logMsg("\n", true);
        }

        return $this;
    }



    public function load(string $filename, ?callable $fnc = null): self
    {
        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logBegin("Restauration de la table " . $this->getName());
        }

        $buff    = fopen($filename, 'r');
        $count   = fgets($buff);
        $count   = (int)trim($count);
        $data    = '';
        $current = 0;
        $this->getBdd()->beginTransaction();
        while (($d = fgets($buff)) !== false) {
            if ($data != '') $data != "\n";
            $data .= $d;
            if (substr($d, -13) == "{<{//#end}>}\n") {
                $line = unserialize(substr($data, 0, -12));
                if ($fnc instanceof \Closure) $line = $fnc($line);
                $current++;
                $val = round($current * 100 / $count, 2);
                $this->getBdd()->logMsg("Restauration de la table " . $this->getName() . " en cours (" . $val . "%)", true);
                if (null !== $line) {
                    $this->insert($line);
                }
                $data = '';
            }
        }
        $this->getBdd()->commitTransaction();


        $this->getBdd()->logMsg("Restauration de la table " . $this->getName() . " Terminée", true);

        fclose($buff);

        if (!$this->getBdd()->isInCopy()) {
            $this->getBdd()->logEnd();
        } else {
            $this->getBdd()->logMsg("\n", true);
        }

        return $this;
    }



    public function getLastInsertId(): ?int
    {
        return $this->lastInsertId;
    }



    public function insert(array &$data, array $options = []): bool
    {
        $bdd = $this->getBdd();

        $idCol = $this->getBdd()->getOption(Bdd::OPTION_ID_COLUMN);

        if (!isset($data[$idCol]) && $this->hasId() && $this->hasSequence()) {
            $this->lastInsertId = $this->getBdd()->sequence()->nextVal($this->ddl['sequence']);
            $data[$idCol]       = $this->lastInsertId;
        }

        $histoUserId = $bdd->getHistoUserId();
        if ($histoUserId !== null && $this->hasHistorique()) {
            $histoCreationCol       = $this->getBdd()->getOption(Bdd::OPTION_HISTO_CREATION_COLUMN);
            $histoCreateurIdCol     = $this->getBdd()->getOption(Bdd::OPTION_HISTO_CREATEUR_ID_COLUMN);
            $histoModificationCol   = $this->getBdd()->getOption(Bdd::OPTION_HISTO_MODIFICATION_COLUMN);
            $histoModificateurIdCol = $this->getBdd()->getOption(Bdd::OPTION_HISTO_MODIFICATEUR_ID_COLUMN);

            if (!isset($data[$histoCreationCol])) $data[$histoCreationCol] = new \DateTime();
            if (!isset($data[$histoCreateurIdCol])) $data[$histoCreateurIdCol] = $histoUserId;
            if (!isset($data[$histoModificationCol])) $data[$histoModificationCol] = new \DateTime();
            if (!isset($data[$histoModificateurIdCol])) $data[$histoModificateurIdCol] = $histoUserId;
        }

        $sourceId = (int)$bdd->getSourceId();
        if ($sourceId && $this->hasImport()) {
            $sourceIdCol = $bdd->getOption(Bdd::OPTION_IMPORT_SOURCE_ID_COLUMN);

            if (!isset($data[$sourceIdCol])) $data[$sourceIdCol] = $sourceId;
        }

        $cols   = [];
        $vals   = [];
        $params = [];
        foreach ($data as $col => $val) {
            $transformer = isset($options['columns'][$col]['transformer']) ? $options['columns'][$col]['transformer'] : null;

            $cols[] = $col;
            if ($transformer) {
                $vals[] = '(' . sprintf($transformer, ':' . $col) . ')';
            } else {
                $vals[] = ':' . $col;
            }
            $params[$col] = $val;
        }

        $cols = implode(", ", $cols);
        $vals = implode(", ", $vals);
        $sql  = "INSERT INTO " . $this->name . " ($cols) VALUES ($vals)";

        $bdd->exec($sql, $params, $this->makeTypesOptions());
        return true;
    }



    public function update(array $data, $where = null, array $options = []): bool
    {
        $bdd = $this->getBdd();

        $params = [];

        $histoUserId = $bdd->getHistoUserId();
        if ($histoUserId !== null && $this->hasHistorique()) {
            $histoModificationCol   = $this->getBdd()->getOption(Bdd::OPTION_HISTO_MODIFICATION_COLUMN);
            $histoModificateurIdCol = $this->getBdd()->getOption(Bdd::OPTION_HISTO_MODIFICATEUR_ID_COLUMN);

            if (!isset($data[$histoModificationCol])) $data[$histoModificationCol] = new \DateTime();
            if (!isset($data[$histoModificateurIdCol])) $data[$histoModificateurIdCol] = $histoUserId;
        }

        $dataSql = '';
        foreach ($data as $col => $val) {
            if ($dataSql != '') $dataSql .= ',';

            $transVal = ':new_' . $col;
            if (isset($options['columns'][$col]['transformer'])) {
                $transVal = '(' . sprintf($options['columns'][$col]['transformer'], $transVal) . ')';
            }
            $dataSql               .= $col . '=' . $transVal;
            $params['new_' . $col] = $val;
        }

        $sql = "UPDATE " . $this->name . " SET $dataSql" . $this->makeWhere($where, $options, $params);

        $bdd->exec($sql, $params, $this->makeTypesOptions());

        return true;
    }



    public function delete(int|string|array|null $where = null, array $options = []): bool
    {
        $params = [];
        $sql    = "DELETE FROM " . $this->name . $this->makeWhere($where, $options, $params);

        $this->getBdd()->exec($sql, $params);
        return true;
    }



    public function truncate(): bool
    {
        $sql = "TRUNCATE TABLE " . $this->name;

        $this->getBdd()->exec($sql);
        return true;
    }



    private function mergeDiff(array &$o, array &$n, array &$options): array
    {
        $diff = [];
        foreach ($o as $c => $ov) {
            if ($options['has-historique']) {
                if ($c == $options['histo-creation-column']
                    || $c == $options['histo-createur-id-column']
                    || $c == $options['histo-modification-column']
                    || $c == $options['histo-modificateur-id-column']
                    || $c == $options['histo-destruction-column']
                    || $c == $options['histo-destructeur-id-column']
                ) {
                    continue; // on ignore les colonnes relatives aux historiques
                }
            }

            $newc = $n[$c] ?? null;
            $oldc = $o[$c] ?? null;
            if ($newc instanceof \DateTime) $newc = $newc->format('Y-m-d H:i:s');
            if ($oldc instanceof \DateTime) $oldc = $oldc->format('Y-m-d H:i:s');
            if ($newc != $oldc && array_key_exists($c, $n) && $c != $options['id-column']) {
                $ok = empty($options['update-cols']); // OK par défaut si une liste n'a pas été établie manuellement

                if (in_array($c, $options['update-cols'])) $ok = true;
                if (in_array($c, $options['update-ignore-cols'])) $ok = false;
                if (in_array($c, $options['update-only-null']) && $oldc !== null) $ok = false;

                if ($ok) {
                    if ((is_float($newc) || is_double($newc)) && (is_float($oldc) || is_double($oldc))) {
                        // nécessaire à cause des pb de précision des float/doubles en PHP :
                        // dans ce cas, on considère quand même que les 2 chiffres sont identiques => diff négligeable
                        $ok = (abs($newc - $oldc) > 0.0000000001);
                    }
                }
                if ($ok) {
                    $diff[$c] = $n[$c];
                }
            }
        }

        return $diff;
    }



    public function merge(array $data, array|string|null $key, array $options = []): array
    {
        $ddl = $this->getDdl();
        $bdd = $this->getBdd();

        /* Initialisation des options */
        $hasHistorique  = $this->hasHistorique();
        $defaultOptions = [
            'custom-select'      => null,
            'where'              => null,
            'key'                => $key,
            'delete'             => true,
            'soft-delete'        => true,
            'undelete'           => true,
            'insert'             => true,
            'update'             => true,
            'return-insert-data' => false,
            'update-cols'        => [],
            'update-ignore-cols' => [],
            'update-only-null'   => [],
            'transaction'        => true,
            'id-column'          => $bdd->getOption(Bdd::OPTION_ID_COLUMN),
            'has-historique'     => $hasHistorique,
            'histo-user-id'      => null,
            'histo-date'         => new \DateTime(),
            'callback'           => null, // Format function(string $action, int $progress, int $total, array $data=[], array $key=[]){ ... }
        ];
        if ($hasHistorique) {
            $defaultOptions['histo-user-id']                = $bdd->getHistoUserId();
            $defaultOptions['histo-creation-column']        = $bdd->getOption(Bdd::OPTION_HISTO_CREATION_COLUMN);
            $defaultOptions['histo-createur-id-column']     = $bdd->getOption(Bdd::OPTION_HISTO_CREATEUR_ID_COLUMN);
            $defaultOptions['histo-modification-column']    = $bdd->getOption(Bdd::OPTION_HISTO_MODIFICATION_COLUMN);
            $defaultOptions['histo-modificateur-id-column'] = $bdd->getOption(Bdd::OPTION_HISTO_MODIFICATEUR_ID_COLUMN);
            $defaultOptions['histo-destruction-column']     = $bdd->getOption(Bdd::OPTION_HISTO_DESTRUCTION_COLUMN);
            $defaultOptions['histo-destructeur-id-column']  = $bdd->getOption(Bdd::OPTION_HISTO_DESTRUCTEUR_ID_COLUMN);

            $result = ['insert' => 0, 'update' => 0, 'delete' => 0, 'soft-delete' => 0, 'undelete' => 0];
        } else {
            $result = ['insert' => 0, 'update' => 0, 'delete' => 0];
        }
        $options = array_merge($defaultOptions, $options);

        if ($options['return-insert-data']) {
            $result['insert-data'] = [];
        }


        if ($hasHistorique && (null === $options['histo-user-id'])) {
            throw new \Exception('L\'id de l\'utilisateur pour la gestion des historiques doit être fourni en configuration ou bien en option');
        }


        /* Mise en forme des nouvelles données */
        $new = [];
        foreach ($data as $i => $d) {
            foreach ($d as $c => $v) {
                if (isset($ddl['columns'][$c])) {
                    if (null == $v) {
                        continue;
                    }
                    if (isset($options['columns'][$c]['transformer'])) {
                        $d[$c] = $this->transform($v, $options['columns'][$c]['transformer'], $ddl['columns'][$c]);
                    }
                    if (isset($ddl['columns'][$c]['type']) && $ddl['columns'][$c]['type'] == Bdd::TYPE_DATE && !empty($val) && is_string($val)) {
                        $d[$c] = \DateTime::createFromFormat('Y-m-d H:i:s', $v);
                    }
                } else {
                    unset($d[$c]);
                }
            }
            $k       = $this->makeKey($d, $key);
            $new[$k] = $d;
        }

        $callback   = $options['callback'];
        $doCallback = $callback !== null;
        if ($doCallback && !is_callable($callback)) {
            throw new \Exception('L\'option callback doit être une fonction "callable"');
        }
        $callbackCount    = count($new);
        $callbackProgress = 0;


        /* Chargement des données actuelles et traitement */
        if ($options['transaction']) {
            $bdd->beginTransaction();
        }

        $selOptions          = $options;
        $selOptions['fetch'] = Bdd::FETCH_EACH;
        $stmt                = $this->select($options['where'], $selOptions);
        while ($o = $stmt->next()) {
            // récupération de k et n & on détermine quelle action effectuer
            $k      = $this->makeKey($o, $key);
            $action = null; // rien à faire par défaut
            if (array_key_exists($k, $new)) {
                $n = $new[$k];
                unset($new[$k]); // on retire de suite du tableau
                $diff = $this->mergeDiff($o, $n, $options);
                if (!empty($diff)) {
                    if ($options['update']) {
                        $action = 'update';
                    }
                } elseif ($hasHistorique) {
                    if ($options['undelete'] && array_key_exists($options['histo-destruction-column'], $o) && $o[$options['histo-destruction-column']]) {
                        $action = 'undelete';
                    }
                }
            } else {
                if ($options['has-historique']) {
                    if (null === $o[$options['histo-destruction-column']]) {
                        // opération de soft-delete
                        if ($options['soft-delete']) {
                            $action = 'soft-delete';
                        }
                    }
                } else {
                    // opération de delete
                    if ($options['delete']) {
                        $action = 'delete';
                    }
                }
            }

            // on effectue l'action
            if ($action) {
                switch ($action) {
                    case 'delete':
                        $callbackCount++;
                        $callbackProgress++;
                        $k = $this->makeKeyArray($o, $key);
                        $this->delete($k);
                        if ($doCallback) call_user_func($callback, 'delete', $callbackProgress, $callbackCount, $o, $k);
                        break;
                    case 'soft-delete':
                        $toUpdate = [
                            $options['histo-destruction-column']    => $options['histo-date'],
                            $options['histo-destructeur-id-column'] => $options['histo-user-id'],
                        ];
                        $callbackCount++;
                        $callbackProgress++;
                        $k = $this->makeKeyArray($o, $key);
                        $this->update($toUpdate, $k, ['ddl' => $ddl]);
                        if ($doCallback) call_user_func($callback, 'soft-delete', $callbackProgress, $callbackCount, $o, $k);
                        break;
                    case 'undelete':
                        $toUpdate = [
                            $options['histo-destruction-column']    => null,
                            $options['histo-destructeur-id-column'] => null,
                        ];
                        $callbackProgress++;
                        $k = $this->makeKeyArray($o, $key);
                        $this->update($toUpdate, $k, ['ddl' => $ddl]);
                        if ($doCallback) call_user_func($callback, 'undelete', $callbackProgress, $callbackCount, $o, $k);
                        break;
                    case 'update':
                        $toUpdate = $diff;
                        if ($hasHistorique) {
                            $toUpdate[$options['histo-modification-column']]    = $options['histo-date'];
                            $toUpdate[$options['histo-modificateur-id-column']] = $options['histo-user-id'];
                        }
                        $callbackProgress++;
                        $k = $this->makeKeyArray($o, $key);
                        $this->update($toUpdate, $k, ['ddl' => $ddl]);
                        if ($doCallback) call_user_func($callback, 'update', $callbackProgress, $callbackCount, $toUpdate, $k);

                        break;
                }
                $result[$action]++;
            } else {
                $callbackProgress++;
                if ($doCallback) call_user_func($callback, 'same', $callbackProgress, $callbackCount);
            }
        }

        /* Pour finir, insertion de tous les nouveaux éléments */
        foreach ($new as $k => $n) {
            if ($options['insert']) {
                if ($hasHistorique) {
                    $n[$options['histo-creation-column']]        = $options['histo-date'];
                    $n[$options['histo-createur-id-column']]     = $options['histo-user-id'];
                    $n[$options['histo-modification-column']]    = $options['histo-date'];
                    $n[$options['histo-modificateur-id-column']] = $options['histo-user-id'];
                }
                $callbackProgress++;
                $this->insert($n);
                if ($doCallback) call_user_func($callback, 'insert', $callbackProgress, $callbackCount, $n);
                $result['insert']++;
                if ($options['return-insert-data']) {
                    $insertedData = [];
                    if (isset($n[$options['id-column']])) {
                        $insertedData[$options['id-column']] = $n[$options['id-column']];
                    }
                    foreach ($key as $null => $kc) {
                        $insertedData[$kc] = $n[$kc];
                    }

                    $result['insert-data'][$k] = $insertedData;
                }
            }
        }

        if ($options['transaction']) {
            $bdd->commitTransaction();
        }

        return $result;
    }



    private function makeKeyArray(array $data, array|string|null $key = null): array
    {
        if (!$key && $this->hasId()) {
            $key = $this->getBdd()->getOption(Bdd::OPTION_ID_COLUMN);
        }
        $key = (array)$key;

        $keyArray = [];
        foreach ($key as $kc) {
            $keyArray[$kc] = $data[$kc];
        }

        return $keyArray;
    }



    public function makeKey(array $data, array|string|null $key): string
    {
        $keyArray = $this->makeKeyArray($data, $key);

        $keyVal = '';
        foreach ($keyArray as $v) {
            if ($keyVal != '') $keyVal .= '_';
            if ($v instanceof \DateTime) {
                $keyVal .= $v->format('Y-m-d-H-i-s');
            } else {
                $keyVal .= (string)$v;
            }
        }

        return $keyVal;
    }



    private function makeWhere(int|string|array|null $where, array $options, array &$params): string
    {
        if (is_string($where) && (
                str_contains($where, '=')
                || str_contains($where, ' IN ')
                || str_contains($where, ' IN(')
                || str_contains($where, ' IS ')
                || str_contains($where, ' NOT ')
                || str_contains($where, ' NOT(')
                || str_contains($where, '<')
                || str_contains($where, '>')
                || str_contains($where, 'LIKE')
            )
        ) {
            return ' WHERE ' . $where;
        }
        if ($where && !is_array($where) && $this->hasId()) {
            $idCol = $this->getBdd()->getOption(Bdd::OPTION_ID_COLUMN);

            $where = [$idCol => $where];
        }


        if ($where) {
            $whereSql = '';
            foreach ($where as $c => $v) {
                if ($whereSql != '') {
                    $whereSql .= ' AND ';
                }


                if (isset($options['columns'][$c]['transformer'])) {
                    $transVal   = ':' . $c;
                    $transVal   = '(' . sprintf($options['columns'][$c]['transformer'], $transVal) . ')';
                    $whereSql   .= $c . ' = ' . $transVal;
                    $params[$c] = $v;
                } else {
                    if ($v === null) {
                        $whereSql .= $c . ' IS NULL';
                    } else {
                        $transVal   = ':' . $c;
                        $whereSql   .= $c . ' = ' . $transVal;
                        $params[$c] = $v;
                    }
                }
            }

            return ' WHERE ' . $whereSql;
        }

        return '';
    }



    protected function hasId(): bool
    {
        $ddl = $this->getDdl();

        $idCol = $this->getBdd()->getOption(Bdd::OPTION_ID_COLUMN);

        return isset($ddl['columns'][$idCol]);
    }



    protected function hasSequence(): bool
    {
        $ddl = $this->getDdl();

        return $ddl['sequence'] != null;
    }



    protected function transform($value, string $transformer): mixed
    {
        if (!isset($this->transformCache[$transformer][$value])) {
            $val = $this->getBdd()->select(sprintf($transformer, ':val'), ['val' => $value]);
            if (isset($val[0])) {
                $this->transformCache[$transformer][$value] = current($val[0]);
            } else {
                $this->transformCache[$transformer][$value] = null;
            }
        }

        return $this->transformCache[$transformer][$value];
    }

}