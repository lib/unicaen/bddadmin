<?php

namespace Unicaen\BddAdmin\Tools;

use Unicaen\BddAdmin\Ddl\Ddl;

class Oracle2Postgresql
{
    protected Ddl $ddl;



    public function translateDdl(Ddl $ddl): void
    {
        $this->ddl = $ddl;

        $this->translateSequences();
        $this->translateTables();
        $this->translateViews();
        //$this->ddl->set(Ddl::VIEW, []);

        //$this->translateMViews();
        $this->ddl->set(Ddl::MATERIALIZED_VIEW, []);

        $this->translateIndexes();
        $this->translatePrimaryKeys();
        $this->translateRefConstraints();
        $this->translateUniqueConstraints();

        // pas de triggers ni de packages
        $this->ddl->set(Ddl::TRIGGER, []);
        $this->ddl->set(Ddl::PACKAGE, []);
    }



    protected function translateSequences(): void
    {
        $oraSeqs = $this->ddl->get(Ddl::SEQUENCE);

        $pgSeqs = [];
        foreach ($oraSeqs as $name => $seq) {
            $pgSeqs[strtolower($name)] = [
                'schema' => 'public',
                'name' => strtolower($seq['name']),
            ];
        }

        $this->ddl->set(Ddl::SEQUENCE, $pgSeqs);
    }



    protected function translateTables(): void
    {
        $oraTables = $this->ddl->get(Ddl::TABLE);

        $pgTables = [];
        foreach ($oraTables as $name => $table) {
            $nt = $table;
            $nt['schema'] = 'public';
            $nt['logging'] = false;
            $nt['name'] = strtolower($nt['name']);
            $nt['columns'] = [];
            foreach ($table['columns'] as $cname => $column) {
                $nt['columns'][strtolower($cname)] = $this->translateColumn($column);
            }
            $pgTables[strtolower($name)] = $nt;
        }

        $this->ddl->set(Ddl::TABLE, $pgTables);
    }



    protected function translateColumn(array $column): array
    {
        $column['name'] = strtolower($column['name']);

        $convTypes = [
            'VARCHAR2' => 'character varying',
            'NUMBER'   => 'integer',
            'DATE'     => 'timestamp with time zone',
            'FLOAT'    => 'real',
            'CLOB'     => 'text',
            'BLOB'     => 'bytea',
        ];

        if (array_key_exists($column['bdd-type'], $convTypes)) {
            $column['bdd-type'] = $convTypes[$column['bdd-type']];
        } else {
            die('Type de colonne inconnu : ' . $column['bdd-type']);
        }

        if ($column['default'] == 'SYSDATE') {
            $column['default'] = 'CURRENT_TIMESTAMP';
        }

        if ($column['default'] && str_contains($column['default'], '"."NEXTVAL"')) {
            $seqName = strtolower(substr($column['default'], 1, -11));
            $column['default'] = "nextval('$seqName'::regclass)";
        }

        switch ($column['type']) {
            case 'bool':
                $column['bdd-type'] = 'boolean';
                if ($column['default'] == '0') {
                    $column['default'] = 'false';
                }
                if ($column['default'] == '1') {
                    $column['default'] = 'true';
                }
                $column['scale'] = null;
                $column['precision'] = null;
                break;
            case 'int':
                switch($column['bdd-type']){
                    case 'integer':
                        $column['precision'] = 4;
                        break;
                    case 'smallint':
                        $column['precision'] = 2;
                        break;
                    case 'bigint':
                        $column['precision'] = 8;
                        break;
                }

                if (is_string($column['scale'])) {
                    $column['scale'] = (int)$column['scale'];
                }
                if ($column['scale'] === 0) {
                    $column['scale'] = null;
                }
                break;

            case 'date':
                if ($column['precision'] === null) {
                    $column['precision'] = 6;
                }
                break;
            case 'float':
                if ($column['bdd-type'] == 'real') {
                    $column['precision'] = 4;
                }

        }

        return $column;
    }



    protected function translateViews(): void
    {
        $oraViews = $this->ddl->get(Ddl::VIEW);

        $pgViews = [];
        foreach ($oraViews as $name => $v) {

            $v['name'] = strtolower($v['name']);
            $v['definition'] = str_replace('CREATE OR REPLACE FORCE VIEW', 'CREATE OR REPLACE VIEW', $v['definition']);

            $pgViews[strtolower($name)] = $v;
        }

        $this->ddl->set(Ddl::VIEW, $pgViews);
    }



    protected function translateMViews(): void
    {
        $oraViews = $this->ddl->get(Ddl::MATERIALIZED_VIEW);

        $pgViews = [];
        foreach ($oraViews as $name => $v) {

            $v['name'] = strtolower($v['name']);

            $pgViews[strtolower($name)] = $v;
        }

        $this->ddl->set(Ddl::MATERIALIZED_VIEW, $pgViews);
    }



    protected function translateIndexes(): void
    {
        $old = $this->ddl->get(Ddl::INDEX);

        $new = [];
        foreach ($old as $name => $d) {
            $d['name'] = strtolower($d['name']);
            $d['table'] = strtolower($d['table']);
            $d['schema'] = 'public';
            $d['type'] = 'btree';
            $oldCols = $d['columns'];
            $d['columns'] = [];
            foreach( $oldCols as $col ){
                $d['columns'][] = strtolower($col);
            }

            if (!str_ends_with($name, '_PK') && !str_ends_with($name, '_UN')) {
                $new[strtolower($name)] = $d;
            }
        }

        $this->ddl->set(Ddl::INDEX, $new);
    }



    protected function translatePrimaryKeys(): void
    {
        $old = $this->ddl->get(Ddl::PRIMARY_CONSTRAINT);

        $new = [];
        foreach ($old as $name => $d) {
            $d['schema'] = 'public';
            $d['name'] = strtolower($d['name']);
            $d['table'] = strtolower($d['table']);
            $d['index'] = strtolower($d['index']);
            $oldCols = $d['columns'];
            $d['columns'] = [];
            foreach( $oldCols as $col ){
                $d['columns'][] = strtolower($col);
            }
            sort($d['columns']);
            $new[strtolower($name)] = $d;
        }

        $this->ddl->set(Ddl::PRIMARY_CONSTRAINT, $new);
    }




    protected function translateRefConstraints(): void
    {
        $old = $this->ddl->get(Ddl::REF_CONSTRAINT);

        $new = [];
        foreach ($old as $name => $d) {
            $d['schema'] = 'public';
            $d['name'] = strtolower($d['name']);
            $d['table'] = strtolower($d['table']);
            $d['rtable'] = strtolower($d['rtable']);
            $d['update_rule'] = 'NO ACTION';
            $d['delete_rule'] = $d['delete_rule'] ?? 'NO ACTION';
            $d['index'] = $d['index'] ? strtolower($d['index']) : null;
            $oldCols = $d['columns'];
            $d['columns'] = [];
            foreach( $oldCols as $col => $rcol ){
                $d['columns'][strtolower($col)] = strtolower($rcol);
            }
            $new[strtolower($name)] = $d;
        }

        $this->ddl->set(Ddl::REF_CONSTRAINT, $new);
    }



    protected function translateUniqueConstraints(): void
    {
        $old = $this->ddl->get(Ddl::UNIQUE_CONSTRAINT);

        $new = [];
        foreach ($old as $name => $d) {
            $d['schema'] = 'public';
            $d['name'] = strtolower($d['name']);
            $d['table'] = strtolower($d['table']);
            $d['index'] = strtolower($d['index']);
            $oldCols = $d['columns'];
            $d['columns'] = [];
            foreach( $oldCols as $col ){
                $d['columns'][] = strtolower($col);
            }
            sort($d['columns']);
            $new[strtolower($name)] = $d;
        }

        $this->ddl->set(Ddl::UNIQUE_CONSTRAINT, $new);
    }
}