<?php

namespace Unicaen\BddAdmin\Trait;

trait OptionsTrait
{
    private array $options = [];


    public function hasOption(string $name): bool
    {
        if (str_contains($name, '/')){
            $temp = &$this->options;
            $names = explode('/', $name);
            foreach ($names as $name) {
                if (!array_key_exists($name, $temp)){
                    return false;
                }
                $temp = &$temp[$name];
            }
            return true;
        }else{
            return array_key_exists($name, $this->options);
        }
    }



    public function getOption(string $name, mixed $default = null): mixed
    {
        if ($this->hasOption($name)) {
            if (str_contains($name, '/')){
                $temp = &$this->options;
                $names = explode('/', $name);
                foreach ($names as $name) {
                    $temp = &$temp[$name];
                }
                return $temp;
            }else{
                return $this->options[$name];
            }
        } else {
            return $default;
        }
    }



    public function setOption(string $name, mixed $value): self
    {
        if (str_contains($name, '/')) {
            $temp = &$this->options;
            $names = explode('/', $name);
            foreach ($names as $name) {
                if (!isset($temp[$name])){
                    $temp[$name] = [];
                }
                $temp = &$temp[$name];
            }

            $temp = $value;
        }else{
            $this->options[$name] = $value;
        }

        return $this;
    }



    public function getOptions(): array
    {
        return $this->options;
    }



    public function setOptions(array $options, bool $clearOthers = false): self
    {
        if ($clearOthers) {
            $this->options = [];
        }
        foreach ($options as $name => $option) {
            $this->setOption($name, $option);
        }

        return $this;
    }
}