<?php

namespace Unicaen\BddAdmin;

class Util
{

    /**
     * A partir d'un schéma et d'un nom d'objet, retourne le nom composé (schema.object)
     */
    static public function fullObjectName(?string $schema, string $object): string
    {
        if ($schema == 'public' || empty($schema)) {
            return $object;
        } else {
            return $schema . '.' . $object;
        }
    }



    /**
     * à partir d'un nom ou d'un nom complsé, retourne un tableau composé du schéma et du nom de l'objet
     *
     * @param string $object
     * @return string[]
     */
    static public function explodedFullObjectName(string $object): array
    {
        if (false !== strpos($object, '.')) {
            return explode('.', $object);
        } else {
            return [null, $object];
        }
    }



    static public function arrayExport($var, string $indent = ""): string
    {
        switch (gettype($var)) {
            case "array":
                $indexed   = array_keys($var) === range(0, count($var) - 1);
                $r         = [];
                $maxKeyLen = 0;
                foreach ($var as $key => $value) {
                    $key    = self::arrayExport($key);
                    $keyLen = strlen($key);
                    if ($keyLen > $maxKeyLen) $maxKeyLen = $keyLen;
                }
                foreach ($var as $key => $value) {
                    $key = self::arrayExport($key);
                    $r[] = "$indent    "
                        . ($indexed ? "" : str_pad($key, $maxKeyLen, ' ') . " => ")
                        . self::arrayExport($value, "$indent    ");
                }

                if (!empty($r)) {
                    return "[\n" . implode(",\n", $r) . ",\n" . $indent . "]";
                }else{
                    return "[]";
                }
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, true);
        }
    }



    static public function getPrintWorkingDirectory(): string
    {
        // Au besoin, la variable SCRIPT_LAUNCH_DIR pourra être initialisée en BASH : export SCRIPT_LAUNCH_DIR="$PWD"
        $dir = getenv('SCRIPT_LAUNCH_DIR');
        if (!$dir){
            $dir = getenv('PWD');
        }
        if (!$dir){
            $dir = getcwd();
        }

        return $dir;
    }
}