<?php declare(strict_types=1);

abstract class AbstractBddProtocoleTestCase extends AbstractBddTestCase
{
    protected array $protocole = [];



    protected function setUp(): void
    {
        parent::setUp();
        $this->protocole = require(__DIR__ . '/protocole/' . $this->bddName . '.php');
    }



    protected function test(array $action)
    {
        $manager = $this->bdd->manager($action['manager']);

        try {
            $result = call_user_func_array([$manager, $action['method']], $action['args'] ?? []);

            $tested = false;
            if (isset($action['test-exists'])) {
                $exists = call_user_func_array([$manager, 'exists'], [$action['test-exists']]);
                $this->assertTrue($exists);
                $tested = true;
            }
            if (isset($action['test-not-exists'])) {
                $exists = call_user_func_array([$manager, 'exists'], [$action['test-not-exists']]);
                $this->assertFalse($exists);
                $tested = true;
            }
            if (isset($action['expected'])) {
                $action['expected'] = $this->normalizeExpected($action['expected']);
                if (is_array($action['expected'])) {
                    $this->assertArrayEquals($action['expected'], $result);
                } else {
                    $this->assertEquals($action['expected'], $result);
                }
            }
            if (!$tested) {
                $this->assertTrue(true);
            }
        } catch (\Exception $e) {
            echo "\n" . $this->bddName . ' ' . $action['manager'] . '->' . $action['method'] . ' : ' . $e->getMessage()."\n";
            echo $e->getFile().' line '.$e->getLine()."\n";
            foreach( $e->getTrace() as $t){
                if (str_contains($t['file'], '/bddadmin/')){
                    echo $t['file'] . ' line ' . $t['line'] . "\n";
                }
            }
            $this->assertTrue(false);
        }
    }



    protected function normalizeExpected(array $expected): array
    {

        $res = [];
        foreach( $expected as $k => $v ){
            if (is_int($k) && is_string($v)){
                $res[$k] = $this->bdd->normalizeObjectName($v);
            }elseif(is_string($k)){
               $res[$this->bdd->normalizeObjectName($k)] = $v;
            }
        }
        return $res;
    }



    public function testProtocole()
    {
        foreach ($this->protocole as $action) {
            $this->test($action);
        }
    }
}
