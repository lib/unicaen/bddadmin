<?php declare(strict_types=1);

namespace Unicaen\BddAdmin\Tests;

use PHPUnit\Framework\TestCase;
use Unicaen\BddAdmin\Ddl\DdlFilter;

final class DdlFilterTest extends TestCase
{

    public function testAddInclude()
    {
        $ddlFilter = new DdlFilter();
        $ddlFilter->addInclude('objet');

        $expected = [0 => 'objet'];

        $this->assertEquals($expected, $ddlFilter->getIncludes());
    }



    public function testAddExclude()
    {
        $ddlFilter = new DdlFilter();
        $ddlFilter->addExclude('objet');

        $expected = [0 => 'objet'];

        $this->assertEquals($expected, $ddlFilter->getExcludes());
    }



    public function testSetIncludes()
    {
        $ddlFilter = new DdlFilter();

        $ddlFilter->setIncludes(['objet1', 'objet2']);
        $expected = [0 => 'objet1', 1 => 'objet2'];
        $this->assertEquals($expected, $ddlFilter->getIncludes());

        $ddlFilter->setIncludes('objet');
        $expected = [0 => 'objet'];
        $this->assertEquals($expected, $ddlFilter->getIncludes());

        $ddlFilter->setIncludes(null);
        $expected = [];
        $this->assertEquals($expected, $ddlFilter->getIncludes());
    }



    public function testSetExcludes()
    {
        $ddlFilter = new DdlFilter();

        $ddlFilter->setExcludes(['objet1', 'objet2']);
        $expected = [0 => 'objet1', 1 => 'objet2'];
        $this->assertEquals($expected, $ddlFilter->getExcludes());

        $ddlFilter->setExcludes('objet');
        $expected = [0 => 'objet'];
        $this->assertEquals($expected, $ddlFilter->getExcludes());

        $ddlFilter->setExcludes(null);
        $expected = [];
        $this->assertEquals($expected, $ddlFilter->getExcludes());
    }



    public function testAddArray()
    {
        $ddlFilter = new DdlFilter();
        $ddlFilter->setIncludes(['testIn']);
        $ddlFilter->setExcludes(['testEx']);

        $ddlFilter->addArray([DdlFilter::INCLUDES => ['newIn']]);
        $expected = [0 => 'testIn', 1 => 'newIn'];
        $this->assertEquals($expected, $ddlFilter->getIncludes());
    }
}
