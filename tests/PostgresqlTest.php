<?php declare(strict_types=1);

final class PostgresqlTest extends AbstractBddProtocoleTestCase
{
    protected string $bddName = 'Postgresql';

    protected function setUp(): void
    {
        parent::setUp();

        $this->bdd->drop();
    }
}
