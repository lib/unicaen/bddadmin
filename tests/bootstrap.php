<?php
/**
 * Bootstrap file for BddAdmin unit tests.
 *
 * @author    Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

use Unicaen\BddAdmin\Bdd;

require 'AbstractBddTestCase.php';
require 'AbstractBddProtocoleTestCase.php';

class BddAdminTest
{
    private static ?BddAdminTest $instance = null;

    /**
     * @var array|Bdd[]
     */
    private array $bdds = [];



    public static function getInstance(): BddAdminTest
    {
        if (empty(self::$instance)) {
            self::$instance = new BddAdminTest();
        }

        return self::$instance;
    }



    public function config(): array
    {
        // Fichier présent dans le répertoire de config du module
        $filename = __DIR__ . '/config/config.local.php';
        if (file_exists($filename)) {
            return require $filename;
        }

        // Fichier présent dans le répertoire de config de l'application
        $filename = dirname(dirname(dirname(dirname(__DIR__))))
            . '/config/autoload/bddadmin-tests.local.php';
        //var_dump($filename);
        if (file_exists($filename)) {
            return require $filename;
        }

        throw new \Exception('BddAdmin a besoin de configuration pour les tests unitaires. CF. tests/config/config.local.php.dist');
    }



    public function bdd(string $name): Bdd
    {
        if (!array_key_exists($name, $this->bdds)) {
            $this->bdds[$name] = $this->initBdd($name);
        }

        return $this->bdds[$name];
    }



    public function bddPostgresql(): Bdd
    {
        return $this->bdd('Postgresql');
    }



    public function bddOracle(): Bdd
    {
        return $this->bdd('Oracle');
    }



    public function bddMysql(): Bdd
    {
        return $this->bdd('Mysql');
    }



    private function initBdd(string $name): Bdd
    {
        $config = $this->config();

        if (!isset($config['bdds'][$name])) {
            throw new \Exception('BDD ' . $name . ' non trouvée en config');
        }
        $bdd = new Bdd($config['bdds'][$name]);

        return $bdd;
    }
}