<?php

$schema = 'BDDADMINTESTS';

$sequence = 'ALBUM_ID_SEQ';

return [
    // création de schéma
    [
        'manager' => 'schema',
        'method'  => 'create',
        'args'    => [
            ['name' => $schema],
        ],
    ],

    [
        'manager'  => 'schema',
        'method'   => 'getList',
        'expected' => [],
    ],

    [
        'manager'  => 'schema',
        'method'   => 'get',
        'expected' => [],
    ],


    // création de la séquence
    [
        'manager'     => 'sequence',
        'method'      => 'create',
        'args'        => [
            ['name' => $sequence],
        ],
        'test-exists' => $sequence
    ],

    [
        'manager'  => 'sequence',
        'method'   => 'getList',
        'expected' => [$sequence],
    ],

    [
        'manager'  => 'sequence',
        'method'   => 'get',
        'expected' => [$sequence => ['name' => $sequence]],
    ],


    // destruction de la séquence
    [
        'manager'         => 'sequence',
        'method'          => 'drop',
        'args'            => [
            $sequence,
        ],
        'test-not-exists' => $sequence,
    ],


    // destruction du schéma
    [
        'manager'         => 'schema',
        'method'          => 'drop',
        'args'            => [
            $schema,
        ],
        'test-not-exists' => $schema,
    ],
];