<?php

$schema = 'bddadmintests';
//$schema = 'public';

$sequence = $schema.'_album_id_seq';

$tableName = 'taux_remu';

$table = [
    'schema'      => $schema,
    'name'        => $tableName . '1',
    'temporary'   => FALSE,
    'commentaire' => 'Test de table...',
    //'sequence'    => $tableName.'_id_seq',
    'columns'     => [
        'code'              => [
            'name'        => 'code',
            'type'        => 'string',
            'bdd-type'    => 'character varying',
            'length'      => 20,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 2,
            'commentaire' => NULL,
        ],
        'histo_createur_id' => [
            'name'        => 'histo_createur_id',
            'type'        => 'int',
            'bdd-type'    => 'integer',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 5,
            'commentaire' => NULL,
        ],
        'histo_creation'    => [
            'name'        => 'histo_creation',
            'type'        => 'date',
            'bdd-type'    => 'DATE',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => 'CURRENT_TIMESTAMP',
            'position'    => 4,
            'commentaire' => NULL,
        ],
        'id'                => [
            'name'        => 'id',
            'type'        => 'int',
            'bdd-type'    => 'integer',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 1,
            'commentaire' => NULL,
        ],
        'libelle'           => [
            'name'        => 'libelle',
            'type'        => 'string',
            'bdd-type'    => 'character varying',
            'length'      => 50,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 3,
            'commentaire' => 'comm de lib',
        ],
    ],
];

$tableOriginale         = $table;
$tableOriginale['name'] = $tableName;

$tableModified                = $tableOriginale;
$tableModified['commentaire'] = 'comm table modifié';
unset($tableModified['columns']['histo_createur_id']);
$tableModified['columns']['libelle2']                   = [
    'name'        => 'libelle2',
    'type'        => 'string',
    'bdd-type'    => 'character varying',
    'length'      => 30,
    'scale'       => NULL,
    'precision'   => NULL,
    'nullable'    => TRUE,
    'default'     => NULL,
    'position'    => 5,
    'commentaire' => 'comm de lib2',
];
$tableModified['columns']['libelle']['length']          = 100;
$tableModified['columns']['libelle']['nullable']        = true;
$tableModified['columns']['libelle']['default']         = "'mon libellé'";
$tableModified['columns']['histo_creation']['bdd-type'] = 'timestamp with time zone';


$viewName = 'v_' . $tableName;
$view     = ['schema' => $schema, 'name' => $viewName . '1', 'definition' => 'SELECT * FROM ' . $schema . '.' . $tableName];


$mviewName = 'mv_' . $tableName;
$mview     = ['schema' => $schema, 'name' => $mviewName . '1', 'definition' => 'SELECT * FROM ' . $schema . '.' . $tableName];

$indexName = 'libelle_idx';
$index     = [
    'schema'  => $schema,
    'name'    => $indexName . '1',
    'table'   => $tableName,
    'columns' => ['libelle'],
];


$functionName = 'insert_into_taux_remu';
$function     = [
    'schema'     => $schema,
    'name'       => $functionName . '1',
    'definition' => "CREATE OR REPLACE FUNCTION $schema.$functionName" . "1(
    p_id integer,
    p_code varchar,
    p_libelle varchar,
    p_histo_createur_id integer
)
RETURNS void AS $$
BEGIN
    INSERT INTO $schema.$tableName(id, code, libelle, histo_createur_id)
    VALUES (p_id, p_code, p_libelle, p_histo_createur_id);
END;
$$ LANGUAGE plpgsql",
];


$triggerName     = 'update_libelle_trigger';
$triggerFunction = [
    'schema'     => $schema,
    'name'       => 'append_lib_to_libelle',
    'definition' => "CREATE OR REPLACE FUNCTION $schema.append_lib_to_libelle()
RETURNS TRIGGER AS $$
BEGIN
    NEW.libelle := NEW.libelle || '_lib';
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;",
];

$trigger = [
    'schema'     => $schema,
    'name'       => $triggerName . '1',
    'definition' => "CREATE TRIGGER $triggerName" . "1
BEFORE INSERT OR UPDATE ON $schema.$tableName
FOR EACH ROW
EXECUTE FUNCTION $schema.append_lib_to_libelle();",
];


$tests = [];


if ($schema != 'public') {
// création du schéma
    $tests[] = [
        'manager'     => 'schema',
        'method'      => 'create',
        'args'        => [
            ['name' => $schema . '1'],
        ],
        'test-exists' => $schema . '1',
    ];

    $tests[] = [
        'manager'     => 'schema',
        'method'      => 'rename',
        'args'        => [
            $schema . '1',
            $schema,
        ],
        'test-exists' => $schema,
    ];

    $tests[] = [
        'manager'  => 'schema',
        'method'   => 'getList',
        'expected' => [$schema],
    ];


    $tests[] = [
        'manager'  => 'schema',
        'method'   => 'get',
        'expected' => [$schema => ['name' => $schema]],
    ];
}


// création de la séquence
$tests[] = [
    'manager'     => 'sequence',
    'method'      => 'create',
    'args'        => [
        ['schema' => $schema, 'name' => $sequence],
    ],
    'test-exists' => $schema . '.' . $sequence,
];

$tests[] = [
    'manager'  => 'sequence',
    'method'   => 'getList',
    'expected' => [$schema . '.' . $sequence],
];

$tests[] = [
    'manager'  => 'sequence',
    'method'   => 'get',
    'expected' => [$schema . '.' . $sequence => ['schema' => $schema, 'name' => $sequence]],
];


// destruction de la séquence
$tests[] = [
    'manager'         => 'sequence',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $sequence,
    ],
    'test-not-exists' => $schema . '.' . $sequence,
];


// Création de la table
$tests[] = [
    'manager'     => 'table',
    'method'      => 'create',
    'args'        => [
        $table,
    ],
    'test-exists' => $schema . '.' . $tableName . '1',
];


// renommage de la table
$tests[] = [
    'manager'         => 'table',
    'method'          => 'rename',
    'args'            => [
        $schema . '.' . $tableName . '1',
        $schema . '.' . $tableName,
    ],
    'test-exists'     => $schema . '.' . $tableName,
    'test-not-exists' => $schema . '.' . $tableName . '1',
];

// Test de création de vues
$tests[] = [
    'manager'     => 'view',
    'method'      => 'create',
    'args'        => [
        $view,
    ],
    'test-exists' => $schema . '.' . $viewName . '1',
];

// renommage de vue
$tests[] = [
    'manager'         => 'view',
    'method'          => 'rename',
    'args'            => [
        $schema . '.' . $viewName . '1',
        $schema . '.' . $viewName,
    ],
    'test-not-exists' => $schema . '.' . $viewName . '1',
    'test-exists'     => $schema . '.' . $viewName,
];

// Test de suppression de vues
$tests[] = [
    'manager'         => 'view',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $viewName,
    ],
    'test-not-exists' => $schema . '.' . $viewName,
];


// Test de création de vues matérialisées
$tests[] = [
    'manager'     => 'materialized-view',
    'method'      => 'create',
    'args'        => [
        $mview,
    ],
    'test-exists' => $schema . '.' . $mviewName . '1',
];

// renommage de vue matérialisée
$tests[] = [
    'manager'         => 'materialized-view',
    'method'          => 'rename',
    'args'            => [
        $schema . '.' . $mviewName . '1',
        $schema . '.' . $mviewName,
    ],
    'test-not-exists' => $schema . '.' . $mviewName . '1',
    'test-exists'     => $schema . '.' . $mviewName,
];

// Test de suppression de vues matérialisées
$tests[] = [
    'manager'         => 'materialized-view',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $mviewName,
    ],
    'test-not-exists' => $schema . '.' . $mviewName,
];


// Modifications sur la table
$tests[] = [
    'manager' => 'table',
    'method'  => 'alter',
    'args'    => [
        $tableOriginale,
        $tableModified,
    ],

];

// test d'application des modifs de table
$tests[] = [
    'manager'  => 'table',
    'method'   => 'get',
    'args'     => [
        [$schema . '.' . $tableName],
    ],
    'expected' => [
        $schema . '.' . $tableName => $tableModified,
    ],
];


// tests d'index
$tests[] = [
    'manager'     => 'index',
    'method'      => 'create',
    'args'        => [
        $index,
    ],
    'test-exists' => $schema . '.' . $indexName . '1',
];

$tests[] = [
    'manager'         => 'index',
    'method'          => 'rename',
    'args'            => [
        $schema . '.' . $indexName . '1',
        $schema . '.' . $indexName,
    ],
    'test-exists'     => $schema . '.' . $indexName,
    'test-not-exists' => $schema . '.' . $indexName . '1',
];


$tests[] = [
    'manager'         => 'index',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $indexName,
    ],
    'test-not-exists' => $indexName,
];


// tests functions
$tests[] = [
    'manager'     => 'function',
    'method'      => 'create',
    'args'        => [
        $function,
    ],
    'test-exists' => $schema . '.' . $functionName . '1',
];

$tests[] = [
    'manager'         => 'function',
    'method'          => 'rename',
    'args'            => [
        $schema . '.' . $functionName . '1',
        $schema . '.' . $functionName,
    ],
    'test-not-exists' => $schema . '.' . $functionName . '1',
    'test-exists'     => $schema . '.' . $functionName,
];

$tests[] = [
    'manager'         => 'function',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $functionName,
    ],
    'test-not-exists' => $schema . '.' . $functionName,
];


// Tests des triggers
$tests[] = [
    'manager' => 'function',
    'method'  => 'create',
    'args'    => [
        $triggerFunction,
    ],
];
$tests[] = [
    'manager'     => 'trigger',
    'method'      => 'create',
    'args'        => [
        $trigger,
    ],
    'test-exists' => $schema . '.' . $triggerName . '1',
];
$tests[] = [
    'manager'         => 'trigger',
    'method'          => 'rename',
    'args'            => [
        $triggerName . '1',
        $triggerName,
        $schema . '.' . $tableName,
    ],
    'test-not-exists' => $schema . '.' . $triggerName . '1',
    'test-exists'     => $schema . '.' . $triggerName,
];
$tests[] = [
    'manager' => 'trigger',
    'method'  => 'disable',
    'args'    => [
        [
            'name'   => $triggerName,
            'table'  => $tableName,
            'schema' => $schema,
        ],
    ],
];
$tests[] = [
    'manager' => 'trigger',
    'method'  => 'enable',
    'args'    => [
        [
            'name'   => $triggerName,
            'table'  => $tableName,
            'schema' => $schema,
        ],
    ],
];
$tests[] = [
    'manager'         => 'trigger',
    'method'          => 'drop',
    'args'            => [
        $triggerName,
        $schema . '.' . $tableName,
    ],
    'test-not-exists' => $schema . '.' . $triggerName,
];
$tests[] = [
    'manager' => 'function',
    'method'  => 'drop',
    'args'    => [
        $schema . '.append_lib_to_libelle',
    ],
];


// Destruction de la table
$tests[] = [
    'manager'         => 'table',
    'method'          => 'drop',
    'args'            => [
        $schema . '.' . $tableName,
    ],
    'test-not-exists' => $schema . '.' . $tableName,
];

// destruction du schéma
if ($schema != 'public') {
    $tests[] = [
        'manager'         => 'schema',
        'method'          => 'drop',
        'args'            => [
            $schema,
        ],
        'test-not-exists' => $schema,
    ];
}

return $tests;